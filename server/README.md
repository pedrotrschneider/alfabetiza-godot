# **Instruções sobre o servidor Alfabetiza**

## **Servidor PHP**

### **Hospedagem**

Eu estou utilizando a ferramente [XAMPP](https://www.apachefriends.org/) para hospedar o servidor localmente.

Para não ser necessário mudar o ```DocumentRoot``` do servidor, mantive ele no local padrão em ```/opt/lampp/htdocs```. O aplicativo espera que haja uma pasta dentro da ```DocumentRoot``` chamada ```alfabetiza```, onde estarão os scripts do servidor. Por isso, os arquivos ```login.php``` e ```minigame_data.php``` devem ser copiados para essa pasta para que o servidor funcione. Para isso, utilize o script ```update-server.php```. Além dos dois arquivos citados, ele copia um terceiro arquivo, ```test.php``` que eu uso apenas para testar que o servidor está no ar. Caso seu ```DocumentRoot``` esteja em um diretório diferente do padrão, você deverá copiar os arquivos manualmente.

Tendo os arquivos no local certo, basta inciar o servidor com o comando ```sudo /opt/lampp/lampp start```, ou o análogo na sua máquina. Acesse ```http://localhost/alfabetiza/test.php``` para conferir que tudo está funcionando. Ele deve exibir uma página em branco dizendo ```Hello World```.

Para parar o servidor, basta utilizar o comando ```sudo /opt/lampp/lampp stop```, ou o análogo na sua máquina.

### **Requisições**

#### **login.php**

A página ```login.php``` no momento aceita uma requisição do tipo ```POST```. O corpo da requsição deve ser do tipo texto no formato:

```JSON
{
  "username": "nome_de_usuário",
  "password": "senha_do_usuário"
}
```

Onde o campo ```username``` contém o nome do usuário que está tentando logar, e o campo ```password``` contém a senha referente àquele usuário.

Ela irá retornar o código de resposta HTTP ```200``` caso as credenciais estejam corretas, e irá retornar no corpo da resposta o ```ID``` do usuário que acabou de logar.

Caso a senha esteja incorreta, ele irá retornar o código de resposta HTTP ```401``` com a mensagem ```"Wrong credentials"``` no corpo da resposta.

Caso o usuário que está tentando logar não exista, ele irá retornar o código de resposta HTTP ```402``` com a mensagem ```"User does not exist"``` no corpo da resposta.

#### **minigame_data.php**

A página ```minigame_data.php``` no momento aceita uma requsição do tipo ```POST```. O corpo da requisição deve ser do tipo texto no formato:

```JSON
{
	"user_id": 0,
    "element_id": 0,
	"minigame_id": 3,
	"level_id": 1,
	"num_mistakes": 0,
	"time_taken": 1.383278,
	"data": {
		"wrong_picks": [],
	}
}
```

Descrição dos campos:

- ```user_id```: número de ID do usuário a quem pertencem os dados sendo salvos.
- ```element_id```: número de ID do elemento que o usuário estava jogando.
- ```minigame_id```: número de ID do minijogo que o usuário estava jogando.
- ```level_id```: número de ID do níel que o usuário estava jogando.
- ```num_mistakes```: número de erros que o usuário cometeu.
- ```time_taken```: tempo em segundos que o usuário levou para completar o nível.
- ```data```: campo aidicional que carrega um objeto ```JSON``` contendo dados adicionais. No exemplo acima, ele contém um campo ```wrong_picks```, que contém um ```Array``` com as opções erradas que o usuário escolheu. Este é apenas um exemplo de uso; podem vir quaisquer dados adicionais nesse campo.

Caso os dados sejam gravados no servidor com sucesso, ele retornar o código de resposta HTTP ```200```, com a mensagem ```"Saved successfully"``` no corpo da resposta.

Caso haja um erro em gravar os dados no servidor, ele irá retornar o código de resposta HTTP ```403```, com a mensagem ```"Save attempt failed"``` no corpo da resposta. Isso provavelmente significa que os dados vieram na requisição formatados da maneira errada.

Caso o servidor receba uma requisição que não do tipo ```POST```, ele irá retornar o código de resposta HTTP ```403```, com a mensagem ```"Bad request"``` no corpo da resposta.

## **Banco de dados SQL**

O aplicativo espera que haja um banco de dados SQL chamado ```alfabetiza``` para gravar e ler os dados. Dentro desse banco de dados, por enquanto utilizamos duas tableas. Uma delas se chama ```users``` e a outra se chama ```minigame_data```.

A tabela ```users``` possui os seguintes campos:

- ```id```: número de ID do usuário.
- ```username```: nome de usuário para login.
- ```password```: senha para login.

Ela deve ser configurada conforme a estrutura da foto:

![Users table structure](resources/users_table_structure.png)

A tabela ```minigame_data``` possui os seguintes campos:

- ```id```: número de ID referente a esta entrada na base de dados.
- ```user_id```: número de ID do usuário a quem pertencem os dados dessa entrada.
- ```element_id```: número de ID do elemento referente aos dados dessa entrada.
- ```minigame_id```: número de ID do minijogo referente aos dados dessa entrada.
- ```level_id```: número de ID do nível referente aos dados dessa entrada.
- ```time_taken```: tempo em segundo que o usuário levou para completar o nível.
- ```num_mistakes```: número de erros que o usuário cometeu no nível.
- ```data```: dados adicionais relevantes sobre a performance do usuário no nível.

Ela deve ser configurada conforme a estrutura da foto:

![Minigame Data table structure](resources/minigame_data_table_structure.png)

### **Importanto o banco de dados**

Para que não seja necessário configurar manualmente o banco de dados, existe o arquivo ```alfabetiza.sql``` nesta pasta que contém as tabelas já configuradas da forma correta.

Tudo o que precisa ser feito é criar um bando de dados chamado ```alfabetiza``` e importar o arquivo ```alfabetiza.sql``` para esse banco de dados.