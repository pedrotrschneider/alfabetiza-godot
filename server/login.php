<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: *");
header("Access-Control-Allow-Headers: *");

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
  header('Access-Control-Allow-Origin: *');
  header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');
  header('Access-Control-Allow-Headers: token, Content-Type');
  header('Access-Control-Max-Age: 1728000');
  header('Content-Length: 0');
  header('Content-Type: text/plain');
  die();
  }

// Evitar injecao de codigo
function limpa ($str) {
  print "--------<br/>" . $str . ": <br/>";
  $str1 = str_replace(":", " ", $str);
  $str1 = str_replace(";", " ", $str1);
  $str1 = str_replace("!", " ", $str1);
  $str1 = str_replace("@", " ", $str1);
  $str1 = str_replace("#", " ", $str1);
  $str1 = str_replace("$", " ", $str1);
  $str1 = str_replace("%", " ", $str1);
  $str1 = str_replace("&", " ", $str1);
  $str1 = str_replace("?", " ", $str1);
  $str1 = str_replace("(", " ", $str1);
  $str1 = str_replace(")", " ", $str1);
  $str1 = str_replace("-", " ", $str1);
  $str1 = str_replace("=", " ", $str1);
  $str1 = trim($str1);
  $itens = explode(" ", $str1);
  // foreach ($itens as $umitem) print $umitem . "<br/>";
  return $itens[0];
  }

function codifica ($str) {
  return md5($str);
  }

$db_host = "localhost";
$db_user = "root";
$db_password = "";
$db_name = "alfabetiza";
$db_users_table = "users";

$conn = new mysqli($db_host, $db_user, $db_password, $db_name);
if ($conn->connect_error) {
  http_response_code(500);
  die("Connection failed: " . $conn->connect_error);
  }

if ($_SERVER['REQUEST_METHOD'] === "POST") {
  $data = json_decode(file_get_contents('php://input'), true);
  $login_username = $data["username"];
  $login_password = codifica($data["password"]); // evita arquivar senha descoberta
  // $sql = "SELECT id, password FROM $db_users_table WHERE username LIKE '" . $login_username . "'";
  $sql = "SELECT id, password FROM " . $db_users_table . " WHERE username='" . $login_username . "'";
  $result = $conn->query($sql);
  if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
      if ($row["password"] == $login_password) {
        http_response_code(200);
        print $row["id"];
      } else {
        http_response_code(401);
        print "Credenciais erradas - Wrong credentials";
        }
      }
    } else {
      http_response_code(402);
      print "Usario inexistente - user does not exist";
      }
  } else {
    http_response_code(403);
    print "Requisiao invalida - Bad request";
    }
?>
