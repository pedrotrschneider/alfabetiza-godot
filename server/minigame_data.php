<?php
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: *");
	header("Access-Control-Allow-Headers: *");

    if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');
        header('Access-Control-Allow-Headers: token, Content-Type');
        header('Access-Control-Max-Age: 1728000');
        header('Content-Length: 0');
        header('Content-Type: text/plain');
        die();
    }
    
	$db_host="localhost";
	$db_user="root";
	$db_password="";
    $db_name="alfabetiza";
    $db_minigame_table="minigame_data";
	
    $conn = new mysqli($db_host, $db_user, $db_password, $db_name);
    if ($conn->connect_error)
    {
        http_response_code(500);
        die("Connection failed: " . $conn->connect_error);
    }
    
	if ($_SERVER['REQUEST_METHOD'] === "POST")
    {
        $data = json_decode(file_get_contents('php://input'), true);

        $element_id=$data["element_id"];
        $minigame_id=$data["minigame_id"];
        $level_id=$data["level_id"];
        $user_id=$data["user_id"];
        $time_taken=$data["time_taken"];
        $num_mistakes=$data["num_mistakes"];
        $data=$data["data"];
        
        $sql="INSERT INTO $db_minigame_table (user_id,element_id,minigame_id,level_id,time_taken,num_mistakes,data) VALUES ('$user_id','$element_id','$minigame_id','$level_id','$time_taken','$num_mistakes','" . json_encode($data) . "')";
        $result=$conn->query($sql);

        if ($result)
        {
            http_response_code(200);
            echo "Saved successfully";
        } else
        {
            http_response_code(403);
            echo "Save attempt failed";
        }
    } else 
	{
		http_response_code(403);
		echo "Bad request";
	}
?>
