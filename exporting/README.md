# **Exportando o projeto para diversas plataformas**

## **Sumário**

- [Clonando o projeto](#clonando-o-projeto)
- [Configurando a Godot](#configurando-a-godot)
    - [Executável](#executável)
    - [Importando o projeto](#importando-o-projeto)
    - [Baixando o plugin Dialogic](#baixando-o-plugin-dialogic)
    - [Baixando os Export Templates](#baixando-os-export-templates)
    - [Exportando as Builds](#exportando-as-builds)
        - [Web](#web)
        - [Desktop](#desktop)
        - [Android](#android)
- [Modificando o projeto](#modificando-o-projeto)
    - [Mudando o caminho do servidor](#mudando-o-caminho-do-servidor)
    - [Habilitando e desabilitando o login](#habilitando-e-desabilitando-o-login)


## **Clonando o projeto**

A primeira coisa a fazer é clonar o repositório do projeto em sua máquina. Para isso, basta acessar [este link](https://gitlab.com/pedrotrschneider/alfabetiza-godot), clicar no botão azul escrito ```Clone``` e clicar em ```Download zip```. Depois, basta extrair o arquivo ```.zip``` em algum lugar em seus arquivos.

Alternativamente, é possível apenas rodar o seguinte comando para clonar com ```git```:

```
git clone https://gitlab.com/pedrotrschneider/alfabetiza-godot.git
```

## **Configurando a Godot**

### **Executável**

Para exportar o projeto, é necessário possuir o executável da Godot Engine no seu computador.

O projeto foi feito com a versão 3.5-stable da engine, que não necessariamente é a versão mais atual.

Para baixar o executável da Godot nessa versão específica, é possível acessar [este link](https://downloads.tuxfamily.org/godotengine/3.5/). Como a Godot roda em várias plataformas, há vários arquivos para baixar neste link, mas os mais importantes são:
- ```Godot_v3.5-stable_x11.32.zip``` para Linux 32 bits
- ```Godot_v3.5-stable_x11.64.zip``` para Linux 64 bits
- ```Godot_v3.5-stable_win32.exe.zip``` para Windows 32 bits
- ```Godot_v3.5-stable_win64.exe.zip``` para Windows 64 bits
- ```Godot_v3.5-stable_osx.universal.zip``` para OSX (MacOS)

Com qualquer um desses executáveis no sistema operacional correspondente é possível exportar para qualquer plataforma. Não é necessário rodar a Godot numa plataforma específica para exportar para ela.

Os arquivos ```.zip``` contém um executável para a plataformam específica, e, após extrair, pode ser executado normalmente. A tela que se abrirá será esta:

![Project Manager da Godot](resources/0-asset-library.png)

### **Importando o projeto**

Tendo o executável da Godot, o próximo passo é importar o projeto para acessar a engine:

1. Abra a godot.

2. Se aparecer algum pop-up falando que você não possui nenhum projeto, clique no botão ```Cancel```.

![Project Manager da Godot](resources/1-cancel-button.png)

3. No canto direito, no meio, clique no botão ```Import```.

![Project Manager da Godot](resources/2-import-button.png)

4. Um pop-up pedindo o caminho do projeto irá aparecer. Clique no botão ```Browse```.

![Botão Browse](resources/3-browse-button.png)

5. Navegue para o local onde o projeto foi clonado anteriormente, entre na pasta ```project``` e selecione o arquivo ```project.godot``` dando dois cliques nele, ou clicando no botão ```open``` após selecionar.

![Arquivo e botão de abrir arquivo](resources/4-file-open-button.png)

6. Fazendo isso, o explorador de arquivos deve fechar, e um ícone verde deve aparecer ao lado do botão ```Browse```. Se esse for o caso, clique no botão ```Import & Edit```.

![Botão Import & Edit](resources/5-import-edit-button.png)

7. Se tudo der certo, você verá essa tela:

![Tela inicial](resources/6-main-screen.png)

E assim, o projeto está importado! Das próximas vezes que você abrir a Godot, o projeto já estará na tela inicial da lista de projetos e poderá ser aberto por lá.

### **Baixando o plugin Dialogic**

A Godot é distribuída de forma modular para que o executável não fique muito grande. Por isso, há um série de plugins disponíveis a serem usados, alguns oficiais e outros feitos pela comunidade.

O projeto Alfabetiza utiliza um desses plugins, chamado Dialogic, para lidar com o sistema de diálogos do jogo. Ele precisa ser instalado para que as exportações corram bem.

1. Clique no botão ```AssetLib``` no canto superior central da tela

![Botão Asset Lib](resources/dialogic-01.png)

2. Digite ```dialogic``` na barra de busca e clique na opção de nome ```Dialogic - Dialogue Editor```

![Buscando por Dialogic](resources/dialogic-02.png)

3. Na nova janela que se abrirá, clique em ```Dowload```

![Botão Download](resources/dialogic-03.png)

4. Após o download ser finalizado, uma nova janela se abrirá mostrando os arquivos que foram baixados. Clique em ```Install```

![Botão Install](resources/dialogic-04.png)

5. Espere o plugin ser instalado

![Esperando instalação](resources/dialogic-05.png)

6. Se tudo der certo, deve aparecer uma janela falando sobre o sucesso da instalação. Clique em ```Ok```

![Botão Ok](resources/dialogic-06.png)

7. Agora, precisamos ativar o plugin. Clique no botão ```Project``` no canto superior esquerdo

![Botão Project](resources/dialogic-07.png)

8. Clique em ```Project Settings```

![Botão Project Settings](resources/dialogic-08.png)

9. Clique em ```Plugins```

![Botão Plugins](resources/dialogic-09.png)

10. Ative o plugin do ```Dialogic``` clicando na caixa ao lado da palavra ```Enabled``` (ela deve ficar marcada e azul como na imagem abaixo)

![Ativando Dialogic](resources/dialogic-10.png)

Com isso, o plugin foi instalado e ativado corretamente!

### **Baixando os Export Templates**

Para ser possível exportar o projeto da Godot para várias plataformas, são necessários os ```Export Templates```. Para baixá-los, siga esses passos:

1. Clique no botão ```Editor``` no canto superior esquerdo

![Botão Editor](resources/7-editor-button.png)

2. Clique no botão ```Manage Export Templates```

![Botão Manage Export Templates](resources/8-manage-templates-button.png)

3. Na janela que aparecer, clique no botão ```Download and Install```

![Botão Download and Install](resources/9-download-and-install-button.png)

4. Irá se abrir uma nova janela com uma barra de progresso. Basta esperar os arquivos terminarem de serem baixados.

![Tela de Download](resources/10-downloading-screen.png)

5. Quando o download terminar, basta clicar no botão ```Close``` e estará tudo certo.

![Botão de Close](resources/11-close-button.png)

Caso queira saber onde esses arquivos foram colocados, clique no botão ```Editor``` e depois clique no botão ```Open Editor Data Folder``` e um explorador de arquivos irá se abrir na pasta. Os export templates foram baixados dentro da pasta ```templates```

### **Exportando as builds**

Além dos ```Export Templates```, no repositório clonado temos também os ```Export Presets```, que contém as informações necessárias para exportas especificamente o projeto Alfabetiza. Usando esses presets, exportar para qualquer plataforma fica muito fácil.

Para qualquer plataforma, o começo do processo é o mesmo:

1. Depois de abrir o projeto, clique no botão project.

![Imagem Faltando](resources/12-project-button.png)

2. Clique no botão export

![Imagem Faltando](resources/13.export-button.png)

Se abrirá a janela de selecão de ```Export Presets```.

![Imagem Faltando](resources/14-export-presets-screen.png)

Agora, siga os passos específicos de cada plataforma.

#### **Web**

3. Clique no item que diz ```HTML5 (Runnable)``` e clique no botão ```Export Project```.

![Imagem Faltando](resources/15-html-export-preset.png)

4. Se abrirá uma janela de explorador de arquivos. Navegue até a pasta em que deseja ter os arquivos da build de web e clique no botão ```Save```. (Caso a caixa de seleção que diz ```Export With Debug``` esteja marcada, desmarque ela antes de clicar em salvar).

![Imagem Faltando](resources/16-html-select-path.png)

Se tudo correr bem, a janela irá se fechar e os arquivos HTML/JavaScript/CSS estarão prontos na pasta selecionada.

#### **Desktop**

O processo será o mesmo para ralizar builds de Linux ou Windows.

3. Clique no item que diz ```Linux/X11 (Runnable)```, ou ```Windows Desktop (Runnable)```

![Imagem Faltando](resources/17-desktop-export-preset.png)

4. Se abrirá uma janela de explorador de arquivos. Navegue até a pasta em que deseja ter os arquivos da build de desktop e clique no botão ```Save```. (Caso a caixa de seleção que diz ```Export With Debug``` esteja marcada, desmarque ela antes de clicar em salvar).

![Imagem Faltando](resources/18-desktop-select-path.png)

Se tudo correr bem, haverá um executável (```.x86_64``` para liux, ou ```.exe``` para windows) na pasta que foi selecionada durante o processo.

#### **Android**

Vou adicionar a descrição para Android em breve!

## **Modificando o projeto**

### **Mudando o caminho do servidor**

Para mudar a URL das chamadas do servidor, basta modificar uma linha em um arquivo de texto. Estando na pasta raíz do repositório, navegue para a pasta

```
project/http_requester/
```

Dentro desta pasta, há um arquivo chamado ```HTTPRequester.gd``` (tem dois arquivos de nome parecido, um com a extensão ```.gd```, e outro com a extensão ```.tscn```; o que deve ser modificado é o que tem a extensão ```.gd```). Abra este arquivo; o começo dele deve ser parecido com:

```python
extends Control

signal request_completed(response_code, body);

#const HOST_URL : String = "http://localhost/alfabetiza/";
const HOST_URL : String = "http://www.usp.br/line/alfabetiza/jogo/server/";
const LOGIN_URL : String = "login.php";
const MINIGAME_DATA_URL : String = "minigame_data.php";

...
```

A constante ```HOST_URL``` guarda a URL da raíz do servidor.

A constante ```LOGIN_URL``` guarda a URL (relativa à ```HOST_URL```) das requisições de login.

A constante ```MINIGAME_DATA_URL``` guarda a URL (relativa à ```HOST_URL```) das requisições de dados de minijogos.

Essas variáveis podem ser modificadas a depender da estrutura do servidor utilizado.

Caso deseje que o jogo rode na versão local de seu servidor, basta descomentar a linha 

```python
const HOST_URL : String = "http://localhost/alfabetiza/";
```

e deixar comentada a linha

```python
const HOST_URL : String = "http://www.usp.br/line/alfabetiza/jogo/server/";
```

Caso deseje que o jogo rode na versão do servidor da usp, realizar o processo inverso, comentando a primeira linha e descomentando a segunda.

Para comentar uma linha em ```GDScript``` basta acrescentar um ```#``` ao começo da linha. Para descomentar, basta remover o ```#``` do começo da linha, como se fosse ```Python```.

### **Habilitando e desabilitando o login**

Para fazer isso, basta mudar a cena inicial que a Godot executa. Para isso:

1. Clique em ```Project``` no canto superior esquerdo

![Botão Project](resources/login-01.png)

2. Clique em ```Project Settings```

![Botão Project Settings](resources/login-02.png)

3. Clique em ```Run```

![Botão Run](resources/login-03.png)

4. No campo ```Main Scene``` clique no ícone de pasta para selecionar a cena inicial

![Botão da pasta](resources/login-04.png)

Caso queira que o loigin seja habilitado, selecione a cena ```LoginScreen.tscn``` no caminho ```res://loign_screen``` e clique em ```open```

![Ativando login](resources/login-06.png)

Caso queira que o login seja desabilitado, selecione a cena ```ElementSelector.tscn``` no caminho ```res://elements``` e clique em ```open```

![Desativando login](resources/login-05.png)
