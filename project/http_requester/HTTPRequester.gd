extends Control

signal request_completed(response_code, body);

var HOST_URL : String = "http://www.usp.br/line/alfabetiza/jogo/server/";
const LOGIN_URL : String = "login.php";
const MINIGAME_DATA_URL : String = "minigame_data.php";


export(NodePath) onready var http_request = self.get_node(http_request) as HTTPRequest;

func _ready() -> void:
	http_request.connect("request_completed", self, "_on_request_completed");


func request_login_post(username : String, password : String) -> void:
	var body : Dictionary = {
		"username" : username,
		"password" : password,
	};
	http_request.request(HOST_URL + LOGIN_URL, [], false,\
		HTTPClient.METHOD_POST, to_json(body));


func request_minigame_data_post(user_id : int, element_id : int,\
	minigame_id : int, level_id : int, time_taken : float,\
	num_mistakes : int, data : Dictionary) -> void:
	var body : Dictionary = {
		"data" : data,
		"user_id" : user_id,
		"element_id" : element_id,
		"minigame_id" : minigame_id,
		"level_id" : level_id,
		"time_taken" : time_taken,
		"num_mistakes" : num_mistakes,
	}
	http_request.request(HOST_URL + MINIGAME_DATA_URL, [], false,\
		HTTPClient.METHOD_POST, to_json(body))


func _on_request_completed(_result : int, response_code : int,\
	_headers : PoolStringArray, body : PoolByteArray) -> void:
	self.emit_signal("request_completed", response_code, body.get_string_from_utf8());
