extends Node

const SUCCESS = 200;
const CLIENT_ERROR = 400;
const WRONG_PASSWORD_ERROR = 401;
const USERNAME_DOESNT_EXIST_ERROR = 402;
const BAD_REQUEST_ERROR = 403;
const SERVER_EREROR = 500;
const SERVER_UNAVAILABLE = 0;
