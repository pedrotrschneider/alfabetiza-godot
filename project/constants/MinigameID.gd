extends Node

# Earth minigames
const FOOD_HUNT : int = 0;
const LETTER_HUNT : int = 1;
const ACROFONY : int = 2;
const VALISE1 : int = 3;
const VALISE2 : int = 4;
const VALISE3 : int = 5;
const REBUS1 : int = 6;

# Water minigames
const RACE : int = 8;
const SYLLABLE_SWAP : int = 9;
# Valise 1 duplicate
# Valise 2 duplicate
# Valise 3 duplicate
const REBUS2 : int = 10;
const RHYME1 : int = 11;

# Fire minigames
const FIRE_RACE : int = 12;
const WORD_HUNT : int = 13;
# Valise 2 duplicate
# Valise 3 duplicate
const REBUS3 : int = 14;
const TONGUE_TWISTER : int  = 15;
const GUESS : int = 16;

# Air minigames
const MEMORY_GAME : int = 17;
# Tongue Twister duplicate
# Valise 3 duplicate
const REBUS4 : int = 18;
const PUZZLE : int = 19;
const QUIZ : int = 20;
const RETELL : int = 21;
