extends Control

onready var godot_rich_text_label: RichTextLabel = $"%GodotRichText";
onready var krita_rich_text_label: RichTextLabel = $"%KritaRichText";
onready var bird_sounds_rich_text_label: RichTextLabel = $"%BirdSoundsRichText";
onready var back_button: Button = $Back;


func _ready() -> void:
	# warning-ignore:return_value_discarded
	godot_rich_text_label.connect("meta_clicked", self, "_on_meta_clicked");
	# warning-ignore:return_value_discarded
	krita_rich_text_label.connect("meta_clicked", self, "_on_meta_clicked");
	# warning-ignore:return_value_discarded
	bird_sounds_rich_text_label.connect("meta_clicked", self, "_on_meta_clicked");
	
	# warning-ignore:return_value_discarded
	back_button.connect("pressed", self, "_on_back_button_clicked");


func _on_meta_clicked(url) -> void:
	# warning-ignore:return_value_discarded
	OS.shell_open(str(url));


func _on_back_button_clicked() -> void:
	BarTransition.transition_to("res://elements/ElementSelector.tscn");
