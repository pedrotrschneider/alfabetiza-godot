extends Control

const LOGIN_ERROR_MESSAGES = [
	"", 
	"O usuário não pode estar vazio",
	"A senha não pode estar vazia",
	"O usuário não pode conter espaços",
	"A senha não pode conter espaços"
];

export(NodePath) onready var username_line_edit = self.get_node(username_line_edit) as LineEdit;
export(NodePath) onready var password_line_edit = self.get_node(password_line_edit) as LineEdit;
export(NodePath) onready var confirm_button = self.get_node(confirm_button) as Button;
export(NodePath) onready var login_error_label = self.get_node(login_error_label) as Label;
export(NodePath) onready var request_error_label = self.get_node(request_error_label) as Label;

var username_ok : bool = false;
var password_ok : bool = false;
var login_ok : bool = false;


func _ready() -> void:
	MusicController.play_ambiance(MusicController.Ambiance.AIR);
	
	username_line_edit.grab_focus();
	confirm_button.connect("pressed", self, "_on_confirm_button_pressed");
	# warning-ignore:return_value_discarded
	HttpRequester.connect("request_completed", self, "_on_request_completed");


func _process(_delta: float) -> void:
	login_error_label.text = LOGIN_ERROR_MESSAGES[check_login_text()];
	confirm_button.disabled = !login_ok;
	
	if request_error_label.text.empty():
		request_error_label.hide();
	else:
		request_error_label.show();
	
	if login_error_label.text.empty():
		login_error_label.hide();
	else:
		login_error_label.show();


func _on_confirm_button_pressed() -> void:
	confirm_button.disabled = true;
	confirm_button.text = "Carregando...";
	HttpRequester.request_login_post(username_line_edit.text, password_line_edit.text);


func _on_request_completed(response_code: int, body: String) -> void:
	request_error_label.text = "";
	confirm_button.disabled = false;
	confirm_button.text = "Confirmar";
	if(response_code == HttpResponseCode.SUCCESS):
		print("Response code 200: request succeeded");
		var pid : int = int(body);
		print("User ID: " + str(pid));
		PlayerData.logged_in = true;
		PlayerData.player_id = pid;
		PlayerData.username = username_line_edit.text;
		BarTransition.transition_to("res://elements/ElementSelector.tscn");
	elif(response_code == HttpResponseCode.WRONG_PASSWORD_ERROR):
		print("Error resopnse code 402: " + body);
		request_error_label.text = "A senha está incorreta";
	elif(response_code == HttpResponseCode.USERNAME_DOESNT_EXIST_ERROR):
		print("Error code 401 : " + body);
		request_error_label.text = "O usuário não existe";
	elif(response_code == HttpResponseCode.BAD_REQUEST_ERROR):
		print("Error code 403 : " + body);
		request_error_label.text = "A requisição foi rejeitada";
	elif(response_code == HttpResponseCode.SERVER_EREROR\
		|| response_code == HttpResponseCode.SERVER_UNAVAILABLE):
		print("Error response code 500:" + body);
		request_error_label.text = "Falha ao se conectar ao servidor\nCheque sua "\
			+ "conexão com a internet e tente novamente";


func check_login_text() -> int:
	login_ok = false;
	if(username_line_edit.text == ""):
		return 1;
	elif(password_line_edit.text == ""):
		return 2;
	if(" " in password_line_edit.text):
		return 4;
	login_ok = true;
	return 0;
