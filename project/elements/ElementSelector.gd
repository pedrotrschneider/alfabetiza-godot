class_name ElementSelector
extends Control

export(NodePath) onready var earth_button = self.get_node(earth_button) as Button;
export(NodePath) onready var water_button = self.get_node(water_button) as Button;
export(NodePath) onready var fire_button = self.get_node(fire_button) as Button;
export(NodePath) onready var air_button = self.get_node(air_button) as Button;
export(NodePath) onready var credits_button = self.get_node(credits_button) as Button;
export(NodePath) onready var logout_button = self.get_node(logout_button) as Button;
export(NodePath) onready var username_label = self.get_node(username_label) as Label;


func _ready() -> void:
	MusicController.play_ambiance(MusicController.Ambiance.AIR);
	
	if Game.login_type == InitLoader.LoginTypes.NO_LOGIN:
		logout_button.hide();
		username_label.hide();
	
	earth_button.connect("pressed", self, "_on_earth_button_pressed");
	water_button.connect("pressed", self, "_on_water_button_pressed");
	fire_button.connect("pressed", self, "_on_fire_button_pressed");
	air_button.connect("pressed", self, "_on_air_button_pressed");
	credits_button.connect("pressed", self, "_on_credits_button_pressed");
	logout_button.connect("pressed", self, "_on_logout_button_pressed");
	
	if PlayerData.logged_in:
		username_label.text = "Logado como: %s" % PlayerData.username;


func _on_earth_button_pressed() -> void:
	BarTransition.transition_to("res://elements/earth/EarthMinigameSelector.tscn");


func _on_water_button_pressed() -> void:
	BarTransition.transition_to("res://elements/water/WaterMinigameSelector.tscn");


func _on_fire_button_pressed() -> void:
	BarTransition.transition_to("res://elements/fire/FireMinigameSelector.tscn");


func _on_air_button_pressed() -> void:
	BarTransition.transition_to("res://elements/air/AirMinigameSelector.tscn");


func _on_credits_button_pressed() -> void:
	BarTransition.transition_to("res://special_thanks/SpecialThanks.tscn");


func _on_logout_button_pressed() -> void:
	PlayerData.logout();
	BarTransition.transition_to("res://loign_screen/LoginScreen.tscn");
