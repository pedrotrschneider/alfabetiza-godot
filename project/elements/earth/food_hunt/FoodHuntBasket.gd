class_name FoodHuntBasket
extends Area2D

signal fruit_collected;


func _ready() -> void:
	# warning-ignore:return_value_discarded
	self.connect("area_entered", self, "_on_area_entered");


func _on_area_entered(area : Area2D) -> void:
	yield(self.get_tree().create_timer(0.1), "timeout");
	area.queue_free();
	
	self.emit_signal("fruit_collected");
