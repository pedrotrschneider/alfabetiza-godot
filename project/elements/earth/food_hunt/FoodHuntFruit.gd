class_name FoodHuntFruit
extends Area2D

export(NodePath) onready var sprite = self.get_node(sprite) as Sprite;
export(NodePath) onready var fail_sfx_player = self.get_node(fail_sfx_player) as AudioStreamPlayer;
export(Array, Texture) var fruit_textures = [];

const G : float = 300.0;
const SPRITE_SIZE : float = 250.0;

var fall_vel : float = 0;
var color : Color;


func _ready() -> void:
	randomize();
	var idx : int = randi() % fruit_textures.size();
	var tex : Texture = fruit_textures[idx];
	sprite.texture = tex;
	sprite.scale = Vector2(SPRITE_SIZE / tex.get_size().x, SPRITE_SIZE / tex.get_size().y);


func _physics_process(delta) -> void:
	fall_vel += G * delta;
	self.position.y += fall_vel * delta;
	if(self.position.y > 1080):
		if(!fail_sfx_player.playing):
			fail_sfx_player.play();
		yield(fail_sfx_player, "finished");
		self.queue_free();
