extends Control

export(NodePath) onready var continue_button = self.get_node(continue_button) as Button;


func _ready() -> void:
	continue_button.connect("pressed", self, "_on_continue_button_pressed");


func _on_continue_button_pressed() -> void:
	PlayerData.played_earth_legend = true;
	BarTransition.transition_to("res://elements/earth/EarthMinigameSelector.tscn");
