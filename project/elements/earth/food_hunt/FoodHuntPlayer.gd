class_name FoodHuntPlayer
extends KinematicBody2D

export(NodePath) onready var fruit_basket = self.get_node(fruit_basket) as FoodHuntBasket;
export(NodePath) onready var tween = self.get_node(tween) as Tween;
export(NodePath) onready var victory_sfx_player = self.get_node(victory_sfx_player) as AudioStreamPlayer;

const GRAVITY : float = 500.0;

var points : int = 0;
var target_pos : Vector2 = Vector2.ZERO;
var acceleration : float = 100.0;
var speed : float = 200.0;
var velocity : Vector2 = Vector2.ZERO;
var fall : float = 0;
var game_ended : bool = false;

func _ready() -> void:
	fruit_basket.connect("fruit_collected", self, "_on_fruit_collected");
	
	target_pos = self.position;


func _unhandled_input(event: InputEvent) -> void:
	if(event is InputEventScreenDrag):
		target_pos = event.position;
	elif(event is InputEventMouseButton && event.is_pressed()
		|| (Input.is_action_pressed("mouse_select")\
		&& event is InputEventMouseMotion)):
		target_pos = get_global_mouse_position();


func _physics_process(delta) -> void:
	if(!Input.is_action_pressed("mouse_select") || game_ended):
		target_pos = self.position;
		fall += GRAVITY;
	else:
		fall = 0;
	velocity = lerp(velocity, (target_pos - self.position) * speed, acceleration * delta);
	velocity.y += fall;
	
	# warning-ignore:return_value_discarded
	self.move_and_slide(velocity * delta, Vector2.UP);


func _on_fruit_collected() -> void:
	victory_sfx_player.play();
	points += 3;


func _on_game_ended() -> void:
	game_ended = true;
