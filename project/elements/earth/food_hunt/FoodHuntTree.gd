class_name FoodHuntTree
extends Node2D

export(PackedScene) var fruit_scene;
export(NodePath) onready var fruit_timer = self.get_node(fruit_timer) as Timer;


func _ready() -> void:
	fruit_timer.start();
	fruit_timer.connect("timeout", self, "_on_fruit_timer_timeout");


func _on_fruit_timer_timeout() -> void:
	randomize();
	var pos : float = -850 + randf() * 1700;
	var new_fruit : FoodHuntFruit = fruit_scene.instance();
	self.add_child(new_fruit);
	new_fruit.position.x = pos;
	new_fruit.position.y = -1080 / 2 - 100;


func _on_game_ended() -> void:
	fruit_timer.stop();
