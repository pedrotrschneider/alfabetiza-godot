class_name FoodHuntGame
extends Node2D

signal game_ended;
signal stop_player;

export(NodePath) onready var back_button = self.get_node(back_button) as Button;
export(NodePath) onready var player = self.get_node(player) as FoodHuntPlayer;
export(NodePath) onready var tree = self.get_node(tree) as FoodHuntTree;
export(NodePath) onready var countdown_timer = self.get_node(countdown_timer) as Timer;
export(NodePath) onready var game_timer = self.get_node(game_timer) as Timer;
export(NodePath) onready var time_label = self.get_node(time_label) as Label;
export(NodePath) onready var points_label = self.get_node(points_label) as Label;
export(NodePath) onready var countdown_label = self.get_node(countdown_label) as Label;
export(NodePath) onready var plant_icon = self.get_node(plant_icon) as TextureRect;
export(NodePath) onready var clock_icon = self.get_node(clock_icon) as TextureRect;
export(NodePath) onready var input_instruction_stream_player = self.get_node(input_instruction_stream_player) as AudioStreamPlayer;

export(NodePath) onready var countdown_ib = self.get_node(countdown_ib) as InstructionBackground;
export(NodePath) onready var right_hud_ib = self.get_node(right_hud_ib) as InstructionBackground;


func _ready() -> void:
	back_button.connect("pressed", self, "_on_back_button_pressed");
	countdown_timer.connect("timeout", self, "_on_countdown_timer_timeout");
	game_timer.connect("timeout", self, "_on_game_timer_timeout");
	# warning-ignore:return_value_discarded
	self.connect("game_ended", tree, "_on_game_ended");
	# warning-ignore:return_value_discarded
	self.connect("stop_player", player, "_on_game_ended");
	
	time_label.hide();
	points_label.hide();
	plant_icon.hide();
	clock_icon.hide();
	right_hud_ib.hide();
	
	yield(self.get_tree().create_timer(0.5), "timeout");
	input_instruction_stream_player.play();


func _process(_delta):
	countdown_label.text = "%d\nCLIQUE COM O BOTÃO ESQUERDO\nE DESLIZE O MOUSE" % countdown_timer.time_left;
	if(countdown_timer.time_left <= 1):
		countdown_label.text = "COLETE AS FRUTAS!";
	time_label.text = "%.1f" % game_timer.time_left;
	points_label.text = "%d" % player.points;


func _on_back_button_pressed() -> void:
	BarTransition.transition_to("res://elements/earth/EarthMinigameSelector.tscn");


func _on_countdown_timer_timeout():
	game_timer.start();
	time_label.show();
	points_label.show();
	plant_icon.show();
	clock_icon.show();
	right_hud_ib.show();
	countdown_label.hide();
	countdown_ib.hide();


func _on_game_timer_timeout():
	back_button.hide();
	self.emit_signal("game_ended");
	yield(self.get_tree().create_timer(4), "timeout");
	self.emit_signal("stop_player");
	var dialogue : Node = Dialogic.start("food_hunt_end");
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");
	yield(self.get_tree().create_timer(0.5), "timeout");
	BarTransition.transition_to("res://elements/earth/food_hunt/FoodHuntMyth.tscn");
