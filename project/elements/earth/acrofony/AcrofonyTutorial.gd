class_name AcrofonyTutorial
extends Node2D

signal tutorial_ended

export(NodePath) onready var question_card = self.get_node(question_card) as AcrofonyQuestionCard;
export(NodePath) onready var option_card1 = self.get_node(option_card1) as AcrofonyOptionCard;
export(NodePath) onready var option_card2 = self.get_node(option_card2) as AcrofonyOptionCard;
export(NodePath) onready var option_card3 = self.get_node(option_card3) as AcrofonyOptionCard;
export(NodePath) onready var animation_player = self.get_node(animation_player) as AnimationPlayer;
export(NodePath) onready var debug_syllable = self.get_node(debug_syllable) as Label;

var tutorial_data : = load("res://elements/earth/acrofony/levels/tutorial.tres") as AcrofonyLevel;

func _ready() -> void:
	question_card.texture = tutorial_data.texture;
	question_card.word = tutorial_data.question;
	
	option_card1.syllable = tutorial_data.syllables[0];
	option_card1.is_answer = true;
	option_card1.selectable = false;
	option_card1.audio_stream_player.stream = tutorial_data.syllable_audios[0];
	option_card2.syllable = tutorial_data.syllables[1];
	option_card2.selectable = false;
	option_card2.audio_stream_player.stream = tutorial_data.syllable_audios[1];
	option_card3.syllable = tutorial_data.syllables[2];
	option_card3.selectable = false;
	option_card3.audio_stream_player.stream = tutorial_data.syllable_audios[2];
	
	yield(get_tree().create_timer(1.0), "timeout");
	var dialogue : Node = Dialogic.start("acrofony_tutorial1");
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");
	animation_player.play("p1", -1, 0.6);
	yield(animation_player, "animation_finished");
	dialogue = Dialogic.start("acrofony_tutorial2");
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");
	yield(get_tree().create_timer(0.5), "timeout");
	self.emit_signal("tutorial_ended");


func set_debug_syllable(syllable : String) -> void:
	debug_syllable.text = syllable;
