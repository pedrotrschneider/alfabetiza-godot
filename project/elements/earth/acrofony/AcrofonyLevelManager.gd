class_name AcrofonyLevelManager
extends Node2D

const NUM_LEVELS : int = 15;

export(PackedScene) var game_scene;
export(PackedScene) var tutorial_scene;
export(NodePath) onready var level_label = self.get_node(level_label) as Label;
export(NodePath) onready var points_label = self.get_node(points_label) as Label;
export(NodePath) onready var back_button = self.get_node(back_button) as Button;
export(NodePath) onready var game_saver = self.get_node(game_saver) as GameSaver;

var levels : Array = range(45);
var cur_level : int = 0;
var points : int = 0;
var game_data : Dictionary = {};
var total_time : float = 0.0;
var num_mistakes : int = 0;


func _ready() -> void:
	back_button.connect("pressed", self, "_on_back_button_pressed");
	
	randomize();
	levels.shuffle();
	if not PlayerData.played_tutorials[MinigameID.ACROFONY]:
		PlayerData.played_tutorials[MinigameID.ACROFONY] = true;
		level_label.text = "Tutorial";
		var tutorial : AcrofonyTutorial = tutorial_scene.instance();
		self.add_child(tutorial);
		yield(tutorial, "tutorial_ended");
		BarTransition.begin_transition();
		yield(BarTransition, "screen_dimmed");
		self.get_child(self.get_child_count() - 1).queue_free();
		start_level();
		BarTransition.end_transition();
	else:
		start_level();


func _on_level_ended(p : int, data : Dictionary) -> void:
	points += p;
	points_label.text = str(points);
	game_data[cur_level] = data;
	total_time += data["time_taken"];
	num_mistakes += data["wrong_picks"].size();
	cur_level += 1;
	if(cur_level == NUM_LEVELS):
		end_game();
	else:
		BarTransition.begin_transition();
		yield(BarTransition, "screen_dimmed");
		self.get_child(self.get_child_count() - 1).queue_free();
		start_level();
		BarTransition.end_transition();


func _on_back_button_pressed() -> void:
	BarTransition.transition_to("res://elements/earth/EarthMinigameSelector.tscn");


func start_level() -> void:
	level_label.text = "Nível %d" % (cur_level + 1);
	var level_data : AcrofonyLevel = load("res://elements/earth/acrofony/levels/level%d.tres" % (levels[cur_level] + 1));
	var new_game : AcrofonyGame = game_scene.instance();
	new_game.level_data = level_data;
	self.add_child(new_game);
	# warning-ignore:return_value_discarded
	new_game.connect("level_ended", self, "_on_level_ended");


func end_game() -> void:
	back_button.hide();
	var dialogue : Node = Dialogic.start("acrofony_game_end");
	self.add_child(dialogue);
	dialogue.get_child(0).update_text(("Parabéns, todos os níveis foram concluídos!\nVocê ganhou %d mudas!" % points).to_upper());
	yield(dialogue, "tree_exited");
	game_saver.save(PlayerData.player_id, ElementID.EARTH, MinigameID.ACROFONY, 0, total_time,\
		num_mistakes, game_data);
	yield(game_saver, "game_saved");
	yield(get_tree().create_timer(0.5), "timeout");
	BarTransition.transition_to("res://elements/earth/EarthMinigameSelector.tscn");
