class_name AcrofonyQuestionCard
extends Node2D

export(NodePath) onready var area = self.get_node(area) as Area2D;
export(NodePath) onready var sprite = self.get_node(sprite) as Sprite;
export(NodePath) onready var label = self.get_node(label) as Label;
export(NodePath) onready var audio_stream_player = self.get_node(audio_stream_player) as AudioStreamPlayer;

var size : Vector2 = Vector2(300, 400);
var pos : Vector2 = -size / 2;
var corner_radius : int = 30;

var texture : Texture setget set_texture;
var word : String setget set_word;
var audio : AudioStream;

var mouse_in : bool = false;


func _ready() -> void:
	area.connect("mouse_entered", self, "_on_area_mouse_entered");
	area.connect("mouse_exited", self, "_on_area_mouse_exited");
	
	audio_stream_player.stream = audio;


func _input(event: InputEvent) -> void:
	if(event is InputEventMouseButton && event.is_pressed()):
		if(mouse_in):
			if(!audio_stream_player.playing):
				audio_stream_player.play();


func _draw() -> void:
	var style_box : StyleBoxFlat = StyleBoxFlat.new()
	style_box.bg_color = Color(0.4, 0.4, 0.4);
	style_box.set_corner_radius_all(corner_radius);
	draw_style_box(style_box, Rect2(pos, size));


func set_texture(val : Texture) -> void:
	texture = val;
	sprite.texture = val;
	sprite.scale = Vector2(250 / val.get_size().x, 250 / val.get_size().y);


func _on_area_mouse_entered() -> void:
	mouse_in = true;


func _on_area_mouse_exited() -> void:
	mouse_in = false;


func set_word(val : String) -> void:
	word = val;
	label.text = val.to_upper();

