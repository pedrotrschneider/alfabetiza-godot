class_name AcrofonyGame
extends Node2D

signal level_ended(points, data);

export(NodePath) onready var layer_node = self.get_node(layer_node) as Node2D;
export(NodePath) onready var debug_syllable_label = self.get_node(debug_syllable_label) as Label;
export(NodePath) onready var victory_sfx_player = self.get_node(victory_sfx_player) as AudioStreamPlayer;
export(NodePath) onready var fail_sfx_player = self.get_node(fail_sfx_player) as AudioStreamPlayer;

export(PackedScene) var option_card_scene;
export(PackedScene) var question_card_scene;

export(Resource) var level_data_export setget set_level_data;

var level_data : AcrofonyLevel;
var points : int = 3;
var time_taken : float = 0.0;
var wrong_picks : Array = [];


func _ready() -> void:
	var new_qcard : AcrofonyQuestionCard = question_card_scene.instance();
	layer_node.add_child(new_qcard);
	new_qcard.word = level_data.question;
	new_qcard.position = Vector2(1920 / 2, 300);
	new_qcard.texture = level_data.texture;
	randomize();
	var idx : Array = range(level_data.syllables.size());
	idx.shuffle();
	var ii : int = 0;
	for i in idx:
		var new_card : AcrofonyOptionCard = option_card_scene.instance();
		new_card.audio = level_data.syllable_audios[i];
		layer_node.add_child(new_card);
		new_card.syllable = level_data.syllables[i];
		new_card.is_answer = i == 0;
		# warning-ignore:return_value_discarded
		new_card.connect("card_selected", self, "_on_card_selected");
		# warning-ignore:return_value_discarded
		new_card.connect("mouse_enered_card", self, "_on_mouse_entered_card");
		# warning-ignore:return_value_discarded
		new_card.connect("mouse_exited_card", self, "_on_mouse_exited_card");
		new_card.position = Vector2(1920 / 4 * (ii + 1), 700);
		ii += 1;


func _process(delta: float) -> void:
	time_taken += delta;


func _on_card_selected(is_answer : bool, syllable : String) -> void:
	if(!is_answer):
		fail_sfx_player.play();
		points -= 1;
		wrong_picks.append(syllable);
		return;
	var data = {
		"level" : level_data.level_num,
		"wrong_picks" : wrong_picks,
		"time_taken" : time_taken,
	}
	victory_sfx_player.play();
	for c in get_children():
		if(c is AcrofonyOptionCard):
			c.selectable = false;
	yield(get_tree().create_timer(0.5), "timeout");
	self.emit_signal("level_ended", points, data);


func _on_mouse_entered_card(syllable : String) -> void:
	debug_syllable_label.text = syllable;


func _on_mouse_exited_card() -> void:
	debug_syllable_label.text = "";


func set_level_data(val : Resource) -> void:
	level_data_export = val;
	level_data = val as AcrofonyLevel;
