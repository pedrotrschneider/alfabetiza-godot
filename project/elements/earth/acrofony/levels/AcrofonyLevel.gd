class_name AcrofonyLevel
extends Resource

export var level_num : int;
export var texture : Texture;
export var question : String;
export var syllables : PoolStringArray = ["", "", ""];
export(Array, AudioStream) var syllable_audios;
