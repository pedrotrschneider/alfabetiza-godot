extends Sprite

var size : int;
var speed_mult : float;

var max_size : float = 500;
var min_size : float = 250;

var max_x : float = 1150.0;
var min_x : float = -1150.0;

export var max_y : float = 150.0;
export var min_y : float = -600.0;


func _ready() -> void:
	randomize_new();
	self.position.x = rand_range(min_x, max_x);
	self.position.x = rand_range(min_x, max_x);


func _process(delta: float) -> void:
	self.position.x += size * speed_mult * self.scale.x * delta;
	
	if self.position.x <= min_x || self.position.x >= max_x:
		randomize_new();


func randomize_new() -> void:
	self.scale.x = 1;
	self.z_index = -1;
	
	randomize();
	
	self.position.x = min_x;
	size = int(rand_range(min_size, max_size))
	resize(size);
	
	speed_mult = rand_range(0.4, 0.8);
	
	if rand_range(0, 100) > 50:
		self.scale.x *= -1;
	if rand_range(0, 100) > 50:
		self.position.x = max_x;
	self.position.y = rand_range(min_y, max_y);


func resize(new_size: int) -> void:
	self.scale = Vector2(new_size / self.texture.get_size().x, new_size / self.texture.get_size().y);
