class_name LetterHuntPlant
extends Node2D

signal fruit_selected;

export(NodePath) onready var area = self.get_node(area) as Area2D;
export(NodePath) onready var label = self.get_node(label) as Label;
export(NodePath) onready var tween = self.get_node(tween) as Tween;
export(NodePath) onready var sprite2 = self.get_node(sprite2) as Sprite;

var letter : String;
var mouse_over : bool = false;
var active : bool = true;


func _ready() -> void:
	area.connect("mouse_entered", self, "_on_area_mouse_entered");
	area.connect("mouse_exited", self, "_on_area_mouse_exited");
	
	label.text = letter;
	self.scale = Vector2.ZERO;
	interpolate_scale_to(1.0);


func _physics_process(_delta):
	if(mouse_over && active):
		if(Input.is_action_just_pressed("mouse_select")):
			interpolate_scale_to(0.9);
		elif(Input.is_action_just_released("mouse_select")):
			interpolate_scale_to(1.0);
			self.emit_signal("fruit_selected", letter);
			sprite2.modulate = Color(0, 0, 0, 0.8);
			label.hide();
			active = false;


func _on_area_mouse_entered() -> void:
	if(!active): return;
	mouse_over = true;
	interpolate_scale_to(1.2);


func _on_area_mouse_exited() -> void:
	if(!active): return;
	mouse_over = false;
	interpolate_scale_to(1.0);


func destroy() -> void:
	self.queue_free();


func interpolate_scale_to(val : float) -> void:
	tween.stop_all();
	tween.interpolate_property(self, "scale", null, Vector2(val, val), 1.0, Tween.TRANS_ELASTIC, Tween.EASE_OUT);
	tween.start();
