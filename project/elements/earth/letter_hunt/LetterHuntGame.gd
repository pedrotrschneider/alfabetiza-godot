class_name LetterHuntGame
extends Node2D

signal round_ended();

const letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

export(PackedScene) var letter_hunt_plant;
export(NodePath) onready var back_button = self.get_node(back_button) as Button;
export(NodePath) onready var countdown_label = self.get_node(countdown_label) as Label;
export(NodePath) onready var countdown_description_label = self.get_node(countdown_description_label) as Label;
export(NodePath) onready var question_label = self.get_node(question_label) as RichTextLabel;
export(NodePath) onready var question_letter_label = self.get_node(question_letter_label) as RichTextLabel;
export(NodePath) onready var game_time_label = self.get_node(game_time_label) as Label;
export(NodePath) onready var round_time_label = self.get_node(round_time_label) as Label;
export(NodePath) onready var points_label = self.get_node(points_label) as Label;
export(NodePath) onready var plant_icon_rect = self.get_node(plant_icon_rect) as TextureRect;
export(NodePath) onready var countdown_timer = self.get_node(countdown_timer) as Timer;
export(NodePath) onready var game_timer = self.get_node(game_timer) as Timer;
export(NodePath) onready var round_timer = self.get_node(round_timer) as Timer;
export(NodePath) onready var letters_node = self.get_node(letters_node) as Node2D;
export(NodePath) onready var victory_sfx_player = self.get_node(victory_sfx_player) as AudioStreamPlayer;
export(NodePath) onready var fail_sfx_player = self.get_node(fail_sfx_player) as AudioStreamPlayer;
export(NodePath) onready var game_saver = self.get_node(game_saver) as GameSaver;

export(NodePath) onready var question_hud_ib = self.get_node(question_hud_ib) as InstructionBackground;
export(NodePath) onready var left_hud_ib = self.get_node(left_hud_ib) as InstructionBackground;

var points : int = 0;
var letters_left : int = 3;
var answer_letter : String;
var iteration : int = 0;
var wrong_picks : Array = [];
var time_taken : float = 0;
var num_mistakes : int = 0;
var data : Dictionary = {};


func _ready() -> void:
	back_button.connect("pressed", self, "_on_back_button_pressed");
	countdown_timer.connect("timeout", self, "_on_countdown_timer_timeout");
	game_timer.connect("timeout", self, "_on_game_timer_timeout");
	round_timer.connect("timeout", self, "_on_round_timer_timeout");
	
	countdown_timer.start();


func _process(delta) -> void:
	time_taken += delta;
	countdown_label.text = "%d" % countdown_timer.time_left;
	if(countdown_timer.time_left <= 1):
		countdown_label.text = "Começar!";
	
	game_time_label.text = "t1: %.1f" % game_timer.time_left;
	round_time_label.text = "t2: %.1f" % max(round_timer.time_left - 0.1, 0.0);
	points_label.text = "%d" % points;


func _on_fruit_selected(letter : String) -> void:
	if(answer_letter == letter):
		letters_left -= 1;
		points += 3;
		victory_sfx_player.play();
		question_letter_label.bbcode_text = "[center][color=#ff0000]%s[/color][/center]" % [answer_letter];
		if(letters_left == 0):
			reset_game(false);
			question_letter_label.hide();
			var text : String = "você encontrou todas as letras".to_upper();
			question_label.bbcode_text = ("[center]%s[/center]") % [text];
	else:
		fail_sfx_player.play();
		wrong_picks.append(letter);


func _on_back_button_pressed() -> void:
	BarTransition.transition_to("res://elements/earth/EarthMinigameSelector.tscn");


func _on_countdown_timer_timeout() -> void:
	question_label.show();
	question_letter_label.show();
	question_hud_ib.show();
	game_time_label.show();
	round_time_label.show();
	points_label.show();
	left_hud_ib.show();
	plant_icon_rect.show();
	countdown_label.hide();
	countdown_description_label.hide();
	game_timer.start();
	init_game();


func _on_game_timer_timeout() -> void:
	question_label.hide();
	question_letter_label.hide();
	back_button.hide();
	round_timer.stop();
	game_time_label.hide();
	round_time_label.hide();
	left_hud_ib.hide();
	for c in letters_node.get_children():
		c.destroy();
	var dialogue : Node = Dialogic.start("letter_hunt_end");
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");
	game_saver.save(PlayerData.player_id, ElementID.EARTH, MinigameID.LETTER_HUNT,\
		0, 120.0, num_mistakes, data);
	yield(game_saver, "game_saved");
	yield(get_tree().create_timer(0.5), "timeout");
	BarTransition.transition_to("res://elements/earth/EarthMinigameSelector.tscn");


func _on_round_timer_timeout() -> void:
	reset_game(true);


func init_game() -> void:
	time_taken = 0;
	letters_left = 3;
	wrong_picks.clear();
	round_timer.stop();
	round_timer.start();
	for c in letters_node.get_children():
		c.destroy();
	randomize();
	var text : String = "encontre a letra".to_upper();
	question_label.bbcode_text = ("[center]%s[/center]") % [text];
	answer_letter = letters[randi() % len(letters)];
	question_letter_label.show();
	question_letter_label.bbcode_text = "[center][color=#ff0000]%s[/color][/center]" % [answer_letter];
	var game_letters : Array = [answer_letter, answer_letter, answer_letter];
	var round_letters : String = letters.replace(answer_letter, "");
	for i in 17:
		game_letters.append(round_letters[randi() % len(round_letters)]);
	game_letters.shuffle();
	for i in 4:
		for j in 5:
			var new_plant : LetterHuntPlant = letter_hunt_plant.instance();
			new_plant.letter = game_letters[i * 5 + j];
			letters_node.add_child(new_plant);
			var x : float = 50 + 1870 / 10 + 1870 / 5 * j + rand_range(-1870 / 10 + 60, 1870 / 10 - 60);
			var y : float = 300 + 780 / 8 + 780 / 4 * i + rand_range(-780 / 8 + 60, 780 / 8 - 60);
			new_plant.position = Vector2(x, y);
			# warning-ignore:return_value_discarded
			new_plant.connect("fruit_selected", self, "_on_fruit_selected");


func end_game(failed : bool) -> void:
	num_mistakes += wrong_picks.size();
	data[iteration] = {
		"letter" : answer_letter,
		"wrong_picks" : wrong_picks.duplicate(),
		"time_taken" : time_taken,
		"failed" : failed,
	}
	iteration += 1;
	yield(self.get_tree().create_timer(0.5), "timeout");
	self.emit_signal("round_ended");

func reset_game(failed : bool) -> void:
	end_game(failed);
	yield(self, "round_ended");
	init_game();
