class_name Valise3Tutorial
extends Node2D

enum Element {
	earth,
	water,
	fire,
	air
}

var LEVEL_DATA_PATHS := [
	load("res://elements/earth/valise3/levels/tutorial.tres"),
	load("res://elements/water/valise3/levels/tutorial.tres"),
	load("res://elements/fire/valise3/levels/tutorial.tres"),
	load("res://elements/air/valise3/levels/tutorial.tres")
]

export(Element) var element = Element.earth;
export(Array, NodePath) var element_visuals;

export(NodePath) onready var animation_player = self.get_node(animation_player) as AnimationPlayer;
export(NodePath) onready var back_button = self.get_node(back_button) as Button;
export(NodePath) onready var question_card = self.get_node(question_card) as Valise3QuestionCard;
export(NodePath) onready var option_card1 = self.get_node(option_card1) as Valise3OptionCard;
export(NodePath) onready var option_card2 = self.get_node(option_card2) as Valise3OptionCard;
export(NodePath) onready var option_card3 = self.get_node(option_card3) as Valise3OptionCard;
export(NodePath) onready var option_card4 = self.get_node(option_card4) as Valise3OptionCard;

var level_data : Valise3Level;

func _ready() -> void:
	for i in element_visuals.size():
		element_visuals[i] = self.get_node(element_visuals[i]);
	
	for i in element_visuals.size():
		if i != element:
			element_visuals[i].hide();
	
	back_button.connect("pressed", self, "_on_back_button_pressed");
	
	level_data = LEVEL_DATA_PATHS[element];
	
	question_card.word = level_data.question_word;
	option_card1.word = level_data.option_words[0];
	option_card1.selectable = false;
	option_card1.is_answer = true;
	option_card2.word = level_data.option_words[1];
	option_card2.selectable = false;
	option_card3.word = level_data.option_words[2];
	option_card3.selectable = false;
	option_card4.word = level_data.option_words[3];
	option_card4.selectable = false;
	
	var dialogue : Node = Dialogic.start(get_dialogue(1));
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");
	animation_player.play("p1");
	yield(animation_player, "animation_finished");
	dialogue = Dialogic.start(get_dialogue(2));
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");
	yield(get_tree().create_timer(0.5), "timeout");
	
	BarTransition.transition_to(get_level_selector_scene_path());
	yield(BarTransition, "screen_dimmed");
	self.queue_free();


func _on_back_button_pressed() -> void:
	BarTransition.transition_to(get_level_selector_scene_path());
	yield(BarTransition, "screen_dimmed");
	self.queue_free();


func get_level_selector_scene_path() -> String:
	match element:
		Element.earth:
			return "res://elements/earth/valise3/Valise3LevelSelector.tscn";
		Element.water:
			return "res://elements/water/valise3/Valise3LevelSelector.tscn";
		Element.fire:
			return "res://elements/fire/valise3/Valise3LevelSelector.tscn";
		Element.air:
			return "res://elements/air/valise3/Valise3LevelSelector.tscn";
	
	printerr("Invalid element");
	return "";


func get_dialogue(idx : int) -> String:
	var folder : String;
	match element:
		Element.earth:
			folder = "earth";
		Element.water:
			folder = "water";
		Element.fire:
			folder = "fire";
		Element.air:
			folder = "air";
	return ("%s/valise3/valise3_tutorial%d" % [folder, idx]);
