class_name Valise3Game
extends Node2D

enum Element {
	earth,
	water,
	fire,
	air
}

export(Element) var element = Element.earth;
export(Array, NodePath) var element_visuals;

export(PackedScene) var option_card_scene;
export(PackedScene) var question_card_scene;
export(NodePath) onready var level_label = self.get_node(level_label) as Label;
export(NodePath) onready var back_button = self.get_node(back_button) as Button;
export(NodePath) onready var victory_sfx_player = self.get_node(victory_sfx_player) as AudioStreamPlayer;
export(NodePath) onready var fail_sfx_player = self.get_node(fail_sfx_player) as AudioStreamPlayer;
export(NodePath) onready var game_saver = self.get_node(game_saver) as GameSaver;

var level_data : Valise3Level;
var points : int = 3;
var wrong_picks : Array = [];
var time_taken : float = 0;


func _ready() -> void:
	for i in element_visuals.size():
		element_visuals[i] = self.get_node(element_visuals[i]);
	
	for i in element_visuals.size():
		if i != element:
			element_visuals[i].hide();
		
	back_button.connect("pressed", self, "_on_back_button_pressed");
	
	init_game();


func _process(delta: float) -> void:
	time_taken += delta;


func _on_card_selected(is_answer : bool, word : String) -> void:
	if(!is_answer):
		fail_sfx_player.play();
		wrong_picks.append(word)
		points -= 1;
		return;
	level_data.num_answers -= 1;
	victory_sfx_player.play();
	if(level_data.num_answers > 0):
		return;
	for c in get_children():
		if(c is Valise3OptionCard):
			c.selectable = false;
	var data : Dictionary = {
		"wrong_picks" : wrong_picks
	}
	game_saver.save(PlayerData.player_id, element, MinigameID.VALISE3,\
		level_data.level_num, time_taken, wrong_picks.size(), data);
	yield(game_saver, "game_saved");
	yield(get_tree().create_timer(0.5), "timeout");
	
	var next_level_idx: int = level_data.level_num + 1;
	var file: File = File.new();
	var next_level_path: String = get_level_resources_path() + "level%d.tres" % next_level_idx;
	if file.file_exists(next_level_path):
		level_data = load(next_level_path);
		element = element;
		BarTransition.begin_transition();
		yield(BarTransition, "screen_dimmed");
		reset();
		BarTransition.end_transition();
	else:
		BarTransition.transition_to(get_level_selector_scene_path());
		yield(BarTransition, "screen_dimmed");
		self.queue_free();


func _on_back_button_pressed() -> void:
	BarTransition.transition_to(get_level_selector_scene_path());
	yield(BarTransition, "screen_dimmed");
	self.queue_free();


func reset() -> void:
	points = 3;
	wrong_picks = [];
	time_taken = 0;
	
	for c in self.get_children():
		if c is Valise3OptionCard or c is Valise3QuestionCard:
			c.queue_free();
	
	init_game();


func init_game() -> void:
	level_label.text = "NÍVEL %d" % level_data.level_num;
	var new_qcard : Valise3QuestionCard = question_card_scene.instance();
	self.add_child(new_qcard);
	new_qcard.word = level_data.question_word;
	new_qcard.position = Vector2(1920 / 2, 350);
	randomize();
	var num_opts : int = level_data.option_words.size();
	var idx : Array = range(num_opts);
	idx.shuffle();
	var ii : int = 0;
	for i in idx:
		var new_card : Valise3OptionCard = option_card_scene.instance();
		self.add_child(new_card);
		new_card.is_answer = i < level_data.num_answers;
		new_card.word = level_data.option_words[i];
		# warning-ignore:return_value_discarded
		new_card.connect("card_selected", self, "_on_card_selected");
		new_card.position = Vector2(1920 / float(num_opts + 1) * (ii + 1), 800);
		ii += 1;


func get_level_resources_path() -> String:
	match element:
		Element.earth:
			return "res://elements/earth/valise3/levels/";
		Element.water:
			return "res://elements/water/valise3/levels/";
		Element.fire:
			return "res://elements/fire/valise3/levels/";
		Element.air:
			return "res://elements/air/valise3/levels/";
	
	printerr("Invalid element");
	return "";


func get_level_selector_scene_path() -> String:
	match element:
		Element.earth:
			return "res://elements/earth/valise3/Valise3LevelSelector.tscn";
		Element.water:
			return "res://elements/water/valise3/Valise3LevelSelector.tscn";
		Element.fire:
			return "res://elements/fire/valise3/Valise3LevelSelector.tscn";
		Element.air:
			return "res://elements/air/valise3/Valise3LevelSelector.tscn";
	
	printerr("Invalid element");
	return "";
