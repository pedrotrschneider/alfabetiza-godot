class_name Valise1Tutorial
extends Node2D

enum Element {
	earth,
	water
}

var LEVEL_DATA_PATHS := [
	load("res://elements/earth/valise1/levels/tutorial.tres"),
	load("res://elements/water/valise1/levels/tutorial.tres")
]

export(Element) var element = Element.earth;
export(Array, NodePath) var element_visuals;

export(NodePath) onready var animation_player = self.get_node(animation_player) as AnimationPlayer;
export(NodePath) onready var back_button = self.get_node(back_button) as Button;
export(NodePath) onready var question_card = self.get_node(question_card) as Valise1QuestionCard;
export(NodePath) onready var option_card1 = self.get_node(option_card1) as Valise1OptionCard;
export(NodePath) onready var option_card2 = self.get_node(option_card2) as Valise1OptionCard;
export(NodePath) onready var option_card3 = self.get_node(option_card3) as Valise1OptionCard;
export(NodePath) onready var option_card4 = self.get_node(option_card4) as Valise1OptionCard;

var level_data : Valise1Level;

func _ready() -> void:
	for i in element_visuals.size():
		element_visuals[i] = self.get_node(element_visuals[i]);
	
	for i in element_visuals.size():
		if i != element:
			element_visuals[i].hide();
	
	back_button.connect("pressed", self, "_on_back_button_pressed");
	
	level_data = LEVEL_DATA_PATHS[element];
	
	question_card.texture = level_data.question_texture;
	option_card1.texture = level_data.option_textures[0];
	option_card1.selectable = false;
	option_card1.is_answer = true;
	option_card2.texture = level_data.option_textures[1]
	option_card2.selectable = false;
	option_card3.texture = level_data.option_textures[2];
	option_card3.selectable = false;
	option_card4.texture = level_data.option_textures[3];
	option_card4.selectable = false;
	
	var dialogue : Node
	dialogue = Dialogic.start(get_dialogue(1));
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");
	animation_player.play("p1");
	yield(animation_player, "animation_finished");
	dialogue = Dialogic.start(get_dialogue(2));
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");
	yield(get_tree().create_timer(0.5), "timeout");
	
	BarTransition.transition_to(get_level_selector_scene_path());
	yield(BarTransition, "screen_dimmed");
	self.queue_free();


func _on_back_button_pressed() -> void:
	BarTransition.transition_to(get_level_selector_scene_path());
	yield(BarTransition, "screen_dimmed");
	self.queue_free();


func get_level_selector_scene_path() -> String:
	match element:
		Element.earth:
			# warning-ignore:return_value_discarded
			return "res://elements/earth/valise1/Valise1LevelSelector.tscn";
		Element.water:
			# warning-ignore:return_value_discarded
			return "res://elements/water/valise1/Valise1LevelSelector.tscn";
	
	printerr("Invalid element");
	return "";


func get_dialogue(idx : int) -> String:
	var folder : String;
	match element:
		Element.earth:
			folder = "earth";
		Element.water:
			folder = "water";
	return ("%s/valise1/valise1_tutorial%d" % [folder, idx]);
