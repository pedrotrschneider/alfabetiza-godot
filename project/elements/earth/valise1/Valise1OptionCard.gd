class_name Valise1OptionCard
extends Node2D

signal card_selected(is_answer, word);
signal mouse_entered_card(word);
signal mouse_exited_card();

export(NodePath) onready var sprite = self.get_node(sprite) as Sprite;
export(NodePath) onready var area = self.get_node(area) as Area2D;
export(NodePath) onready var tween = self.get_node(tween) as Tween;

var size : Vector2 = Vector2(300, 300);
var pos : Vector2 = -size / 2;
var corner_radius : int = 30;

var texture : Texture setget set_texture;
var word : String = "";
var is_answer : bool = false;
var mouse_over : bool = false;
var selectable : bool = true;
var selected : bool = false;


func _ready() -> void:
	area.connect("mouse_entered", self, "_on_area_mouse_entered");
	area.connect("mouse_exited", self, "_on_area_mouse_exited");


func _input(event: InputEvent) -> void:
	if(selectable && mouse_over):
		if(event.is_action_pressed("mouse_select") || (event is InputEventScreenTouch && event.is_pressed())):
			interpolate_scale_to(0.9);
		elif(event.is_action_released("mouse_select") || (event is InputEventScreenTouch && !event.is_pressed())):
			interpolate_scale_to(1.0);
			select();


func _draw() -> void:
	var style_box : StyleBoxFlat = StyleBoxFlat.new()
	style_box.bg_color = Color(0.4, 0.4, 0.4);
	style_box.set_corner_radius_all(corner_radius);
	draw_style_box(style_box, Rect2(pos, size));


func _on_area_mouse_entered() -> void:
	if(!selectable): return;
	self.emit_signal("mouse_entered_card", word);
	mouse_over = true;
	interpolate_scale_to(1.1);


func _on_area_mouse_exited() -> void:
	if(!selectable): return;
	self.emit_signal("mouse_exited_card");
	mouse_over = false;
	interpolate_scale_to(1.0);


func interpolate_scale_to(val : float) -> void:
	tween.stop_all();
	tween.remove_all();
	tween.interpolate_property(self, "scale", null, Vector2(val, val), 1.0, Tween.TRANS_ELASTIC, Tween.EASE_OUT);
	tween.start();


func select() -> void:
	selectable = false;
	mouse_over = false;
	if(is_answer):
		interpolate_scale_to(1.2);
	else:
		interpolate_scale_to(1.0);
		self.modulate = Color(0.3, 0.3, 0.3);
	self.emit_signal("card_selected", is_answer, word);


func toggle_mouse_over() -> void:
	mouse_over = !mouse_over;


func set_texture(val : Texture) -> void:
	texture = val;
	sprite.texture = val;
	sprite.scale = Vector2(250 / val.get_size().x, 250 / val.get_size().y);
