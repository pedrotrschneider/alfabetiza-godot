class_name Valise1Level
extends Resource

export var level_num : int = 1;
export var question_texture : Texture;
export var question_word : String = ""
export(Array, Texture) var option_textures : Array;
export var option_words : PoolStringArray = ["", "", ""];
