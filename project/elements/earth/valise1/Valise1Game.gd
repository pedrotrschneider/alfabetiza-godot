class_name Valise1Game
extends Node2D

enum Element {
	earth,
	water
}

export(Element) var element = Element.earth;
export(Array, NodePath) var element_visuals;

export(PackedScene) var option_card_scene;
export(PackedScene) var question_card_scene;

export(NodePath) onready var layer_node = self.get_node(layer_node) as Node2D;
export(NodePath) onready var level_label = self.get_node(level_label) as Label;
export(NodePath) onready var back_button = self.get_node(back_button) as Button;
export(NodePath) onready var debug_word_label = self.get_node(debug_word_label) as Label;
export(NodePath) onready var victory_sfx_player = self.get_node(victory_sfx_player) as AudioStreamPlayer;
export(NodePath) onready var fail_sfx_player = self.get_node(fail_sfx_player) as AudioStreamPlayer;
export(NodePath) onready var game_saver = self.get_node(game_saver) as GameSaver;

var level_data : Valise1Level;
var points : int = 3;
var wrong_picks : Array = [];
var time_taken : float = 0;


func _ready() -> void:
	for i in element_visuals.size():
		element_visuals[i] = self.get_node(element_visuals[i]);
	
	for i in element_visuals.size():
		if i != element:
			element_visuals[i].hide();
	
	back_button.connect("pressed", self, "_on_back_button_pressed");
	
	init_game();



func _process(delta: float) -> void:
	time_taken += delta;


func _on_card_selected(is_answer : bool, word : String) -> void:
	if(!is_answer):
		fail_sfx_player.play();
		wrong_picks.append(word)
		points -= 1;
		return;
	victory_sfx_player.play();
	for c in get_children():
		if(c is Valise1OptionCard):
			c.selectable = false;
	var data : Dictionary = {
		"wrong_picks" : wrong_picks
	}
	yield(get_tree().create_timer(0.5), "timeout");
	game_saver.save(PlayerData.player_id, element, MinigameID.VALISE1,\
		level_data.level_num, time_taken, wrong_picks.size(), data);
	yield(game_saver, "game_saved");
	
	var next_level_idx: int = level_data.level_num + 1;
	var file: File = File.new();
	var next_level_path: String = get_level_resources_path() + "level%d.tres" % next_level_idx;
	if file.file_exists(next_level_path):
		level_data = load(next_level_path);
		element = element;
		BarTransition.begin_transition();
		yield(BarTransition, "screen_dimmed");
		reset();
		BarTransition.end_transition();
	else:
		BarTransition.transition_to(get_level_selector_scene_path());
		yield(BarTransition, "screen_dimmed");
		self.queue_free();


func _on_mouse_entered_card(word : String) -> void:
	debug_word_label.text = word;


func _on_mouse_exited_card() -> void:
	debug_word_label.text = "";


func _on_back_button_pressed() -> void:
	BarTransition.transition_to(get_level_selector_scene_path());
	yield(BarTransition, "screen_dimmed");
	self.queue_free();


func reset() -> void:
	points = 3;
	wrong_picks = [];
	time_taken = 0;
	
	for c in layer_node.get_children():
		if c is Node:
			c.queue_free();
	
	init_game();


func init_game() -> void:
	level_label.text = "NÍVEL  %d" % level_data.level_num;
	var new_qcard : Valise1QuestionCard = question_card_scene.instance();
	new_qcard.word = level_data.question_word;
	layer_node.add_child(new_qcard);
	new_qcard.position = Vector2(1920 / 2, 350);
	new_qcard.texture = level_data.question_texture;
	randomize();
	var num_opts : int = level_data.option_textures.size();
	var idx : Array = range(num_opts);
	idx.shuffle();
	var ii : int = 0;
	for i in idx:
		var new_card : Valise1OptionCard = option_card_scene.instance();
		layer_node.add_child(new_card);
		new_card.texture = level_data.option_textures[i];
		new_card.is_answer = i == 0;
		new_card.word = level_data.option_words[i];
		# warning-ignore:return_value_discarded
		new_card.connect("card_selected", self, "_on_card_selected");
		# warning-ignore:return_value_discarded
		new_card.connect("mouse_entered_card", self, "_on_mouse_entered_card");
		# warning-ignore:return_value_discarded
		new_card.connect("mouse_exited_card", self, "_on_mouse_exited_card");
		new_card.position = Vector2(1920 / float(num_opts + 1) * (ii + 1), 800);
		ii += 1;


func get_level_resources_path() -> String:
	match element:
		Element.earth:
			return "res://elements/earth/valise1/levels/";
		Element.water:
			return "res://elements/water/valise1/levels/";
	
	printerr("Invalid element");
	return "";


func get_level_selector_scene_path() -> String:
	match element:
		Element.earth:
			return "res://elements/earth/valise1/Valise1LevelSelector.tscn";
		Element.water:
			return "res://elements/water/valise1/Valise1LevelSelector.tscn";
	
	printerr("Invalid element");
	return "";
