class_name EarthMinigameSelector
extends Control

export(NodePath) onready var food_hunt_button = self.get_node(food_hunt_button) as Button;
export(NodePath) onready var letter_hunt_button = self.get_node(letter_hunt_button) as Button;
export(NodePath) onready var acrofony_button = self.get_node(acrofony_button) as Button;
export(NodePath) onready var valise1_button = self.get_node(valise1_button) as Button;
export(NodePath) onready var valise2_button = self.get_node(valise2_button) as Button;
export(NodePath) onready var valise3_button = self.get_node(valise3_button) as Button;
export(NodePath) onready var rebus_button = self.get_node(rebus_button) as Button;
export(NodePath) onready var back_button = self.get_node(back_button) as Button;

onready var minigame_buttons: Array = [
	$MinigameButtons/FoodHuntButton, 
	$MinigameButtons/LetterHuntButton,
	$MinigameButtons/AcrofonyButton, 
	$MinigameButtons/Valise1Button, 
	$MinigameButtons/Valise2Button, 
	$MinigameButtons/Valise3Button, 
	$MinigameButtons/RebusButton,
];


func _ready() -> void:
	MusicController.play_ambiance(MusicController.Ambiance.EARTH);
	
	food_hunt_button.connect("pressed", self, "_on_food_hunt_button_pressed");
	letter_hunt_button.connect("pressed", self, "_on_letter_hunt_button_pressed");
	acrofony_button.connect("pressed", self, "_on_acrofony_buttton_pressed");
	valise1_button.connect("pressed", self, "_on_valise1_button_pressed");
	valise2_button.connect("pressed", self, "_on_valise2_button_pressed");
	valise3_button.connect("pressed", self, "_on_valise3_button_pressed");
	rebus_button.connect("pressed", self, "_on_rebus_button_pressed");
	
	back_button.connect("pressed", self, "_on_back_button_pressed");
	
	minigame_buttons[0].enabled = true;
	if PlayerData.played_earth_legend:
		for mb in minigame_buttons:
			mb.enabled = true;


func _on_food_hunt_button_pressed() -> void:
	BarTransition.transition_to("res://elements/earth/food_hunt/FoodHuntGame.tscn");


func _on_letter_hunt_button_pressed() -> void:
	BarTransition.transition_to("res://elements/earth/letter_hunt/LetterHuntGame.tscn");


func _on_acrofony_buttton_pressed() -> void:
	BarTransition.transition_to("res://elements/earth/acrofony/AcrofonyLevelManager.tscn");


func _on_valise1_button_pressed() -> void:
	BarTransition.transition_to("res://elements/earth/valise1/Valise1LevelSelector.tscn");


func _on_valise2_button_pressed() -> void:
	BarTransition.transition_to("res://elements/earth/valise2/Valise2LevelSelector.tscn");


func _on_valise3_button_pressed() -> void:
	BarTransition.transition_to("res://elements/earth/valise3/Valise3LevelSelector.tscn");


func _on_rebus_button_pressed() -> void:
	BarTransition.transition_to("res://elements/earth/rebus1/Rebus1LevelSelector.tscn");


func _on_back_button_pressed() -> void:
	BarTransition.transition_to("res://elements/ElementSelector.tscn");
