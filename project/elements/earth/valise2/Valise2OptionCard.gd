class_name Valise2OptionCard
extends Node2D

signal card_selected(is_answer, word);
signal mouse_entered_card(word);
signal mouse_exited_card();

export(NodePath) onready var area = self.get_node(area) as Area2D;
export(NodePath) onready var scale_tween = self.get_node(scale_tween) as Tween;
export(NodePath) onready var position_tween = self.get_node(position_tween) as Tween;
export(NodePath) onready var draggable = self.get_node(draggable) as Draggable;
export(NodePath) onready var audio_stream_player = self.get_node(audio_stream_player) as AudioStreamPlayer;

var size : Vector2 = Vector2(250, 200);
var pos : Vector2 = -size / 2;
var corner_radius : int = 30;

var word : String = "";
var audio : AudioStream;
var is_answer : bool = false;
var mouse_over : bool = false;
var mouse_pressed_over : bool = false;
var selectable : bool = true;
var selected : bool = false;


func _ready() -> void:
	area.connect("mouse_entered", self, "_on_area_mouse_entered");
	area.connect("mouse_exited", self, "_on_area_mouse_exited");
	
	audio_stream_player.stream = audio;


func _process(_delta : float) -> void:
	if(!mouse_pressed_over && !draggable.docked):
		interpolate_to_target();
		if(draggable.dock):
			select();


func _input(event: InputEvent) -> void:
	if(selectable):
		if(mouse_over):
			if(event.is_action_pressed("mouse_select") || (event is InputEventScreenTouch && event.is_pressed())):
				mouse_pressed_over = true;
				draggable.docked = false;
				interpolate_scale_to(0.9);
			elif(event.is_action_released("mouse_select") || (event is InputEventScreenTouch && !event.is_pressed())):
				mouse_pressed_over = false;
				interpolate_scale_to(1.0);
			elif(mouse_pressed_over):
				if(event is InputEventMouseMotion):
					self.position += event.relative;


func _draw() -> void:
	var style_box : StyleBoxFlat = StyleBoxFlat.new()
	style_box.bg_color = Color(0.4, 0.4, 0.4);
	style_box.set_corner_radius_all(corner_radius);
	draw_style_box(style_box, Rect2(pos, size));


func _on_area_mouse_entered() -> void:
	if(!selectable): return;
	if(!audio_stream_player.playing):
		audio_stream_player.play();
	self.emit_signal("mouse_entered_card", word);
	mouse_over = true;
	interpolate_scale_to(1.1);
	if Game.plataform == InitLoader.Plataforms.MOBILE:
		mouse_pressed_over = true;
		draggable.docked = false;
		interpolate_scale_to(0.9);


func _on_area_mouse_exited() -> void:
	if(!selectable): return;
	self.emit_signal("mouse_exited_card");
	mouse_over = false;
	interpolate_scale_to(1.0);
	if Game.plataform == InitLoader.Plataforms.MOBILE:
		mouse_pressed_over = false;
		interpolate_scale_to(1.0);


func interpolate_scale_to(val : float) -> void:
	scale_tween.stop_all();
	scale_tween.remove_all();
	scale_tween.interpolate_property(self, "scale", null, Vector2(val, val), 1.0, Tween.TRANS_ELASTIC, Tween.EASE_OUT);
	scale_tween.start();


func interpolate_to_target() -> void:
	position_tween.stop_all();
	position_tween.remove_all();
	position_tween.interpolate_property(self, "position", null, draggable.target_position, 0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT);
	position_tween.start();
	draggable.docked = true;


func select() -> void:
	selectable = false;
	mouse_over = false;
	if(is_answer):
		interpolate_scale_to(1.0);
	else:
		interpolate_scale_to(1.0);
		draggable.reset();
		interpolate_to_target()
		self.modulate = Color(0.3, 0.3, 0.3);
	self.emit_signal("card_selected", is_answer, word);


func play_audio() -> void:
	audio_stream_player.play();


func toggle_mouse_over() -> void:
	mouse_over = !mouse_over;
