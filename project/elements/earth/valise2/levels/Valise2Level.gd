class_name Valise2Level
extends Resource

export var level_num : int = 1;
export var question_word : String = "";
export var question_audio : AudioStream;
export var num_answers : int = 1;
export var option_words : PoolStringArray = ["", "", ""];
export(Array, AudioStream) var option_audios;
export var hint : String = "";
