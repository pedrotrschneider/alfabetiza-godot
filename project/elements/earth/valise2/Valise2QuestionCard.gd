class_name Valise2QuestionCard
extends Node2D

export(NodePath) onready var area = self.get_node(area) as Area2D;
export(NodePath) onready var scale_tween = self.get_node(scale_tween) as Tween;
export(NodePath) onready var position_tween = self.get_node(position_tween) as Tween;
export(NodePath) onready var debug_question_label = self.get_node(debug_question_label) as Label;
export(NodePath) onready var audio_stream_player = self.get_node(audio_stream_player) as AudioStreamPlayer;

var size : Vector2 = Vector2(250, 200);
var pos : Vector2 = -size / 2;
var corner_radius : int = 30;

var word : String;
var audio : AudioStream;
var selectable : bool = true;
var mouse_over : bool = false;


func _ready() -> void:
	area.connect("mouse_entered", self, "_on_area_mouse_entered");
	area.connect("mouse_exited", self, "_on_area_mouse_exited");
	
	debug_question_label.text = word;
	audio_stream_player.stream = audio;


func _input(event: InputEvent) -> void:
	if(selectable):
		if(mouse_over):
			if(event.is_action_pressed("mouse_select") || (event is InputEventScreenTouch && event.is_pressed())):
				interpolate_scale_to(0.9);
			elif(event.is_action_released("mouse_select") || (event is InputEventScreenTouch && !event.is_pressed())):
				interpolate_scale_to(1.0);


func _draw() -> void:
	var style_box : StyleBoxFlat = StyleBoxFlat.new()
	style_box.bg_color = Color(0.4, 0.4, 0.4);
	style_box.set_corner_radius_all(corner_radius);
	style_box.set_border_width_all(10);
	style_box.set_border_color(Color.black);
	draw_style_box(style_box, Rect2(pos, size));


func _on_area_mouse_entered() -> void:
	if(!selectable): return;
	if(!audio_stream_player.playing):
		audio_stream_player.play();
	mouse_over = true;
	interpolate_scale_to(1.1);


func _on_area_mouse_exited() -> void:
	if(!selectable): return;
	mouse_over = false;
	interpolate_scale_to(1.0);


func interpolate_scale_to(val : float) -> void:
	scale_tween.stop_all();
	scale_tween.remove_all();
	scale_tween.interpolate_property(self, "scale", null, Vector2(val, val), 1.0, Tween.TRANS_ELASTIC, Tween.EASE_OUT);
	scale_tween.start();
