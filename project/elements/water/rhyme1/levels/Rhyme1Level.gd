class_name Rhyme1Level
extends Resource

export var level_num : int = 1;
export var question_texture : Texture;
export var question_word : String = "";
export var question_audio : AudioStream;
export(Array, Texture) var option_textures;
export var option_words : PoolStringArray = ["", "", ""];
export (Array, AudioStream) var option_audios;
