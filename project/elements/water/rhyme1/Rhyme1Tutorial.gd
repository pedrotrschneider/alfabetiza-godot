class_name Rhyme1Tutorial
extends Node2D

export(NodePath) onready var animation_player = self.get_node(animation_player) as AnimationPlayer;
export(NodePath) onready var back_button = self.get_node(back_button) as Button;
export(NodePath) onready var question_card = self.get_node(question_card) as Valise1QuestionCard;
export(NodePath) onready var option_card1 = self.get_node(option_card1) as Valise1OptionCard;
export(NodePath) onready var option_card2 = self.get_node(option_card2) as Valise1OptionCard;
export(NodePath) onready var option_card3 = self.get_node(option_card3) as Valise1OptionCard;

var level_data : Rhyme1Level = load("res://elements/water/rhyme1/levels/tutorial.tres");


func _ready() -> void:
	back_button.connect("pressed", self, "_on_back_button_pressed");
	
	question_card.texture = level_data.question_texture;
	option_card1.texture = level_data.option_textures[0];
	option_card1.selectable = false;
	option_card1.is_answer = true;
	option_card2.texture = level_data.option_textures[1]
	option_card2.selectable = false;
	option_card3.texture = level_data.option_textures[2];
	option_card3.selectable = false;
	
	var dialogue : Node
	dialogue = Dialogic.start("water/rhyme1/rhyme1_tutorial1");
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");
	animation_player.play("p1");
	yield(animation_player, "animation_finished");
	dialogue = Dialogic.start("water/rhyme1/rhyme1_tutorial2");
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");
	yield(get_tree().create_timer(0.5), "timeout");
	
	BarTransition.transition_to("res://elements/water/rhyme1/Rhyme1LevelSelector.tscn");
	yield(BarTransition, "screen_dimmed");
	self.queue_free();


func _on_back_button_pressed() -> void:
	BarTransition.transition_to("res://elements/water/rhyme1/Rhyme1LevelSelector.tscn");
	yield(BarTransition, "screen_dimmed");
	self.queue_free();
