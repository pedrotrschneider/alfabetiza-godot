class_name Rhyme1QuestionCard
extends Node2D

export(NodePath) onready var area = self.get_node(area) as Area2D;
export(NodePath) onready var sprite = self.get_node(sprite) as Sprite;
export(NodePath) onready var debug_question_label = self.get_node(debug_question_label) as Label;
export(NodePath) onready var tween = self.get_node(tween) as Tween;
export(NodePath) onready var bird_stream_player = self.get_node(bird_stream_player) as AudioStreamPlayer;

var size : Vector2 = Vector2(300, 300);
var pos : Vector2 = -size / 2;
var corner_radius : int = 30;

var texture : Texture setget set_texture;
var word : String;
export var audio : AudioStream;

var mouse_over : bool = false;
var selectable : bool = true;
var selected : bool = false;


func _ready() -> void:
	area.connect("mouse_entered", self, "_on_area_mouse_entered");
	area.connect("mouse_exited", self, "_on_area_mouse_exited");
	
	debug_question_label.text = word;
	
	$BirdStreamPlayer.stream = audio;


func _input(event: InputEvent) -> void:
	if(selectable && mouse_over):
		if(event.is_action_pressed("mouse_select") || (event is InputEventScreenTouch && event.is_pressed())):
			interpolate_scale_to(0.9);
		elif(event.is_action_released("mouse_select") || (event is InputEventScreenTouch && !event.is_pressed())):
			interpolate_scale_to(1.0);
			if not bird_stream_player.playing:
				bird_stream_player.play();


func _draw() -> void:
	var style_box : StyleBoxFlat = StyleBoxFlat.new()
	style_box.bg_color = Color(0.4, 0.4, 0.4);
	style_box.set_corner_radius_all(corner_radius);
	style_box.set_border_width_all(10);
	style_box.set_border_color(Color.black);
	draw_style_box(style_box, Rect2(pos, size));


func _on_area_mouse_entered() -> void:
	if(!selectable): return;
	mouse_over = true;
	interpolate_scale_to(1.1);


func _on_area_mouse_exited() -> void:
	if(!selectable): return;
	mouse_over = false;
	interpolate_scale_to(1.0);


func set_texture(val : Texture) -> void:
	texture = val;
	sprite.texture = val;
	sprite.scale = Vector2(250 / val.get_size().x, 250 / val.get_size().y);


func interpolate_scale_to(val : float) -> void:
	tween.stop_all();
	tween.remove_all();
	tween.interpolate_property(self, "scale", null, Vector2(val, val), 1.0, Tween.TRANS_ELASTIC, Tween.EASE_OUT);
	tween.start();
