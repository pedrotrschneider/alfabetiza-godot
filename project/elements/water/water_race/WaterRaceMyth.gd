extends Control

export(NodePath) onready var continue_button = self.get_node(continue_button) as Button;
 

func _ready() -> void:
	continue_button.connect("pressed", self, "_on_continue_button_pressed");


func _on_continue_button_pressed() -> void:
	BarTransition.transition_to("res://elements/water/WaterMinigameSelector.tscn");
