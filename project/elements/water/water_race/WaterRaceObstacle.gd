class_name WaterRaceObstacle
extends Area2D

signal obstacle_dodged();

var speed : float = 400.0;


func _physics_process(delta: float) -> void:
	self.position.y += speed * delta;
	
	if self.position.y >= 1080 + 20:
		self.emit_signal("obstacle_dodged");
		self.queue_free();
