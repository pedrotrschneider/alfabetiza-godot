class_name WaterRace
extends Node2D

signal game_ended();

export(PackedScene) var obstacle_scene;
export(NodePath) onready var back_button = self.get_node(back_button) as Button;
export(NodePath) onready var countdown_label = self.get_node(countdown_label) as Label;
export(NodePath) onready var time_label = self.get_node(time_label) as Label;
export(NodePath) onready var clock_icon = self.get_node(clock_icon) as TextureRect;
export(NodePath) onready var points_label = self.get_node(points_label) as Label;
export(NodePath) onready var bucket_icon = self.get_node(bucket_icon) as TextureRect;
export(Array, NodePath) var life_icons : Array;
export(NodePath) onready var countdown_timer = self.get_node(countdown_timer) as Timer;
export(NodePath) onready var game_timer = self.get_node(game_timer) as Timer;
export(NodePath) onready var spawn_timer = self.get_node(spawn_timer) as Timer;
export(NodePath) onready var player = self.get_node(player) as WaterRacePlayer;

var points : int = 0;
var game_started : bool = false;
var game_over : bool = false;


func _ready() -> void:
	for i in life_icons.size():
		life_icons[i] = self.get_node(life_icons[i]);
	
	# warning-ignore:return_value_discarded
	self.connect("game_ended", player, "_end_game");
	
	player.connect("player_hit", self, "_on_player_hit");
	player.connect("player_lost", self, "_on_player_lost");
	back_button.connect("pressed", self, "_on_back_button_pressed");
	countdown_timer.connect("timeout", self, "_on_countdown_timer_timeout");
	game_timer.connect("timeout", self, "_on_game_timer_timeout");
	spawn_timer.connect("timeout", self, "_on_spawn_timer_timeout");
	
	update();
	
	time_label.hide();
	clock_icon.hide();
	points_label.hide();
	bucket_icon.hide();


func _process(_delta):
	countdown_label.text = "%d\nCLIQUE COM O BOTÃO ESQUERDO\nE DESLIZE O MOUSE!" % countdown_timer.time_left;
	if(countdown_timer.time_left <= 1):
		countdown_label.text = "DESVIE DOS OBSTÁCULOS!";
	time_label.text = "%.1f" % game_timer.time_left;
	points_label.text = "%d" % points;


func _on_obstacle_dodged() -> void:
	points += 3;


func _on_player_hit() -> void:
	if(life_icons.size() > 0):
		life_icons[life_icons.size() - 1].queue_free();
		life_icons.pop_back();


func _on_player_lost() -> void:
	game_over = true;
#	yield(self.get_tree().create_timer(4.0), "timeout");
	self.emit_signal("game_ended");
	var dialogue : Node = Dialogic.start("water_race_lost");
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");
	yield(self.get_tree().create_timer(0.5), "timeout");
	BarTransition.transition_to("res://elements/water/WaterMinigameSelector.tscn");


func _on_countdown_timer_timeout() -> void:
	game_timer.start();
	spawn_timer.start();
	countdown_label.hide();
	time_label.show();
	clock_icon.show();
	points_label.show();
	bucket_icon.show();


func _on_spawn_timer_timeout() -> void:
	randomize();
	var new_obstacle : WaterRaceObstacle = obstacle_scene.instance();
	self.add_child(new_obstacle);
	# warning-ignore:return_value_discarded
	new_obstacle.connect("obstacle_dodged", self, "_on_obstacle_dodged");
	new_obstacle.position.y = -40;
	new_obstacle.position.x = 1920 / 2 + (500 * (randf() - 0.5) * 2);
	
	if(!game_over):
		spawn_timer.start();


func _on_game_timer_timeout() -> void:
	game_over = true;
	yield(self.get_tree().create_timer(4.0), "timeout");
	self.emit_signal("game_ended");
	var dialogue : Node = Dialogic.start("water_race_end");
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");
	yield(self.get_tree().create_timer(0.5), "timeout");
	BarTransition.transition_to("res://elements/water/water_race/WaterRaceMyth.tscn");


func _on_back_button_pressed() -> void:
	BarTransition.transition_to("res://elements/water/WaterMinigameSelector.tscn");
