class_name WaterRacePlayer
extends KinematicBody2D

signal player_hit();
signal player_lost();

export(NodePath) onready var hitbox_area = self.get_node(hitbox_area) as Area2D;
export(NodePath) onready var sprite = self.get_node(sprite) as Sprite; 

var lives : int = 3;
var target_pos : Vector2 = Vector2.ZERO;
var acceleration : float = 100.0;
var speed : float = 200.0;
var velocity : Vector2 = Vector2.ZERO;
var game_ended : bool = false;


func _ready() -> void:
	hitbox_area.connect("area_entered", self, "_on_hitbox_area_entered")
	
	target_pos = self.global_position;


func _unhandled_input(event: InputEvent) -> void:
	if(event is InputEventScreenDrag):
		target_pos = event.position;
	elif(event is InputEventMouseButton && event.button_index == BUTTON_LEFT && event.is_pressed()
		|| (Input.is_action_pressed("mouse_select")\
		&& event is InputEventMouseMotion)):
		target_pos = get_global_mouse_position();


func _physics_process(delta: float) -> void:
	velocity = lerp(velocity, (target_pos - self.global_position) * speed, acceleration * delta);
	
	if(!game_ended):
		# warning-ignore:return_value_discarded
		self.move_and_slide(velocity * delta, Vector2.UP);


func _process(_delta: float) -> void:
	if abs(velocity.x) > 0:
		$Sprite.scale.x = sign((target_pos - $Sprite.global_position).x) * abs($Sprite.scale.x);


func _on_hitbox_area_entered(area : Area2D) -> void:
	if(!game_ended and area is WaterRaceObstacle):
		area.queue_free();
		sprite.modulate = Color.red;
		self.emit_signal("player_hit");
		lives -= 1;
		if(lives == 0):
			self.emit_signal("player_lost");
		yield(self.get_tree().create_timer(1.0), "timeout");
		sprite.modulate = Color.white;


func _end_game() -> void:
	game_ended = true;
