class_name WaterMinigameSelector
extends Control

export(NodePath) onready var race_button = self.get_node(race_button) as Button;
export(NodePath) onready var syllable_swap_button = self.get_node(syllable_swap_button) as Button;
export(NodePath) onready var valise1_button = self.get_node(valise1_button) as Button;
export(NodePath) onready var valise2_button = self.get_node(valise2_button) as Button;
export(NodePath) onready var valise3_button = self.get_node(valise3_button) as Button;
export(NodePath) onready var rebus_button = self.get_node(rebus_button) as Button;
export(NodePath) onready var rhyme_button = self.get_node(rhyme_button) as Button;
export(NodePath) onready var back_button = self.get_node(back_button) as Button;


func _ready() -> void:
	MusicController.play_ambiance(MusicController.Ambiance.WATER);
	
	race_button.connect("pressed", self, "_on_race_button_pressed");
	syllable_swap_button.connect("pressed", self, "_on_syllable_swap_button_pressed");
	valise1_button.connect("pressed", self, "_on_valise1_button_pressed");
	valise2_button.connect("pressed", self, "_on_valise2_button_pressed");
	valise3_button.connect("pressed", self, "_on_valise3_button_pressed");
	rebus_button.connect("pressed", self, "_on_rebus_button_pressed");
	rhyme_button.connect("pressed", self, "_on_rhyme_buttton_pressed");
	
	back_button.connect("pressed", self, "_on_back_button_pressed")


func _on_race_button_pressed() -> void:
	BarTransition.transition_to("res://elements/water/water_race/WaterRaceGame.tscn");


func _on_syllable_swap_button_pressed() -> void:
	BarTransition.transition_to("res://elements/water/syllable_swap/SyllableSwapLevelSelector.tscn");


func _on_valise1_button_pressed() -> void:
	BarTransition.transition_to("res://elements/water/valise1/Valise1LevelSelector.tscn");


func _on_valise2_button_pressed() -> void:
	BarTransition.transition_to("res://elements/water/valise2/Valise2LevelSelector.tscn");


func _on_valise3_button_pressed() -> void:
	BarTransition.transition_to("res://elements/water/valise3/Valise3LevelSelector.tscn");


func _on_rebus_button_pressed() -> void:
	BarTransition.transition_to("res://elements/water/rebus2/Rebus2LevelSelector.tscn");


func _on_rhyme_buttton_pressed() -> void:
	BarTransition.transition_to("res://elements/water/rhyme1/Rhyme1LevelSelector.tscn");


func _on_back_button_pressed() -> void:
	BarTransition.transition_to("res://elements/ElementSelector.tscn");
