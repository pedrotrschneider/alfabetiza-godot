class_name SyllableSwapGame
extends Node2D

export(PackedScene) var option_card_scene;
export(PackedScene) var question_card_scene;
export(PackedScene) var question_syllable_scene;
export(NodePath) onready var level_label = self.get_node(level_label) as Label;
export(NodePath) onready var back_button = self.get_node(back_button) as Button;
export(NodePath) onready var debug_word_label = self.get_node(debug_word_label) as Label;
export(NodePath) onready var victory_sfx_player = self.get_node(victory_sfx_player) as AudioStreamPlayer;
export(NodePath) onready var fail_sfx_player = self.get_node(fail_sfx_player) as AudioStreamPlayer;
export(NodePath) onready var question_word_player = self.get_node(question_word_player) as AudioStreamPlayer;
export(NodePath) onready var game_saver = self.get_node(game_saver) as GameSaver;

var level_data : SyllableSwapLevel;
var num_answers : int = 0;
var points : int = 3;
var wrong_picks : Array = [];
var time_taken : float = 0;


func _ready() -> void:
	back_button.connect("pressed", self, "_on_back_button_pressed");
	
	init_game();


func _process(delta: float) -> void:
	time_taken += delta;


func _on_card_selected(opt_card : SyllableSwapOptionCard, dock_index : int) -> void:
	opt_card._select_answer(dock_index == opt_card.index);
	if(dock_index != opt_card.index):
		fail_sfx_player.play();
		wrong_picks.append(opt_card.word)
		# warning-ignore:narrowing_conversion
		points = max(0, points - 1);
		return;
	victory_sfx_player.play();
	question_word_player.stream = level_data.question_audios[opt_card.index];
	question_word_player.play();
	opt_card.selectable = false;
	
	num_answers -= 1;
	if(num_answers == 0):
		for c in get_children():
			if(c is SyllableSwapOptionCard):
				c.selectable = false;
		var data : Dictionary = {
			"wrong_picks" : wrong_picks
		}
		yield(get_tree().create_timer(0.5), "timeout");
		game_saver.save(PlayerData.player_id, ElementID.WATER, MinigameID.SYLLABLE_SWAP,\
			level_data.level_num, time_taken, wrong_picks.size(), data);
		yield(game_saver, "game_saved");
		
		var next_level_idx: int = level_data.level_num + 1;
		var file: File = File.new();
		var next_level_path: String = "res://elements/water/syllable_swap/levels/level%d.tres" % next_level_idx;
		if file.file_exists(next_level_path):
			level_data = load(next_level_path);
			BarTransition.begin_transition();
			yield(BarTransition, "screen_dimmed");
			reset();
			BarTransition.end_transition();
		else:
			BarTransition.begin_transition();
			yield(BarTransition, "screen_dimmed");
			# warning-ignore:return_value_discarded
			get_tree().change_scene("res://elements/water/syllable_swap/SyllableSwapLevelSelector.tscn");
			self.queue_free();
			BarTransition.end_transition();


func reset() -> void:
	num_answers = 0;
	points = 3;
	wrong_picks = [];
	time_taken = 0;
	
	for c in self.get_children():
		if c is SyllableSwapOptionCard \
		or c is SyllableSwapQuestionCard \
		or c is SyllableSwapSyllableQuestionCard:
			c.queue_free();
	
	init_game();


func init_game() -> void: 
	level_label.text = "NÍVEL  %d" % level_data.level_num;
	
	var num_q_cards : int = level_data.question_textures.size();
	num_answers = num_q_cards;
	for i in num_q_cards:
		var texture : Texture = level_data.question_textures[i];
		var word : String = level_data.question_words[i];
		var new_q_card : SyllableSwapQuestionCard = question_card_scene.instance();
		self.add_child(new_q_card);
		new_q_card.texture = texture;
		new_q_card.word = word;
		new_q_card.position.y = 300;
		new_q_card.position.x = 1920 / float(num_q_cards + 1) * (i + 1);
	
	for i in num_q_cards:
		var syllable : String = level_data.question_syllable;
		var new_q_card : SyllableSwapSyllableQuestionCard = question_syllable_scene.instance();
		new_q_card.invert = level_data.invert;
		new_q_card.position.y = 600;
		new_q_card.position.x = 1920 / float(num_q_cards + 1) * (i + 1);
		self.add_child(new_q_card);
		new_q_card.dock.dock_index = i;
		new_q_card.word = syllable;
	
	var num_o_cards : int = level_data.option_syllables.size();
	var idx : Array = range(num_o_cards);
	randomize();
	idx.shuffle();
	var ii : int = 0;
	for i in idx:
		var syllable : String = level_data.option_syllables[i];
		var new_o_card : SyllableSwapOptionCard = option_card_scene.instance();
		self.add_child(new_o_card);
		new_o_card.index = i;
		new_o_card.word = syllable;
		new_o_card.position.y = 900;
		new_o_card.position.x = 1920 / float(num_o_cards + 1) * (ii + 1);
		# warning-ignore:return_value_discarded
		new_o_card.connect("card_selected", self, "_on_card_selected");
		ii += 1;


func _on_mouse_entered_card(word : String) -> void:
	debug_word_label.text = word;


func _on_mouse_exited_card() -> void:
	debug_word_label.text = "";


func _on_back_button_pressed() -> void:
	BarTransition.begin_transition();
	yield(BarTransition, "screen_dimmed");
	# warning-ignore:return_value_discarded
	get_tree().change_scene("res://elements/water/syllable_swap/SyllableSwapLevelSelector.tscn");
	self.queue_free();
	BarTransition.end_transition();
