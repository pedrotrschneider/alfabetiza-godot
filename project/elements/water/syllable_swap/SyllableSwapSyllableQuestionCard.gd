class_name SyllableSwapSyllableQuestionCard
extends Node2D

export(NodePath) onready var label = self.get_node(label) as Label;
export(NodePath) onready var area = self.get_node(area) as Area2D;
export(NodePath) onready var dock = self.get_node(dock) as Dock;
export var invert : bool = false;

var size : Vector2 = Vector2(200, 200);
var pos : Vector2 = -size / 2 + Vector2(110, 0);
var corner_radius : int = 30;

var word : String setget set_word;


func _ready() -> void:
	label.text = word;
	
	if(invert):
		pos.x -= 220;
		label.rect_position.x -= 220;
		area.position.x -= 220;
		dock.position.x += 220;


func _draw() -> void:
	var style_box : StyleBoxFlat = StyleBoxFlat.new()
	style_box.bg_color = Color(0.4, 0.4, 0.4);
	style_box.set_corner_radius_all(corner_radius);
	style_box.set_border_width_all(10);
	style_box.set_border_color(Color.black);
	draw_style_box(style_box, Rect2(pos, size));


func set_word(val : String) -> void:
	word = val;
	label.text = val.to_upper();
