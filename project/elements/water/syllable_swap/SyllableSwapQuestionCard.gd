class_name SyllableSwapQuestionCard
extends Node2D

export(NodePath) onready var sprite = self.get_node(sprite) as Sprite;
export(NodePath) onready var debug_question_label = self.get_node(debug_question_label) as Label;

var size : Vector2 = Vector2(300, 300);
var pos : Vector2 = -size / 2;
var corner_radius : int = 30;

var texture : Texture setget set_texture;
var word : String;


func _ready() -> void:
	debug_question_label.text = word;


func _draw() -> void:
	var style_box : StyleBoxFlat = StyleBoxFlat.new()
	style_box.bg_color = Color(0.4, 0.4, 0.4);
	style_box.set_corner_radius_all(corner_radius);
	style_box.set_border_width_all(10);
	style_box.set_border_color(Color.black);
	draw_style_box(style_box, Rect2(pos, size));


func set_texture(val : Texture) -> void:
	texture = val;
	sprite.texture = val;
	sprite.scale = Vector2(250 / val.get_size().x, 250 / val.get_size().y);
