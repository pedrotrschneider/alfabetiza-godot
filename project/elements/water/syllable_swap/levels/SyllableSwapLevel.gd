class_name SyllableSwapLevel
extends Resource

export(int) var level_num = 0;
export(Array, String) var question_words = [];
export(Array, Texture) var question_textures = [];
export(Array, AudioStream) var question_audios = [];
export(String) var question_syllable = ""
export(bool) var invert  = false;
export(Array, String) var option_syllables = [];
