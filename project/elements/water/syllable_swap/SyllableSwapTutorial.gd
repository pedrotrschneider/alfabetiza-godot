class_name SyllableSwapTutorial
extends Node2D

export(NodePath) onready var question_card1 = self.get_node(question_card1) as SyllableSwapQuestionCard;
export(NodePath) onready var question_card2 = self.get_node(question_card2) as SyllableSwapQuestionCard;
export(NodePath) onready var syllable_question_card1 = self.get_node(syllable_question_card1) as SyllableSwapSyllableQuestionCard;
export(NodePath) onready var syllable_question_card2 = self.get_node(syllable_question_card2) as SyllableSwapSyllableQuestionCard;
export(NodePath) onready var option_card1 = self.get_node(option_card1) as SyllableSwapOptionCard;
export(NodePath) onready var option_card2 = self.get_node(option_card2) as SyllableSwapOptionCard;
export(NodePath) onready var option_card3 = self.get_node(option_card3) as SyllableSwapOptionCard;
export(NodePath) onready var question_word_player1 = self.get_node(question_word_player1) as AudioStreamPlayer;
export(NodePath) onready var question_word_player2 = self.get_node(question_word_player2) as AudioStreamPlayer;
export(NodePath) onready var animation_player = self.get_node(animation_player) as AnimationPlayer;
export(NodePath) onready var back_button = self.get_node(back_button) as Button;

var level_data := load("res://elements/water/syllable_swap/levels/tutorial.tres") as SyllableSwapLevel;


func _ready() -> void:
	back_button.connect("pressed", self, "_on_back_button_pressed");
	
	question_card1.texture = level_data.question_textures[0];
	question_card2.texture = level_data.question_textures[1];
	syllable_question_card1.word = level_data.question_syllable;
	syllable_question_card2.word = level_data.question_syllable;
	option_card1.word = level_data.option_syllables[0];
	option_card2.word = level_data.option_syllables[1];
	option_card3.word = level_data.option_syllables[2];
	
	question_word_player1.stream = level_data.question_audios[0];
	question_word_player2.stream = level_data.question_audios[1];
	
	var dialogue : Node = Dialogic.start("syllable_swap_tutorial1");
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");
	animation_player.play("p1");
	yield(animation_player, "animation_finished");
	dialogue = Dialogic.start("syllable_swap_tutorial2");
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");
	
	yield(get_tree().create_timer(0.5), "timeout");
	BarTransition.transition_to("res://elements/water/syllable_swap/SyllableSwapLevelSelector.tscn");


func _on_back_button_pressed() -> void:
	BarTransition.transition_to("res://elements/water/syllable_swap/SyllableSwapLevelSelector.tscn");
