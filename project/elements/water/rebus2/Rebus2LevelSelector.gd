class_name Rebus2LevelSelector
extends Control

export(PackedScene) var rebus2_game_scene;
export(NodePath) onready var back_button = self.get_node(back_button) as Button;
export(NodePath) onready var tutorial_button = self.get_node(tutorial_button) as Button;
export(NodePath) onready var level1_button = self.get_node(level1_button) as Button;
export(NodePath) onready var level2_button = self.get_node(level2_button) as Button;
export(NodePath) onready var level3_button = self.get_node(level3_button) as Button;
export(NodePath) onready var level4_button = self.get_node(level4_button) as Button;
export(NodePath) onready var level5_button = self.get_node(level5_button) as Button;
export(NodePath) onready var level6_button = self.get_node(level6_button) as Button;
export(NodePath) onready var level7_button = self.get_node(level7_button) as Button;

var num_levels : int = 7;


func _ready() -> void:
	back_button.connect("pressed", self, "_on_back_button_pressed");
	tutorial_button.connect("pressed", self, "_on_tutorial_button_pressed");
	
	for i in num_levels:
		var ii : int = i + 1;
		self["level%d_button" % ii].connect("pressed", self, "_on_level_button_pressed", [ii]);


func _on_level_button_pressed(level : int) -> void:
	BarTransition.begin_transition();
	yield(BarTransition, "screen_dimmed");
	var new_game : Rebus2Game = rebus2_game_scene.instance();
	new_game.level_data = load("res://elements/water/rebus2/levels/level%d.tres" % level);
	self.get_parent().add_child(new_game);
	self.queue_free();
	BarTransition.end_transition();


func _on_tutorial_button_pressed() -> void:
	BarTransition.transition_to("res://elements/water/rebus2/Rebus2Tutorial.tscn");


func _on_back_button_pressed() -> void:
	BarTransition.transition_to("res://elements/water/WaterMinigameSelector.tscn");
