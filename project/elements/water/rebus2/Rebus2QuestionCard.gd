class_name Rebus2QuestionCard
extends Node2D

export(NodePath) onready var sprite = get_node(sprite) as Sprite;
export(NodePath) onready var label1 = get_node(label1) as Label;
export(NodePath) onready var label2 = get_node(label2) as Label;
export(NodePath) onready var animation_player = self.get_node(animation_player) as AnimationPlayer;


var size : Vector2 = Vector2(300, 400);
var pos : Vector2 = -size / 2;
var corner_radius : int = 30;

var texture : Texture setget set_texture;
var word : String setget set_word;
var w1 : String setget set_w1;
var w2 : String setget set_w2;


func _ready() -> void:
	label1.theme.default_font.outline_size = 0;


func _draw() -> void:
	var style_box : StyleBoxFlat = StyleBoxFlat.new();
	style_box.bg_color = Color(0.4, 0.4, 0.4);
	style_box.set_corner_radius_all(corner_radius);
	self.draw_style_box(style_box, Rect2(pos, size));


func glow() -> void:
	animation_player.play("glow");


func set_texture(val : Texture) -> void:
	texture = val;
	sprite.texture = val;
	sprite.scale = Vector2(250 / val.get_size().x, 250 / val.get_size().y);


func set_word(val : String) -> void:
	word = val;


func set_w1(val : String) -> void:
	label1.text = val;
	w1 = val;


func set_w2(val : String) -> void:
	label2.text = val;
	w2 = val;
