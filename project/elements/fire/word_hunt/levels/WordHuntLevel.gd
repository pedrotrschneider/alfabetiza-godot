class_name WordHuntLevel
extends Resource

export(int) var level_num = 0;
export(String) var question_word = "";
export(Texture) var question_texture;
export(AudioStream) var question_audio;
export(Array, String) var syllables = [];
export(int) var num_answers = 1;
