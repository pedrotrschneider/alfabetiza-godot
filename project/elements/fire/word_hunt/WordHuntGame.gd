class_name WordHuntGame
extends Node2D

export(PackedScene) var option_card_scene;
export(PackedScene) var question_card_scene;
export(PackedScene) var syllable_dock_scene;
export(NodePath) onready var level_label = self.get_node(level_label) as Label;
export(NodePath) onready var answer_label = self.get_node(answer_label) as Label;
export(NodePath) onready var back_button = self.get_node(back_button) as Button;
export(NodePath) onready var debug_word_label = self.get_node(debug_word_label) as Label;
export(NodePath) onready var victory_sfx_player = self.get_node(victory_sfx_player) as AudioStreamPlayer;
export(NodePath) onready var fail_sfx_player = self.get_node(fail_sfx_player) as AudioStreamPlayer;
export(NodePath) onready var game_saver = self.get_node(game_saver) as GameSaver;

var level_data : WordHuntLevel;
var num_answers : int = 0;
var points : int = 3;
var wrong_picks : Array = [];
var time_taken : float = 0;


func _ready() -> void:
	back_button.connect("pressed", self, "_on_back_button_pressed");
	
	init_game();


func _process(delta: float) -> void:
	time_taken += delta;


func _on_card_selected(opt_card : WordHuntOptionCard, dock_index : int) -> void:
	opt_card._select_answer(dock_index == opt_card.index);
	if(dock_index != opt_card.index):
		fail_sfx_player.play();
		wrong_picks.append(opt_card.word)
		# warning-ignore:narrowing_conversion
		points = max(0, points - 1);
		return;
	victory_sfx_player.play();
	opt_card.selectable = false;
	
	num_answers -= 1;
	if(num_answers == 0):
		for c in get_children():
			if(c is WordHuntOptionCard):
				c.selectable = false;
		var data : Dictionary = {
			"wrong_picks" : wrong_picks
		}
		yield(self.get_tree().create_timer(0.5), "timeout");
		answer_label.show();
		yield(get_tree().create_timer(0.5), "timeout");
		game_saver.save(PlayerData.player_id, ElementID.FIRE, MinigameID.WORD_HUNT,\
			level_data.level_num, time_taken, wrong_picks.size(), data);
		yield(game_saver, "game_saved");
		
		var next_level_idx: int = level_data.level_num + 1;
		var file: File = File.new();
		var next_level_path: String = "res://elements/fire/word_hunt/levels/level%d.tres" % next_level_idx;
		if file.file_exists(next_level_path):
			level_data = load(next_level_path);
			BarTransition.begin_transition();
			yield(BarTransition, "screen_dimmed");
			reset();
			BarTransition.end_transition();
		else:
			BarTransition.begin_transition();
			yield(BarTransition, "screen_dimmed");
			# warning-ignore:return_value_discarded
			get_tree().change_scene("res://elements/fire/word_hunt/WordHuntLevelSelector.tscn");
			self.queue_free();
			BarTransition.end_transition();


func reset() -> void:
	num_answers = 0;
	points = 3;
	wrong_picks = [];
	time_taken = 0;
	
	for c in self.get_children():
		if c is WordHuntSyllableDock \
		or c is WordHuntOptionCard \
		or c is WordHuntQuestionCard:
			c.queue_free();
	
	init_game();


func init_game() -> void:
	level_label.text = "NÍVEL  %d" % level_data.level_num;
	answer_label.hide();
	answer_label.text = level_data.question_word.to_upper();
	
	num_answers = level_data.num_answers;
	
	var new_q_card : WordHuntQuestionCard = question_card_scene.instance();
	self.add_child(new_q_card);
	new_q_card.texture = level_data.question_texture;
	new_q_card.word = level_data.question_word;
	new_q_card.position.x = 400;
	new_q_card.position.y = 400;
	
	for i in num_answers:
		var new_dock : WordHuntSyllableDock = syllable_dock_scene.instance();
		new_dock.position.y = 850;
		new_dock.position.x = 200 + 300 * i;
		self.add_child(new_dock);
		new_dock.dock.dock_index = i;
	
	var num_o_cards : int = level_data.syllables.size();
	var idx : Array = range(num_o_cards);
	randomize();
	idx.shuffle();
	var cards_in_rows : Array = [
		min(num_o_cards, 3),
		max(num_o_cards - 3, 0)
	];
	var total_width : float = 1250.0;
	var card_size : float = 200.0;
	var ii : int = 0;
	for i in idx:
		var new_o_card : WordHuntOptionCard = option_card_scene.instance();
		self.add_child(new_o_card);
		new_o_card.index = i;
		new_o_card.word = level_data.syllables[i];
		# warning-ignore:integer_division
		var v_index : int = ii / 3;
		var h_index : int = ii % 3;
		new_o_card.position.y = 300 + 250 * v_index;
		new_o_card.position.x = 600 + (total_width - cards_in_rows[v_index] * card_size) / 2.0 \
								+ total_width / float(cards_in_rows[v_index] + 1) * h_index;
		# warning-ignore:return_value_discarded
		new_o_card.connect("card_selected", self, "_on_card_selected");
		ii += 1;
	
	$QuestionWordPlayer.stream = level_data.question_audio;
	yield(self.get_tree().create_timer(0.5), "timeout");
	$QuestionWordPlayer.play();


func _on_back_button_pressed() -> void:
	BarTransition.begin_transition();
	yield(BarTransition, "screen_dimmed");
	# warning-ignore:return_value_discarded
	get_tree().change_scene("res://elements/fire/word_hunt/WordHuntLevelSelector.tscn");
	self.queue_free();
	BarTransition.end_transition();
