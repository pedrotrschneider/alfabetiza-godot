class_name WordHuntTutorial
extends Node2D

export(NodePath) onready var answer_label = self.get_node(answer_label) as Label;
export(NodePath) onready var question_card = self.get_node(question_card) as WordHuntQuestionCard;
export(NodePath) onready var option_card1 = self.get_node(option_card1) as WordHuntOptionCard;
export(NodePath) onready var option_card2 = self.get_node(option_card2) as WordHuntOptionCard;
export(NodePath) onready var option_card3 = self.get_node(option_card3) as WordHuntOptionCard;
export(NodePath) onready var option_card4 = self.get_node(option_card4) as WordHuntOptionCard;
export(NodePath) onready var option_card5 = self.get_node(option_card5) as WordHuntOptionCard;
export(NodePath) onready var syllable_dock1 = self.get_node(syllable_dock1) as WordHuntSyllableDock;
export(NodePath) onready var syllable_dock2 = self.get_node(syllable_dock2) as WordHuntSyllableDock;
export(NodePath) onready var syllable_dock3 = self.get_node(syllable_dock3) as WordHuntSyllableDock;
export(NodePath) onready var animation_player = self.get_node(animation_player) as AnimationPlayer;
export(NodePath) onready var back_button = self.get_node(back_button) as Button;

var level_data := load("res://elements/fire/word_hunt/levels/tutorial.tres") as WordHuntLevel;


func _ready() -> void:
	back_button.connect("pressed", self, "_on_back_button_pressed");
	
	answer_label.hide();
	answer_label.text = level_data.question_word.to_upper();
	
	question_card.texture = level_data.question_texture;
	option_card1.word = level_data.syllables[4];
	option_card2.word = level_data.syllables[0];
	option_card3.word = level_data.syllables[3];
	option_card4.word = level_data.syllables[2];
	option_card5.word = level_data.syllables[1];
	
	option_card1.selectable = false;
	option_card2.selectable = false;
	option_card3.selectable = false;
	option_card4.selectable = false;
	option_card5.selectable = false;
	
	var dialogue : Node = Dialogic.start("word_hunt_tutorial1");
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");
	animation_player.play("p1");
	yield(animation_player, "animation_finished");
	dialogue = Dialogic.start("word_hunt_tutorial2");
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");
	
	yield(get_tree().create_timer(0.5), "timeout");
	BarTransition.transition_to("res://elements/fire/word_hunt/WordHuntLevelSelector.tscn");


func _on_back_button_pressed() -> void:
	BarTransition.transition_to("res://elements/fire/word_hunt/WordHuntLevelSelector.tscn");
