extends HBoxContainer

signal next_screen(current_id);

export(NodePath) onready var instruction_label = self.get_node(instruction_label) as Label;
export(NodePath) onready var play_button = self.get_node(play_button) as Button;
export(NodePath) onready var record_button = self.get_node(record_button) as Button;
export(NodePath) onready var play_record_button = self.get_node(play_record_button) as Button;
export(NodePath) onready var continue_button = self.get_node(continue_button) as Button;
export(NodePath) onready var tongue_twister_player = self.get_node(tongue_twister_player) as AudioStreamPlayer;
export(NodePath) onready var audio_stream_record_player = self.get_node(audio_stream_record_player) as AudioStreamPlayer;
export(NodePath) onready var animation_player = self.get_node(animation_player) as AnimationPlayer;

var effect: AudioEffectRecord;
var recording: AudioStreamSample;

var id: int = 2;


func start() -> void:
	play_button.connect("pressed", self, "_on_play_button_pressed");
	record_button.connect("pressed", self, "_on_record_button_pressed");
	play_record_button.connect("pressed", self, "_on_play_record_button_pressed");
	continue_button.connect("pressed", self, "_on_continue_button_pressed");
	
	var idx = AudioServer.get_bus_index("Record");
	effect = AudioServer.get_bus_effect(idx, 0);
	
	var dialogue : Node = Dialogic.start("tongue_twister2");
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");


func _on_play_button_pressed() -> void:
	if(!tongue_twister_player.playing):
		tongue_twister_player.play();
		yield(tongue_twister_player, "finished");
		record_button.disabled = false;
		instruction_label.text = "PRESSIONE O BOTÃO DO MEIO\nPARA GRAVAR SUA VOZ"


func _on_record_button_pressed() -> void:
	if(effect.is_recording_active()): # End recording
		animation_player.play("end-record");
		recording = effect.get_recording();
		effect.set_recording_active(false);
		play_record_button.disabled = false;
		instruction_label.text = "PRESSIONE O BOTÃO DA DIREITA\nPARA OUVIR SUA VOZ"
	else: # Start recording
		animation_player.play("start-record");
		effect.set_recording_active(true);
		play_record_button.disabled = true;


func _on_play_record_button_pressed() -> void:
	if(!audio_stream_record_player.playing):
		record_button.disabled = true;
		audio_stream_record_player.stream = recording;
		audio_stream_record_player.play();
		yield(audio_stream_record_player, "finished");
		record_button.disabled = false;
		continue_button.disabled = false;


func _on_continue_button_pressed() -> void:
	continue_button.disabled = true;
	self.emit_signal("next_screen", id);
