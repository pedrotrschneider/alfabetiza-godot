class_name TongueTwisterAnimationCard
extends Node2D

var size : Vector2 = Vector2(500, 300);
var pos : Vector2 = Vector2(0.0, -75) - size / 2;
var corner_radius : int = 30;

func _process(_delta: float) -> void:
	update();

func _draw() -> void:
	var style_box : StyleBoxFlat = StyleBoxFlat.new()
	style_box.bg_color = Color(0.4, 0.4, 0.4);
	style_box.set_corner_radius_all(corner_radius);
	style_box.set_border_width_all(10);
	style_box.set_border_color(Color.black);
	draw_style_box(style_box, Rect2(pos, size));


func play() -> void:
	$FullPlayer.play("full");
