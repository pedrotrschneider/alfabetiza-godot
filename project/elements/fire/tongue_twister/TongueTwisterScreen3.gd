extends Node2D

export(NodePath) onready var tongue_twister_dock1 = self.get_node(tongue_twister_dock1) as TongueTwisterDock;
export(NodePath) onready var tongue_twister_dock2 = self.get_node(tongue_twister_dock2) as TongueTwisterDock;
export(NodePath) onready var tongue_twister_dock3 = self.get_node(tongue_twister_dock3) as TongueTwisterDock;
export(NodePath) onready var tongue_twister_dock4 = self.get_node(tongue_twister_dock4) as TongueTwisterDock;
export(NodePath) onready var tongue_twister_dock5 = self.get_node(tongue_twister_dock5) as TongueTwisterDock;
export(NodePath) onready var tongue_twister_dock6 = self.get_node(tongue_twister_dock6) as TongueTwisterDock;
export(NodePath) onready var tongue_twister_option_card1 = self.get_node(tongue_twister_option_card1) as TongueTwisterOptionCard;
export(NodePath) onready var tongue_twister_option_card2 = self.get_node(tongue_twister_option_card2) as TongueTwisterOptionCard;
export(NodePath) onready var victory_sfx_player = self.get_node(victory_sfx_player) as AudioStreamPlayer;
export(NodePath) onready var fail_sfx_player = self.get_node(fail_sfx_player) as AudioStreamPlayer;

signal next_screen(current_id);

var id : int = 3;

var num_answers : int = 2;
var wrong_picks : Array = [];


func start() -> void:
	var dialogue : Node = Dialogic.start("tongue_twister3");
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");
	
	tongue_twister_option_card1.connect("card_selected", self, "_on_card_selected");
	tongue_twister_option_card2.connect("card_selected", self, "_on_card_selected");


func _on_card_selected(opt_card : TongueTwisterOptionCard, dock_index : int) -> void:
	opt_card._select_answer(dock_index == opt_card.index);
	if(dock_index != opt_card.index):
		fail_sfx_player.play();
		wrong_picks.append(opt_card.word)
		return;
	victory_sfx_player.play();
	opt_card.selectable = false;
	num_answers -= 1;
	if(num_answers == 0):
		self.emit_signal("next_screen", id);
