class_name TongueTwisterGame
extends Control

export(NodePath) onready var next_screen_tween = self.get_node(next_screen_tween) as Tween;
export(NodePath) onready var screens_container = self.get_node(screens_container) as HBoxContainer;
export(NodePath) onready var back_button = self.get_node(back_button) as Button;
export(Array, NodePath) var screen_paths;

var screens: Array = [];


func _ready() -> void:
	back_button.connect("pressed", self, "_on_back_button_pressed");
	for path in screen_paths:
		var new_screen = self.get_node(path);
		screens.append(new_screen);
		new_screen.connect("next_screen", self, "_on_next_screen");
	screens[-1].connect("end_game", self, "_on_game_ended");
	
	screens[0].start();


func _on_next_screen(current_id: int) -> void:
	next_screen_tween.stop_all();
	next_screen_tween.remove_all();
	next_screen_tween.interpolate_property(screens_container, "rect_position", null, screens_container.rect_position - Vector2(1920, 0), 2.0, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT);
	next_screen_tween.start();
	
	yield(next_screen_tween, "tween_all_completed");
	
	if(current_id <= screens.size() - 1):
		screens[current_id].start();


func _on_game_ended() -> void:
	BarTransition.transition_to("res://elements/fire/FireMinigameSelector.tscn");


func _on_back_button_pressed() -> void:
	BarTransition.transition_to("res://elements/fire/FireMinigameSelector.tscn");
