class_name TongueTwisterScreen1
extends HBoxContainer

signal next_screen(current_id);

export(NodePath) onready var play_button = self.get_node(play_button) as Button;
export(NodePath) onready var continue_button = self.get_node(continue_button) as Button;
export(NodePath) onready var tongue_twister_player = self.get_node(tongue_twister_player) as AudioStreamPlayer;

var id: int = 1;


func start() -> void:
	play_button.connect("pressed", self, "_on_play_button_pressed");
	continue_button.connect("pressed", self, "_on_continue_button_pressed");
	
	var dialogue : Node = Dialogic.start("tongue_twister1");
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");


func _on_play_button_pressed() -> void:
	if(!tongue_twister_player.playing):
		tongue_twister_player.play();
		yield(tongue_twister_player, "finished");
		continue_button.disabled = false;


func _on_continue_button_pressed() -> void:
	continue_button.disabled = true;
	self.emit_signal("next_screen", id);
