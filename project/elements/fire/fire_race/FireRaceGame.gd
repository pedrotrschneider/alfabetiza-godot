class_name FireRace
extends Node2D

signal game_ended();

export(PackedScene) var obstacle_scene;
export(NodePath) onready var finish_pos = self.get_node(finish_pos) as Position2D;
export(NodePath) onready var finish_area = self.get_node(finish_area) as Area2D;
export(NodePath) onready var back_button = self.get_node(back_button) as Button;
export(NodePath) onready var countdown_label = self.get_node(countdown_label) as Label;
export(NodePath) onready var time_label = self.get_node(time_label) as Label;
export(NodePath) onready var clock_icon = self.get_node(clock_icon) as TextureRect;
export(NodePath) onready var points_label = self.get_node(points_label) as Label;
export(NodePath) onready var bucket_icon = self.get_node(bucket_icon) as TextureRect;
export(Array, NodePath) var life_icons : Array;
export(NodePath) onready var countdown_timer = self.get_node(countdown_timer) as Timer;
export(NodePath) onready var game_timer = self.get_node(game_timer) as Timer;
export(NodePath) onready var instruction_player = self.get_node(instruction_player) as AudioStreamPlayer;
export(NodePath) onready var victory_sfx = self.get_node(victory_sfx) as AudioStreamPlayer;
export(NodePath) onready var fail_sfx = self.get_node(fail_sfx) as AudioStreamPlayer;
export(NodePath) onready var player = self.get_node(player) as FireRacePlayer;

var points : int = 0;
var game_started : bool = false;
var game_over : bool = false;


func _ready() -> void:
	for i in life_icons.size():
		life_icons[i] = self.get_node(life_icons[i]);
	
	# warning-ignore:return_value_discarded
	self.connect("game_ended", player, "_end_game");
	
	player.connect("player_hit", self, "_on_player_hit");
	player.connect("player_lost", self, "_on_player_lost");
	back_button.connect("pressed", self, "_on_back_button_pressed");
	countdown_timer.connect("timeout", self, "_on_countdown_timer_timeout");
	game_timer.connect("timeout", self, "_on_game_timer_timeout");
	finish_area.connect("body_entered", self, "_on_finish_area_body_entered");
	
	player.finish_pos = finish_pos.position;
	
	time_label.hide();
	clock_icon.hide();
	points_label.hide();
	bucket_icon.hide();
	countdown_label.hide();
	
	var dialogue : Node = Dialogic.start("fire_race_intro");
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");
	yield(self.get_tree().create_timer(0.5), "timeout");
	countdown_timer.start();
	countdown_label.show();
	instruction_player.play();


func _process(_delta):
	if(!countdown_timer.is_stopped()):
		countdown_label.text = "%d\nCLIQUE COM O BOTÃO ESQUERDO\nE DESLIZE O MOUSE!" % countdown_timer.time_left;
		if(countdown_timer.time_left <= 1):
			countdown_label.text = "LEVE O FOGO À ALDEIA!";
	
	time_label.text = "%.1f" % game_timer.time_left;
	points_label.text = "%d" % points;


func _on_finish_area_body_entered(body: Node) -> void:
	if body is FireRacePlayer:
		time_label.hide();
		clock_icon.hide();
		points_label.hide();
		bucket_icon.hide();
		countdown_label.hide();
		body.can_move = false;
		var tween := self.get_tree().create_tween();
		# warning-ignore:return_value_discarded
		tween.tween_property(body, "position", body.position + Vector2(0.0, -1000.0), 2.0);
		game_timer.stop();
		victory_sfx.play();
		yield(self.get_tree().create_timer(1.0), "timeout");
		var dialogue : Node = Dialogic.start("fire_race_won");
		self.add_child(dialogue);
		yield(dialogue, "tree_exited");
		yield(self.get_tree().create_timer(0.5), "timeout");
		BarTransition.transition_to("res://elements/fire/FireMinigameSelector.tscn");


func _on_player_hit() -> void:
	if(life_icons.size() > 0):
		life_icons[life_icons.size() - 1].queue_free();
		life_icons.pop_back();
		fail_sfx.play();


func _on_player_lost() -> void:
	game_over = true;
	self.emit_signal("game_ended");
	var dialogue : Node = Dialogic.start("fire_race_lost");
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");
	yield(self.get_tree().create_timer(0.5), "timeout");
	BarTransition.transition_to("res://elements/fire/FireMinigameSelector.tscn");


func _on_countdown_timer_timeout() -> void:
	game_timer.start();
	countdown_label.hide();
	time_label.show();
	clock_icon.show();
	points_label.show();
	bucket_icon.show();
	
	player.can_move = true;


func _on_game_timer_timeout() -> void:
	game_over = true;
	self.emit_signal("game_ended");
	var tween := self.get_tree().create_tween();
	# warning-ignore:return_value_discarded
	tween.tween_property(player.get_node("Light2D"), "energy", 0.0, 1.0);
	var dialogue : Node = Dialogic.start("fire_race_end");
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");
	yield(self.get_tree().create_timer(0.5), "timeout");
	BarTransition.transition_to("res://elements/fire/FireMinigameSelector.tscn");


func _on_back_button_pressed() -> void:
	BarTransition.transition_to("res://elements/fire/FireMinigameSelector.tscn");
