class_name FireRacePlayer
extends KinematicBody2D

signal player_hit();
signal player_lost();

export(NodePath) onready var hitbox_area = self.get_node(hitbox_area) as Area2D;
export(NodePath) onready var sprite = self.get_node(sprite) as Sprite;
export(NodePath) onready var arrow = self.get_node(arrow) as Node2D;

var lives : int = 3;
var finish_pos : Vector2 = Vector2(0, -10000);
var target_pos : Vector2 = Vector2.ZERO;
var acceleration : float = 100.0;
var speed : float = 200.0;
var velocity : Vector2 = Vector2.ZERO;
var game_ended : bool = false;
var can_move: bool = false;


func _ready() -> void:
	hitbox_area.connect("area_entered", self, "_on_hitbox_area_entered")
	
	target_pos = self.global_position;


func _physics_process(delta: float) -> void:
	arrow.look_at(finish_pos);
	
	if(!game_ended && can_move):
		if(Input.is_action_pressed("mouse_select")):
			target_pos = get_global_mouse_position();
		
		velocity = lerp(velocity, (target_pos - self.position) * speed, acceleration * delta);
		velocity = self.move_and_slide(velocity * delta, Vector2.UP);


func _on_hitbox_area_entered(area : Area2D) -> void:
	if(!game_ended and area is FireRaceObstacle):
		area.queue_free();
		sprite.modulate = Color.red;
		self.emit_signal("player_hit");
		lives -= 1;
		if(lives == 0):
			self.emit_signal("player_lost");
			var tween := self.get_tree().create_tween();
			# warning-ignore:return_value_discarded
			tween.tween_property($Light2D, "energy", 0.0, 1.0);
		yield(self.get_tree().create_timer(1.0), "timeout");
		sprite.modulate = Color.white;


func _end_game() -> void:
	game_ended = true;
