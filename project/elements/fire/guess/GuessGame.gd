class_name GuessGame
extends Node2D

export(NodePath) onready var option_card1 = self.get_node(option_card1) as GuessOptionCard;
export(NodePath) onready var option_card2 = self.get_node(option_card2) as GuessOptionCard;
export(NodePath) onready var option_card3 = self.get_node(option_card3) as GuessOptionCard;
export(NodePath) onready var back_button = self.get_node(back_button) as Button;
export(NodePath) onready var victory_sfx_player = self.get_node(victory_sfx_player) as AudioStreamPlayer;
export(NodePath) onready var fail_sfx_player = self.get_node(fail_sfx_player) as AudioStreamPlayer;
export(NodePath) onready var game_saver = self.get_node(game_saver) as GameSaver;

var points : int = 3;
var wrong_picks : Array = [];
var time_taken : float = 0;


func _ready() -> void:
	back_button.connect("pressed", self, "_on_back_button_pressed");
	
	option_card1.connect("card_selected", self, "_on_card_selected");
	option_card2.connect("card_selected", self, "_on_card_selected");
	option_card3.connect("card_selected", self, "_on_card_selected");
	
	var dialogue : Node = Dialogic.start("guess_intro");
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");


func _process(delta: float) -> void:
	time_taken += delta;


func _on_card_selected(is_answer : bool, word : String) -> void:
	if(!is_answer):
		fail_sfx_player.play();
		wrong_picks.append(word)
		points -= 1;
		yield(self.get_tree().create_timer(0.1), "timeout");
		var dialogue : Node = Dialogic.start("guess_tip");
		self.add_child(dialogue);
		yield(dialogue, "tree_exited");
		return;
	victory_sfx_player.play();
	for c in get_children():
		if(c is GuessOptionCard):
			c.selectable = false;
	var data : Dictionary = {
		"wrong_picks" : wrong_picks
	}
	game_saver.save(PlayerData.player_id, ElementID.FIRE, MinigameID.GUESS,\
		0, time_taken, wrong_picks.size(), data);
	yield(game_saver, "game_saved");
	yield(get_tree().create_timer(0.5), "timeout");
	
	BarTransition.transition_to(get_level_selector_scene_path());
	yield(BarTransition, "screen_dimmed");
	self.queue_free();


func _on_back_button_pressed() -> void:
	BarTransition.transition_to(get_level_selector_scene_path());
	yield(BarTransition, "screen_dimmed");
	self.queue_free();


func get_level_selector_scene_path() -> String:
	return "res://elements/fire/FireMinigameSelector.tscn";
