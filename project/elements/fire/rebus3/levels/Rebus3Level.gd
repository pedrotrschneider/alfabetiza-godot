class_name Rebus3Level
extends Resource

export var level_num : int = 1;
export(Array, Texture) var question_textures : Array;
export var question_words : PoolStringArray = ["", ""];
export(Array, PoolStringArray) var question_words_parts;
export(Array, Texture) var option_textures : Array;
export var option_words : PoolStringArray = ["", "", ""];
