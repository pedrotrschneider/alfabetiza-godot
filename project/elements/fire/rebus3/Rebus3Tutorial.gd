class_name Rebus3Tutorial
extends Node2D

export(NodePath) onready var animation_player = self.get_node(animation_player) as AnimationPlayer;
export(NodePath) onready var back_button = self.get_node(back_button) as Button;
export(NodePath) onready var question_card1 = self.get_node(question_card1) as Rebus3QuestionCard;
export(NodePath) onready var question_card2 = self.get_node(question_card2) as Rebus3QuestionCard;
export(NodePath) onready var option_card1 = self.get_node(option_card1) as Rebus3OptionCard;
export(NodePath) onready var option_card2 = self.get_node(option_card2) as Rebus3OptionCard;
export(NodePath) onready var option_card3 = self.get_node(option_card3) as Rebus3OptionCard;
export(NodePath) onready var question1_tween = self.get_node(question1_tween) as Tween;
export(NodePath) onready var question2_tween = self.get_node(question2_tween) as Tween;

var level_data : Rebus3Level = load("res://elements/fire/rebus3/levels/tutorial.tres") as Rebus3Level;

var draw_box : bool = false;
var question_cards_box : StyleBoxFlat;
var box_size : Vector2 = Vector2(0, 450);
var box_pos : Vector2 = Vector2(1920 / 2, 370 - box_size.y / 2);


func _ready() -> void:
	back_button.connect("pressed", self, "_on_back_button_pressed");
	
	question_cards_box = StyleBoxFlat.new();
	question_cards_box.set_corner_radius_all(40);
	question_cards_box.set_border_width_all(10);
	question_cards_box.border_color = Color.black;
	question_cards_box.bg_color = Color(0, 0, 0, 0);
	box_size.x = 1920.0 / (2 + 1) * (2 - 1) + 350;
	box_pos.x = (1920.0 - box_size.x) / 2;
	
	question_card1.texture = level_data.question_textures[0];
	question_card1.word = level_data.question_words[0];
	question_card1.w1 = level_data.question_words_parts[0][0];
	question_card1.w2 = level_data.question_words_parts[0][1];
	question_card2.texture = level_data.question_textures[1];
	question_card2.word = level_data.question_words[1];
	question_card2.w1 = level_data.question_words_parts[1][0];
	question_card2.w2 = level_data.question_words_parts[1][1];
	
	option_card1.texture = level_data.option_textures[0];
	option_card1.word = level_data.option_words[0];
	option_card1.selectable = false;
	option_card1.is_answer = true;
	option_card2.texture = level_data.option_textures[1];
	option_card2.word = level_data.option_words[1];
	option_card2.selectable = false;
	option_card2.is_answer = false;
	option_card3.texture = level_data.option_textures[2];
	option_card3.word = level_data.option_words[2];
	option_card3.selectable = false;
	option_card3.is_answer = false;
	
	var dialogue : Node = Dialogic.start("rebus3_tutorial1");
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");
	animation_player.play("p1");
	yield(animation_player, "animation_finished");
	draw_box = true;
	update();
	dialogue = Dialogic.start("rebus3_tutorial2");
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");
	animation_player.play("p2");
	yield(animation_player, "animation_finished");
	question_card1.glow();
	question_card2.glow();
	dialogue = Dialogic.start("rebus3_tutorial3");
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");
	yield(get_tree().create_timer(0.5), "timeout");
	BarTransition.transition_to("res://elements/fire/rebus3/Rebus3LevelSelector.tscn");


func _draw() -> void:
	if(draw_box):
		self.draw_style_box(question_cards_box, Rect2(box_pos, box_size));


func _on_back_button_pressed() -> void:
	BarTransition.transition_to("res://elements/fire/rebus3/Rebus3LevelSelector.tscn");


func popon_option_cards() -> void:
	question1_tween.interpolate_property(question_card1, "scale", null, Vector2(1.0, 1.0), 1.0, Tween.TRANS_ELASTIC, Tween.EASE_OUT);
	question2_tween.interpolate_property(question_card2, "scale", null, Vector2(1.0, 1.0), 1.0, Tween.TRANS_ELASTIC, Tween.EASE_OUT);
	question1_tween.start();
	question2_tween.start();
