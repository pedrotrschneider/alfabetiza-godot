class_name FireMinigameSelector
extends Control

export(NodePath) onready var race_button = self.get_node(race_button) as Button;
export(NodePath) onready var word_hunt_button = self.get_node(word_hunt_button) as Button;
export(NodePath) onready var valise2_button = self.get_node(valise2_button) as Button;
export(NodePath) onready var valise3_button = self.get_node(valise3_button) as Button;
export(NodePath) onready var rebus3_button = self.get_node(rebus3_button) as Button;
export(NodePath) onready var tongue_twister_button = self.get_node(tongue_twister_button) as Button;
export(NodePath) onready var guess_button = self.get_node(guess_button) as Button;
export(NodePath) onready var back_button = self.get_node(back_button) as Button;


func _ready() -> void:
	MusicController.play_ambiance(MusicController.Ambiance.FIRE);
	
	race_button.connect("pressed", self, "_on_race_button_pressed");
	word_hunt_button.connect("pressed", self, "_on_word_hunt_button_pressed");
	valise2_button.connect("pressed", self, "_on_valise2_button_pressed");
	valise3_button.connect("pressed", self, "_on_valise3_button_pressed");
	rebus3_button.connect("pressed", self, "_on_rebus3_button_pressed");
	tongue_twister_button.connect("pressed", self, "_on_tongue_twister_button_pressed");
	guess_button.connect("pressed", self, "_on_guess_buttton_pressed");
	
	back_button.connect("pressed", self, "_on_back_button_pressed")


func _on_race_button_pressed() -> void:
	BarTransition.transition_to("res://elements/fire/fire_race/FireRaceGame.tscn");


func _on_word_hunt_button_pressed() -> void:
	BarTransition.transition_to("res://elements/fire/word_hunt/WordHuntLevelSelector.tscn");


func _on_valise2_button_pressed() -> void:
	BarTransition.transition_to("res://elements/fire/valise2/Valise2LevelSelector.tscn");


func _on_valise3_button_pressed() -> void:
	BarTransition.transition_to("res://elements/fire/valise3/Valise3LevelSelector.tscn");


func _on_rebus3_button_pressed() -> void:
	BarTransition.transition_to("res://elements/fire/rebus3/Rebus3LevelSelector.tscn");


func _on_tongue_twister_button_pressed() -> void:
	BarTransition.transition_to("res://elements/fire/tongue_twister/TongueTwisterGame.tscn");


func _on_guess_buttton_pressed() -> void:
	BarTransition.transition_to("res://elements/fire/guess/GuessGame.tscn");


func _on_back_button_pressed() -> void:
	BarTransition.transition_to("res://elements/ElementSelector.tscn");
