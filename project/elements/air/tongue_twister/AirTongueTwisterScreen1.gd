class_name AirTongueTwisterScreen1
extends HBoxContainer

signal next_screen(current_id);

export(NodePath) onready var image_card1 = self.get_node(image_card1) as AirTongueTwisterImageOptionCard;
export(NodePath) onready var image_card2 = self.get_node(image_card2) as AirTongueTwisterImageOptionCard;
export(NodePath) onready var image_card3 = self.get_node(image_card3) as AirTongueTwisterImageOptionCard;
export(NodePath) onready var image_card4 = self.get_node(image_card4) as AirTongueTwisterImageOptionCard;
export(NodePath) onready var image_card5 = self.get_node(image_card5) as AirTongueTwisterImageOptionCard;
export(NodePath) onready var play_button = self.get_node(play_button) as Button;
export(NodePath) onready var continue_button = self.get_node(continue_button) as Button;
export(NodePath) onready var tongue_twister_player = self.get_node(tongue_twister_player) as AudioStreamPlayer;
export(NodePath) onready var victory_sfx_player = self.get_node(victory_sfx_player) as AudioStreamPlayer;
export(NodePath) onready var fail_sfx_player = self.get_node(fail_sfx_player) as AudioStreamPlayer;

var id: int = 1;
var started : bool = false;


func start() -> void:
	play_button.connect("pressed", self, "_on_play_button_pressed");
	continue_button.connect("pressed", self, "_on_continue_button_pressed");
	
	image_card1.connect("card_selected", self, "_on_card_selected");
	image_card2.connect("card_selected", self, "_on_card_selected");
	image_card3.connect("card_selected", self, "_on_card_selected");
	image_card4.connect("card_selected", self, "_on_card_selected");
	image_card5.connect("card_selected", self, "_on_card_selected");


func _on_play_button_pressed() -> void:
	if(!tongue_twister_player.playing):
		tongue_twister_player.play();
		yield(tongue_twister_player, "finished");
		continue_button.disabled = false;
		if not started:
			yield(self.get_tree().create_timer(0.5), "timeout");
			image_card1.interpolate_scale_to(1.0);
			image_card2.interpolate_scale_to(1.0);
			image_card3.interpolate_scale_to(1.0);
			image_card4.interpolate_scale_to(1.0);
			image_card5.interpolate_scale_to(1.0);
			var dialogue : Node = Dialogic.start("air_tongue_twister1");
			self.add_child(dialogue);
			yield(dialogue, "tree_exited");
			
		


func _on_card_selected(is_answer : bool, _word : String) -> void:
	if(!is_answer):
		fail_sfx_player.play();
		return;
	victory_sfx_player.play();
	self.emit_signal("next_screen", id);


func _on_continue_button_pressed() -> void:
	continue_button.disabled = true;
	self.emit_signal("next_screen", id);
