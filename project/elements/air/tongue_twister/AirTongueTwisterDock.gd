class_name AirTongueTwisterDock
extends Node2D

export(NodePath) onready var dock = self.get_node(dock) as Dock;

export var dock_index : int = -1;

var size : Vector2 = Vector2(200, 200);
var pos : Vector2 = Vector2(-size / 2);
var corner_radius : int = 30;


func _ready() -> void:
	dock.dock_index = dock_index;


func _draw() -> void:
	var style_box : StyleBoxFlat = StyleBoxFlat.new()
	style_box.bg_color = Color(0.4, 0.4, 0.4);
	style_box.set_corner_radius_all(corner_radius);
	style_box.set_border_width_all(10);
	style_box.set_border_color(Color.black);
	draw_style_box(style_box, Rect2(pos, size));
