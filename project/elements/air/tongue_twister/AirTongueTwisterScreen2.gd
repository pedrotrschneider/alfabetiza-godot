extends Node2D

export(NodePath) onready var tongue_twister_dock1 = self.get_node(tongue_twister_dock1) as AirTongueTwisterDock;
export(NodePath) onready var tongue_twister_dock2 = self.get_node(tongue_twister_dock2) as AirTongueTwisterDock;
export(NodePath) onready var tongue_twister_dock3 = self.get_node(tongue_twister_dock3) as AirTongueTwisterDock;
export(NodePath) onready var tongue_twister_dock4 = self.get_node(tongue_twister_dock4) as AirTongueTwisterDock;
export(NodePath) onready var tongue_twister_dock5 = self.get_node(tongue_twister_dock5) as AirTongueTwisterDock;
export(NodePath) onready var tongue_twister_dock6 = self.get_node(tongue_twister_dock6) as AirTongueTwisterDock;
export(NodePath) onready var tongue_twister_option_card1 = self.get_node(tongue_twister_option_card1) as AirTongueTwisterOptionCard;
export(NodePath) onready var tongue_twister_option_card2 = self.get_node(tongue_twister_option_card2) as AirTongueTwisterOptionCard;
export(NodePath) onready var tongue_twister_option_card3 = self.get_node(tongue_twister_option_card3) as AirTongueTwisterOptionCard;
export(NodePath) onready var tongue_twister_option_card4 = self.get_node(tongue_twister_option_card4) as AirTongueTwisterOptionCard;
export(NodePath) onready var tongue_twister_option_card5 = self.get_node(tongue_twister_option_card5) as AirTongueTwisterOptionCard;
export(NodePath) onready var tongue_twister_option_card6 = self.get_node(tongue_twister_option_card6) as AirTongueTwisterOptionCard;
export(NodePath) onready var victory_sfx_player = self.get_node(victory_sfx_player) as AudioStreamPlayer;
export(NodePath) onready var fail_sfx_player = self.get_node(fail_sfx_player) as AudioStreamPlayer;

signal next_screen();
signal end_game();

var id : int = 2;

var num_answers : int = 6;
var answers : Array = [0, 0, 0, 0, 0, 0, 0];


func start() -> void:
	var dialogue : Node = Dialogic.start("air_tongue_twister2");
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");

	tongue_twister_option_card1.connect("card_selected", self, "_on_card_selected");
	tongue_twister_option_card2.connect("card_selected", self, "_on_card_selected");
	tongue_twister_option_card3.connect("card_selected", self, "_on_card_selected");
	tongue_twister_option_card4.connect("card_selected", self, "_on_card_selected");
	tongue_twister_option_card5.connect("card_selected", self, "_on_card_selected");
	tongue_twister_option_card6.connect("card_selected", self, "_on_card_selected");


func _on_card_selected(opt_card : AirTongueTwisterOptionCard, dock_index : int) -> void:
	opt_card._select_answer(true);
	num_answers -= 1;
	answers[dock_index] = opt_card.index;
	
	if(num_answers == 0):
		if(!check_answer()):
			answers = [0, 0, 0, 0, 0, 0];
			num_answers = 6;
			fail_sfx_player.play();
			
			tongue_twister_option_card1.interpolate_to_origin();
			tongue_twister_option_card2.interpolate_to_origin();
			tongue_twister_option_card3.interpolate_to_origin();
			tongue_twister_option_card4.interpolate_to_origin();
			tongue_twister_option_card5.interpolate_to_origin();
			tongue_twister_option_card6.interpolate_to_origin();
		else:
			victory_sfx_player.play();
			self.emit_signal("next_screen", id);


func check_answer() -> bool:
	if answers[0] == 0 || answers[0] == 3:
		if answers[1] == 2:
			if answers[2] == 5:
				if answers[3] == 4:
					if answers[4] == 0 || answers[4] == 3:
						if answers[5] == 1:
							return true;
	return false;
