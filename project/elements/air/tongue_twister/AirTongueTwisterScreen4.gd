extends Node2D

export(NodePath) onready var tongue_twister_dock1 = self.get_node(tongue_twister_dock1) as AirTongueTwisterDock;
export(NodePath) onready var tongue_twister_dock2 = self.get_node(tongue_twister_dock2) as AirTongueTwisterDock;
export(NodePath) onready var tongue_twister_dock3 = self.get_node(tongue_twister_dock3) as AirTongueTwisterDock;
export(NodePath) onready var tongue_twister_dock4 = self.get_node(tongue_twister_dock4) as AirTongueTwisterDock;
export(NodePath) onready var tongue_twister_dock5 = self.get_node(tongue_twister_dock5) as AirTongueTwisterDock;
export(NodePath) onready var tongue_twister_option_card1 = self.get_node(tongue_twister_option_card1) as AirTongueTwisterOptionCard;
export(NodePath) onready var tongue_twister_option_card2 = self.get_node(tongue_twister_option_card2) as AirTongueTwisterOptionCard;
export(NodePath) onready var tongue_twister_option_card3 = self.get_node(tongue_twister_option_card3) as AirTongueTwisterOptionCard;
export(NodePath) onready var tongue_twister_option_card4 = self.get_node(tongue_twister_option_card4) as AirTongueTwisterOptionCard;
export(NodePath) onready var tongue_twister_option_card5 = self.get_node(tongue_twister_option_card5) as AirTongueTwisterOptionCard;
export(NodePath) onready var victory_sfx_player = self.get_node(victory_sfx_player) as AudioStreamPlayer;
export(NodePath) onready var fail_sfx_player = self.get_node(fail_sfx_player) as AudioStreamPlayer;

signal next_screen();
signal end_game();

var id : int = 1;

var num_answers : int = 5;
var answers : Array = [0, 0, 0, 0, 0, 0];


func start() -> void:
	var dialogue : Node = Dialogic.start("tongue_twister4");
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");

	tongue_twister_option_card1.connect("card_selected", self, "_on_card_selected");
	tongue_twister_option_card2.connect("card_selected", self, "_on_card_selected");
	tongue_twister_option_card3.connect("card_selected", self, "_on_card_selected");
	tongue_twister_option_card4.connect("card_selected", self, "_on_card_selected");
	tongue_twister_option_card5.connect("card_selected", self, "_on_card_selected");


func _on_card_selected(opt_card : AirTongueTwisterOptionCard, dock_index : int) -> void:
	opt_card._select_answer(true);
	num_answers -= 1;
	answers[dock_index] = opt_card.index;
	
	if(num_answers == 0):
		if(!check_answer()):
			answers = [0, 0, 0, 0, 0];
			num_answers = 5;
			fail_sfx_player.play();
			
			tongue_twister_option_card1.interpolate_to_origin();
			tongue_twister_option_card2.interpolate_to_origin();
			tongue_twister_option_card3.interpolate_to_origin();
			tongue_twister_option_card4.interpolate_to_origin();
			tongue_twister_option_card5.interpolate_to_origin();
		else:
			self.emit_signal("end_game");
			victory_sfx_player.play();


func check_answer() -> bool:
	if(answers[0] == 0 || answers[0] == 3):
		if(answers[1] == 1 || answers[1] == 4):
			if(answers[2] == 2):
				if(answers[4] == 4 || answers[4] == 1):
					return true;
	if(answers[0] == 2):
		if(answers[1] == 0 || answers[1] == 3):
			if(answers[2] == 1 || answers[2] == 4):
				if(answers[3] == 0 || answers[3] == 3):
					if(answers[4] == 1 || answers[4] == 4):
						return true;
	return false;
