class_name PuzzleLevel
extends Resource

export(int) var level_num = -1;
export(String, MULTILINE) var puzzle = "";
export(String) var answer = "";
export(Texture) var answer_texture;
