class_name PuzzleGame
extends Node2D

export(PackedScene) var puzzle_option_card_scene;
export(PackedScene) var puzzle_letter_dock_scene;
export(PackedScene) var puzzle_answer_card_scene;

export(NodePath) onready var puzzle_margin_container = self.get_node(puzzle_margin_container) as MarginContainer;
export(NodePath) onready var puzzle_label = self.get_node(puzzle_label) as RichTextLabel;
export(NodePath) onready var level_label = self.get_node(level_label) as Label;
export(NodePath) onready var back_button = self.get_node(back_button) as Button;
export(NodePath) onready var game_saver = self.get_node(game_saver) as GameSaver;
export(NodePath) onready var victory_sfx_player = self.get_node(victory_sfx_player) as AudioStreamPlayer;
export(NodePath) onready var fail_sfx_player = self.get_node(fail_sfx_player) as AudioStreamPlayer;

var answer_card: PuzzleAnswerCard;

var level_data : PuzzleLevel = load("res://elements/air/puzzle/levels/level5.tres");
var num_answers : int = 0;
var wrong_picks : Array = [];
var time_taken : float = 0;


func _ready() -> void:
	back_button.connect("pressed", self, "_on_back_button_connected");
	
	init_game();


func _process(delta: float) -> void:
	time_taken += delta;


func _on_back_button_connected() -> void:
	BarTransition.begin_transition();
	yield(BarTransition, "screen_dimmed");
	# warning-ignore:return_value_discarded
	self.get_tree().change_scene("res://elements/air/puzzle/PuzzleLevelSelector.tscn");
	self.queue_free();
	BarTransition.end_transition();


func _on_card_selected(card : PuzzleOptionCard, dock : PuzzleLetterDock) -> void:
	card._select_answer(card.word == dock.word);
	if(card.word != dock.word):
		fail_sfx_player.play();
		wrong_picks.append(card.word);
		return;
	
	num_answers -= 1;
	victory_sfx_player.play();
	if num_answers > 0:
		return
	
	for c in get_children():
		if(c is PuzzleOptionCard):
			c.selectable = false;
	answer_card.show();
	
	var data : Dictionary = {
		"wrong_picks" : wrong_picks
	}
	game_saver.save(PlayerData.player_id, ElementID.AIR, MinigameID.PUZZLE,\
		0, time_taken, wrong_picks.size(), data);
	yield(game_saver, "game_saved");
	yield(get_tree().create_timer(0.5), "timeout");
	
	var next_level_idx: int = level_data.level_num + 1;
	var file: File = File.new();
	var next_level_path: String = "res://elements/air/puzzle/levels/level%d.tres" % next_level_idx;
	if file.file_exists(next_level_path):
		level_data = load(next_level_path);
		BarTransition.begin_transition();
		yield(BarTransition, "screen_dimmed");
		reset();
		BarTransition.end_transition();
	else:
		BarTransition.transition_to("res://elements/air/puzzle/PuzzleLevelSelector.tscn");
		yield(BarTransition, "screen_dimmed");
		self.queue_free();


func reset() -> void:
	num_answers = 0;
	wrong_picks = [];
	time_taken = 0;
	
	for c in self.get_children():
		if c is PuzzleLetterDock \
		or c is PuzzleOptionCard \
		or c is PuzzleAnswerCard:
			c.queue_free();
	
	init_game();


func init_game() -> void: 
	level_label.text = "NÍVEL %d" % [level_data.level_num];
	puzzle_label.bbcode_text = level_data.puzzle;
	puzzle_label.bbcode_text = puzzle_label.bbcode_text.to_upper();
	num_answers = level_data.answer.length();
	
	# Add the card docks
	for i in num_answers:
		var new_dock : PuzzleLetterDock = puzzle_letter_dock_scene.instance();
		self.add_child(new_dock);
		new_dock.position.y = 916;
		new_dock.position.x = 180 + 225 * i;
		new_dock.word = level_data.answer[i];
	
	# Add the cards
	var idx : Array = range(num_answers);
	idx.shuffle();
	var total_width : float = 900;
	var ii : int = 0;
	var cards_in_rows : Array = [
		min(num_answers, 4),
		max(num_answers - 4, 0)
	];
	for i in idx:
		var new_o_card : PuzzleOptionCard = puzzle_option_card_scene.instance()
		self.add_child(new_o_card);
		new_o_card.word = level_data.answer[i];
		new_o_card.index = i;
		# warning-ignore:return_value_discarded
		new_o_card.connect("card_selected", self, "_on_card_selected");
		# warning-ignore:integer_division
		var v_index : int = ii / 4;
		var h_index : int = ii % 4;
		if cards_in_rows[1] == 0:
			new_o_card.position.y = 320 + 150;
		else:
			new_o_card.position.y = 320 + 300 * v_index;
		
		if cards_in_rows[v_index] == 4:
			new_o_card.position.x = 780 + total_width / float(cards_in_rows[v_index] - 1) * h_index;
		else:
			new_o_card.position.x = 780 + total_width / float(cards_in_rows[v_index] + 1) * (h_index + 1)
		ii += 1;
	
	# Add answer card
	var new_answer_card: PuzzleAnswerCard = puzzle_answer_card_scene.instance();
	self.add_child(new_answer_card);
	new_answer_card.position = Vector2(1920 * 0.65, 1080 * 0.45);
	new_answer_card.texture = level_data.answer_texture;
	new_answer_card.hide();
	answer_card = new_answer_card;
