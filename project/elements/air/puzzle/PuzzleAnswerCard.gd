class_name PuzzleAnswerCard
extends Node2D

export(NodePath) onready var sprite = self.get_node(sprite) as Sprite;

var size : Vector2 = Vector2(500, 500);
var pos : Vector2 = -size / 2;
var corner_radius : int = 30;

export var texture : Texture setget set_texture;

func _draw() -> void:
	var style_box : StyleBoxFlat = StyleBoxFlat.new()
	style_box.bg_color = Color(0.4, 0.4, 0.4);
	style_box.set_corner_radius_all(corner_radius);
	style_box.set_border_width_all(10);
	style_box.set_border_color(Color.black);
	draw_style_box(style_box, Rect2(pos, size));


func set_texture(val : Texture) -> void:
	texture = val;
	$Sprite.texture = val;
	$Sprite.scale = Vector2(450 / val.get_size().x, 450 / val.get_size().y);
