extends Node2D

export(NodePath) onready var animation_player = self.get_node(animation_player) as AnimationPlayer;
export(NodePath) onready var back_button = self.get_node(back_button) as Button;
export(NodePath) onready var background_rect = self.get_node(background_rect) as ColorRect;
export(NodePath) onready var puzzle_margin_container = self.get_node(puzzle_margin_container) as MarginContainer;


func _ready() -> void:
	back_button.connect("pressed", self, "_on_back_button_pressed");
	
	var dialogue : Node = Dialogic.start("puzzle_tutorial1");
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");
	yield(self.get_tree().create_timer(0.5), "timeout");
	animation_player.play("p1");
	yield(animation_player, "animation_finished");
	dialogue = Dialogic.start("puzzle_tutorial2");
	self.add_child(dialogue);
	yield(dialogue, "tree_exiting");
	yield(self.get_tree().create_timer(0.5), "timeout");
	BarTransition.transition_to("res://elements/air/puzzle/PuzzleLevelSelector.tscn");


func _process(_delta: float) -> void:
	background_rect.rect_min_size = puzzle_margin_container.rect_size;


func _on_back_button_pressed() -> void:
	BarTransition.transition_to("res://elements/air/puzzle/PuzzleLevelSelector.tscn");
