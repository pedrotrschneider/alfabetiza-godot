class_name Rebus4Game
extends Node2D

export(PackedScene) var option_card_scene;
export(PackedScene) var question_card_scene;
export(NodePath) onready var back_button = self.get_node(back_button) as Button;
export(NodePath) onready var victory_sfx_player = self.get_node(victory_sfx_player) as AudioStreamPlayer;
export(NodePath) onready var level_label = self.get_node(level_label) as Label;
export(NodePath) onready var fail_sfx_player = self.get_node(fail_sfx_player) as AudioStreamPlayer;
export(NodePath) onready var game_saver = self.get_node(game_saver) as GameSaver;

var level_data : Rebus4Level;
var points : int = 3;
var wrong_picks : Array = [];
var time_taken : float = 0;

var question_cards_box : StyleBoxFlat;
var box_size : Vector2 = Vector2(0, 450);
var box_pos : Vector2 = Vector2(1920 / 2, 370 - box_size.y / 2);


func _ready() -> void:
	back_button.connect("pressed", self, "_on_back_button_pressed");
	
	init_game();


func _process(delta: float) -> void:
	time_taken += delta;

func _draw() -> void:
	self.draw_style_box(question_cards_box, Rect2(box_pos, box_size));


func _on_card_selected(is_answer : bool, word : String) -> void:
	if(!is_answer):
		fail_sfx_player.play();
		wrong_picks.append(word);
		points -= 1;
		return;
	victory_sfx_player.play();
	for c in get_children():
		if(c is Rebus4OptionCard):
			c.selectable = false;
	var data : Dictionary = {
		"wrong_picks" : wrong_picks
	}
	yield(get_tree().create_timer(0.5), "timeout");
	game_saver.save(PlayerData.player_id, ElementID.EARTH, MinigameID.REBUS1,\
		level_data.level_num, time_taken, wrong_picks.size(), data);
	yield(game_saver, "game_saved");
	
	var next_level_idx: int = level_data.level_num + 1;
	var file: File = File.new();
	var next_level_path: String = "res://elements/air/rebus4/levels/level%d.tres" % next_level_idx;
	if file.file_exists(next_level_path):
		level_data = load(next_level_path);
		BarTransition.begin_transition();
		yield(BarTransition, "screen_dimmed");
		reset();
		BarTransition.end_transition();
	else:
		BarTransition.begin_transition();
		yield(BarTransition, "screen_dimmed");
		# warning-ignore:return_value_discarded
		get_tree().change_scene("res://elements/air/rebus4/Rebus4LevelSelector.tscn");
		self.queue_free();
		BarTransition.end_transition();


func reset() -> void:
	points = 3;
	wrong_picks = [];
	time_taken = 0;
	
	for c in self.get_children():
		if c is Rebus4OptionCard or c is Rebus4QuestionCard:
			c.queue_free();
	
	init_game();


func init_game() -> void:
	var num_qst_cards : int = level_data.question_words.size();
	for i in num_qst_cards:
		var new_card : Rebus4QuestionCard = question_card_scene.instance();
		self.add_child(new_card);
		new_card.word = level_data.question_words[i];
		new_card.texture = level_data.question_textures[i];
		# warning-ignore:integer_division
		new_card.position = Vector2(1920 / (num_qst_cards + 1) * (i + 1), 370);
	
	question_cards_box = StyleBoxFlat.new();
	question_cards_box.set_corner_radius_all(40);
	question_cards_box.set_border_width_all(10);
	question_cards_box.border_color = Color.black;
	question_cards_box.bg_color = Color(0, 0, 0, 0);
	box_size.x = 1920.0 / (num_qst_cards + 1) * (num_qst_cards - 1) + 350;
	box_pos.x = (1920.0 - box_size.x) / 2;
	
	var num_opt_cards : int = level_data.option_words.size();
	var opts : Array = range(num_opt_cards);
	randomize();
	opts.shuffle();
	for i in num_opt_cards:
		var new_card : Rebus4OptionCard = option_card_scene.instance();
		self.add_child(new_card);
		new_card.word = level_data.option_words[opts[i]];
		new_card.texture = level_data.option_textures[opts[i]];
		# warning-ignore:integer_division
		new_card.position = Vector2(1920 / (num_opt_cards + 1) * (i + 1), 820);
		new_card.is_answer = opts[i] == 0;
		# warning-ignore:return_value_discarded
		new_card.connect("card_selected", self, "_on_card_selected");
	
	level_label.text = "Nível " + str(level_data.level_num);
	
	update();


func _on_back_button_pressed() -> void:
	BarTransition.begin_transition();
	yield(BarTransition, "screen_dimmed");
	# warning-ignore:return_value_discarded
	get_tree().change_scene("res://elements/air/rebus4/Rebus4LevelSelector.tscn");
	self.queue_free();
	BarTransition.end_transition();
