class_name Rebus4QuestionCard
extends Node2D

export(NodePath) onready var sprite = get_node(sprite) as Sprite;
export(NodePath) onready var label = get_node(label) as Label;

var size : Vector2 = Vector2(300, 400);
var pos : Vector2 = -size / 2;
var corner_radius : int = 30;

var texture : Texture setget set_texture;
var word : String setget set_word;


func _draw() -> void:
	var style_box : StyleBoxFlat = StyleBoxFlat.new()
	style_box.bg_color = Color(0.4, 0.4, 0.4);
	style_box.set_corner_radius_all(corner_radius);
	self.draw_style_box(style_box, Rect2(pos, size));


func set_texture(val : Texture) -> void:
	texture = val;
	sprite.texture = val;
	sprite.scale = Vector2(250 / val.get_size().x, 250 / val.get_size().y);


func set_word(val : String):
	word = val;
	label.text = val.to_upper();
