class_name RetellImageDock
extends Node2D

export(NodePath) onready var dock = self.get_node(dock) as Dock;

export var image_index : int = -1;

func _ready() -> void:
	$Dock.dock_index = image_index;

