class_name RetellTutorial
extends Node2D

export(NodePath) onready var back_button = self.get_node(back_button) as Button;
export(NodePath) onready var game_saver = self.get_node(game_saver) as GameSaver;
export(NodePath) onready var animation_player = self.get_node(animation_player) as AnimationPlayer;

var level_data : RetellLevel = load("res://elements/air/retell/levels/tutorial.tres") as RetellLevel;
var num_answers : int = 3;
var points : int = 3;
var wrong_picks : Array = [];
var time_taken : float = 0;


func _ready() -> void:
	back_button.connect("pressed", self, "_on_back_button_pressed");
	
	var dialogue : Node = Dialogic.start("retell_tutorial1");
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");
	yield(self.get_tree().create_timer(0.5), "timeout");
	animation_player.play("p1");
	yield(animation_player, "animation_finished");
	yield(self.get_tree().create_timer(0.5), "timeout");
	dialogue = Dialogic.start("retell_tutorial2");
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");
	yield(self.get_tree().create_timer(0.5), "timeout");
	BarTransition.transition_to("res://elements/air/retell/RetellLevelSelector.tscn");


func _on_back_button_pressed() -> void:
	BarTransition.begin_transition();
	yield(BarTransition, "screen_dimmed");
	# warning-ignore:return_value_discarded
	get_tree().change_scene("res://elements/air/retell/RetellLevelSelector.tscn");
	self.queue_free();
	BarTransition.end_transition();
