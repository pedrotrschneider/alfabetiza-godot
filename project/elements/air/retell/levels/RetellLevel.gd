class_name RetellLevel
extends Resource

export(int) var level_num = -1;
export(String, MULTILINE) var myth_bbcode_text = "";
export(Array, String) var option_words = ["", "", "", "", "", ""];
export(Array, Texture) var option_images = [null, null, null, null, null, null];
