class_name RetellOptionCard
extends Node2D

signal card_selected(opt_card, dock_index);

export(NodePath) onready var area = self.get_node(area) as Area2D;
export(NodePath) onready var scale_tween = self.get_node(scale_tween) as Tween;
export(NodePath) onready var position_tween = self.get_node(position_tween) as Tween;
export(NodePath) onready var draggable = self.get_node(draggable) as Draggable;

var size : Vector2 = Vector2(250, 250);
var pos : Vector2 = -size / 2;
var corner_radius : int = 30;

export(Texture) var texture;
var word : String = "default word";
export var index : int = -1;
var mouse_over : bool = false;
var mouse_pressed_over : bool = false;
export var selectable : bool = true;
var selected : bool = false;


func _ready() -> void:
	area.connect("mouse_entered", self, "_on_area_mouse_entered");
	area.connect("mouse_exited", self, "_on_area_mouse_exited");
	
	$Sprite.texture = texture;
	$Sprite.scale = Vector2((size.x - 50) / texture.get_size().x, (size.y - 50) / texture.get_size().y);


func _process(_delta : float) -> void:
	if(!mouse_pressed_over && !draggable.docked):
		if(draggable.dock):
			select();
		interpolate_to_target();


func _input(event: InputEvent) -> void:
	if(selectable):
		if(mouse_over):
			if(event.is_action_pressed("mouse_select") || (event is InputEventScreenTouch && event.is_pressed())):
				mouse_pressed_over = true;
				draggable.docked = false;
				interpolate_scale_to(0.9);
			elif(event.is_action_released("mouse_select") || (event is InputEventScreenTouch && !event.is_pressed())):
				mouse_pressed_over = false;
				interpolate_scale_to(1.0);
			elif(mouse_pressed_over):
				if(event is InputEventMouseMotion):
					self.position += event.relative;


func _draw() -> void:
	var style_box : StyleBoxFlat = StyleBoxFlat.new()
	style_box.bg_color = Color(0.4, 0.4, 0.4);
	style_box.set_corner_radius_all(corner_radius);
	draw_style_box(style_box, Rect2(pos, size));


func _on_area_mouse_entered() -> void:
	if(!selectable): return;
	mouse_over = true;
	interpolate_scale_to(1.1);
	if Game.plataform == InitLoader.Plataforms.MOBILE:
		mouse_pressed_over = true;
		draggable.docked = false;
		interpolate_scale_to(0.9);


func _on_area_mouse_exited() -> void:
	if(!selectable): return;
	mouse_over = false;
	interpolate_scale_to(1.0);
	if Game.plataform == InitLoader.Plataforms.MOBILE:
		mouse_pressed_over = false;
		interpolate_scale_to(1.0);


func interpolate_scale_to(val : float) -> void:
	scale_tween.stop_all();
	scale_tween.remove_all();
	scale_tween.interpolate_property(self, "scale", null, Vector2(val, val), 1.0, Tween.TRANS_ELASTIC, Tween.EASE_OUT);
	scale_tween.start();


func interpolate_to_target() -> void:
	position_tween.stop_all();
	position_tween.remove_all();
	position_tween.interpolate_property(self, "position", null, draggable.target_position, 0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT);
	position_tween.start();
	draggable.docked = true;


func select() -> void:
	self.emit_signal("card_selected", self, draggable.dock.dock_index);


func _select_answer(is_answer : bool) -> void:
	mouse_over = false;
	if(is_answer):
		interpolate_scale_to(1.0);
	else:
		interpolate_scale_to(1.0);
		draggable.reset();
		interpolate_to_target()


func toggle_mouse_over() -> void:
	mouse_over = !mouse_over;


func set_texture(texture_ : Texture, word_  : String) -> void:
	texture = texture_;
	word = word_;
	$Sprite.texture = texture_;
	$Sprite.scale = Vector2((size.x - 50) / texture_.get_size().x, (size.y - 50) / texture_.get_size().y);
