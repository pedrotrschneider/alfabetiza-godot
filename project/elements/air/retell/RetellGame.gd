class_name RetellGame
extends Node2D

export(NodePath) onready var option_cards = self.get_node(option_cards) as Node2D;
export(NodePath) onready var image_docks = self.get_node(image_docks) as Node2D;
export(NodePath) onready var level_label = self.get_node(level_label) as Label;
export(NodePath) onready var back_button = self.get_node(back_button) as Button;
export(NodePath) onready var myth_label = self.get_node(myth_label) as RichTextLabel;
export(NodePath) onready var victory_sfx_player = self.get_node(victory_sfx_player) as AudioStreamPlayer;
export(NodePath) onready var fail_sfx_player = self.get_node(fail_sfx_player) as AudioStreamPlayer;
export(NodePath) onready var game_saver = self.get_node(game_saver) as GameSaver;

var initial_card_positions: Array = [];

var level_data : RetellLevel = load("res://elements/air/retell/levels/tutorial.tres") as RetellLevel;
var num_answers : int = 3;
var points : int = 3;
var wrong_picks : Array = [];
var time_taken : float = 0;


func _ready() -> void:
	back_button.connect("pressed", self, "_on_back_button_pressed");
	
	init_game();
	
	for opt_card in option_cards.get_children():
		opt_card.connect("card_selected", self, "_on_card_selected");


func _process(delta: float) -> void:
	time_taken += delta;


func _on_card_selected(opt_card : RetellOptionCard, dock_index : int) -> void:
	opt_card._select_answer(dock_index == opt_card.index);
	if(dock_index != opt_card.index):
		fail_sfx_player.play();
		wrong_picks.append(opt_card.word)
		# warning-ignore:narrowing_conversion
		points = max(0, points - 1);
		return;
	victory_sfx_player.play();
	opt_card.selectable = false;
	
	num_answers -= 1;
	if(num_answers == 0):
		for c in get_children():
			if(c is RetellOptionCard):
				c.selectable = false;
		var data : Dictionary = {
			"wrong_picks" : wrong_picks
		}
		yield(self.get_tree().create_timer(0.5), "timeout");
		game_saver.save(PlayerData.player_id, ElementID.FIRE, MinigameID.WORD_HUNT,\
			level_data.level_num, time_taken, wrong_picks.size(), data);
		yield(game_saver, "game_saved");
		
		var next_level_idx: int = level_data.level_num + 1;
		var file: File = File.new();
		var next_level_path: String = "res://elements/air/retell/levels/level%d.tres" % next_level_idx;
		if file.file_exists(next_level_path):
			level_data = load(next_level_path);
			BarTransition.begin_transition();
			yield(BarTransition, "screen_dimmed");
			reset();
			BarTransition.end_transition();
		else:
			BarTransition.begin_transition();
			yield(BarTransition, "screen_dimmed");
			# warning-ignore:return_value_discarded
			get_tree().change_scene("res://elements/air/retell/RetellLevelSelector.tscn");
			self.queue_free();
			BarTransition.end_transition();


func reset() -> void:
	num_answers = 3;
	points = 3;
	wrong_picks = [];
	time_taken = 0;
	
	for opt_card in option_cards.get_children():
		if opt_card is RetellOptionCard:
			opt_card.global_position = initial_card_positions[0];
			initial_card_positions.remove(0);
			opt_card.mouse_over = false;
			opt_card.mouse_pressed_over = false;
			opt_card.selectable = true;
			opt_card.selected = false;
	
	init_game();


func init_game() -> void:
	level_label.text = "NÍVEL  %d" % level_data.level_num;
	myth_label.bbcode_text = level_data.myth_bbcode_text;
	
	initial_card_positions.clear();
	var idx : Array = range(6);
	idx.shuffle();
	print(idx);
	var i : int = 0;
	for opt_card in option_cards.get_children():
		opt_card = opt_card as RetellOptionCard;
		initial_card_positions.append(opt_card.global_position);
		opt_card.set_texture(level_data.option_images[idx[i]], level_data.option_words[idx[i]]);
		opt_card.index = idx[i];
		i += 1;


func _on_back_button_pressed() -> void:
	BarTransition.begin_transition();
	yield(BarTransition, "screen_dimmed");
	# warning-ignore:return_value_discarded
	get_tree().change_scene("res://elements/air/retell/RetellLevelSelector.tscn");
	self.queue_free();
	BarTransition.end_transition();
