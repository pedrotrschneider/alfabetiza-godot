class_name MemoryGame
extends Node2D

export(Array, AudioStream) var bird_audios : Array;
export(Array, Texture) var bird_images : Array;
export(Array, NodePath) var bird_cards : Array;
export(Array, NodePath) var sound_cards : Array;
export(NodePath) onready var level_label = self.get_node(level_label) as Label;
export(NodePath) onready var back_button = self.get_node(back_button) as Button;
export(NodePath) onready var debug_word_label = self.get_node(debug_word_label) as Label;
export(NodePath) onready var victory_sfx_player = self.get_node(victory_sfx_player) as AudioStreamPlayer;
export(NodePath) onready var fail_sfx_player = self.get_node(fail_sfx_player) as AudioStreamPlayer;
export(NodePath) onready var game_saver = self.get_node(game_saver) as GameSaver;

var num_cards : int = 8;
var points : int = 0;
var num_answers : int = num_cards;
var wrong_picks : Array = [];
var time_taken : float = 0;


func _ready() -> void:
	MusicController.play_ambiance(MusicController.Ambiance.STOP);
	
	back_button.connect("pressed", self, "_on_back_button_pressed");
	
	for i in bird_cards.size():
		bird_cards[i] = self.get_node(bird_cards[i]);
	for i in sound_cards.size():
		sound_cards[i] = self.get_node(sound_cards[i]);
	
	var bird_idx : Array = range(num_cards);
	bird_idx.shuffle();
	for i in num_cards:
		var card : MemoryGameBirdCard = bird_cards[i];
		card.texture = bird_images[bird_idx[i]];
		card.bird_index = bird_idx[i];
		card.audio = bird_audios[bird_idx[i]];
	
	var sound_idx : Array = range(num_cards);
	sound_idx.shuffle();
	for i in num_cards:
		var card : MemoryGameSoundCard = sound_cards[i];
		# warning-ignore:return_value_discarded
		card.connect("card_selected", self, "_on_card_selected")
		card.audio = bird_audios[sound_idx[i]];
		card.bird_index = sound_idx[i];
	
	var dialogue: Node = Dialogic.start("memory_game1");
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");


func _process(delta: float) -> void:
	time_taken += delta;


func _on_card_selected(card : MemoryGameSoundCard, dock_index : int) -> void:
	card._select_answer(card.bird_index == dock_index);
	if(dock_index != card.bird_index):
		fail_sfx_player.play();
		wrong_picks.append(card.bird_name);
		return;
	num_answers -= 1;
	victory_sfx_player.play();
	if num_answers > 0:
		return
	
	for c in get_children():
		if(c is MemoryGameSoundCard):
			c.selectable = false;
	var data : Dictionary = {
		"wrong_picks" : wrong_picks
	}
	game_saver.save(PlayerData.player_id, ElementID.AIR, MinigameID.MEMORY_GAME,\
		0, time_taken, wrong_picks.size(), data);
	yield(game_saver, "game_saved");
	yield(get_tree().create_timer(0.5), "timeout");
	
	BarTransition.transition_to("res://elements/air/memory_game/MemoryGameMyth.tscn");
	yield(BarTransition, "screen_dimmed");
	self.queue_free();


func _on_mouse_entered_card(word : String) -> void:
	debug_word_label.text = word;


func _on_mouse_exited_card() -> void:
	debug_word_label.text = "";


func _on_back_button_pressed() -> void:
	BarTransition.transition_to("res://elements/air/AirMinigameSelector.tscn");
	yield(BarTransition, "screen_dimmed");
	self.queue_free();


func stop_all_chants() -> void:
	for bc in bird_cards:
		if bc is MemoryGameBirdCard:
			bc.stop_chant();
	
	for sc in sound_cards:
		if sc is MemoryGameSoundCard:
			sc.stop_chant();
