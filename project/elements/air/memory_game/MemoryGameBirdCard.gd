class_name MemoryGameBirdCard
extends Node2D

export(NodePath) onready var area = self.get_node(area) as Area2D;
export(NodePath) onready var dock = self.get_node(dock) as Dock;
export(NodePath) onready var sprite = self.get_node(sprite) as Sprite;
export(NodePath) onready var debug_question_label = self.get_node(debug_question_label) as Label;
export(NodePath) onready var audio_player = self.get_node(audio_player) as AudioStreamPlayer;
export(NodePath) onready var tween = self.get_node(tween) as Tween;

export var bird_index : int = -1 setget set_bird_index;

var size : Vector2 = Vector2(200, 200);
var pos : Vector2 = -size / 2;
var scl : Vector2 = Vector2(1.0, 1.0);
var corner_radius : int = 30;
var sprite_scale : Vector2 = Vector2.ONE;

var texture : Texture setget set_texture;
var word : String;
export var audio : AudioStream setget set_audio;

var mouse_over : bool = false;
var selectable : bool = true;
var selected : bool = false;


func _ready() -> void:
	area.connect("mouse_entered", self, "_on_area_mouse_entered");
	area.connect("mouse_exited", self, "_on_area_mouse_exited");
	
	audio_player.stream = audio;
	debug_question_label.text = word;
	dock.dock_index = bird_index;


func _input(event: InputEvent) -> void:
	if(selectable && mouse_over):
		if(event.is_action_pressed("mouse_select") || (event is InputEventScreenTouch && event.is_pressed())):
			interpolate_scale_to(0.9);
		elif(event.is_action_released("mouse_select") || (event is InputEventScreenTouch && !event.is_pressed())):
			interpolate_scale_to(1.0);

func _physics_process(_delta: float) -> void:
	update();


func _draw() -> void:
	pos = -(size * scl) / 2;
	var style_box : StyleBoxFlat = StyleBoxFlat.new()
	style_box.bg_color = Color(0.4, 0.4, 0.4);
	style_box.set_corner_radius_all(corner_radius);
	style_box.set_border_width_all(10);
	style_box.set_border_color(Color.black);
	draw_style_box(style_box, Rect2(pos, size * scl));


func _on_area_mouse_entered() -> void:
	if(!selectable): return;
	if not audio_player.playing:
		self.get_parent().stop_all_chants();
		audio_player.play();
	mouse_over = true;
	interpolate_scale_to(1.1);


func _on_area_mouse_exited() -> void:
	if(!selectable): return;
	mouse_over = false;
	interpolate_scale_to(1.0);


func interpolate_scale_to(val : float) -> void:
	tween.stop_all();
	tween.remove_all();
	tween.interpolate_property(sprite, "scale", null, sprite_scale * val, 1.0, Tween.TRANS_ELASTIC, Tween.EASE_OUT);
	tween.interpolate_property(self, "scl", null, Vector2(val, val), 1.0, Tween.TRANS_ELASTIC, Tween.EASE_OUT);
	tween.start();


func stop_chant() -> void:
	audio_player.stop();


func set_texture(val : Texture) -> void:
	texture = val;
	sprite.texture = val;
	sprite_scale = Vector2(150 / val.get_size().x, 150 / val.get_size().y);
	sprite.scale = sprite_scale;


func set_bird_index(val : int) -> void:
	$Dock.dock_index = val;
	bird_index = val;


func set_audio(val : AudioStream) -> void:
	$AudioStreamPlayer.stream = val;
	audio = val;
