class_name AirMinigameSelector
extends Control

export(NodePath) onready var memory_game_button = self.get_node(memory_game_button) as Button;
export(NodePath) onready var tongue_twister_button = self.get_node(tongue_twister_button) as Button;
export(NodePath) onready var valise3_button = self.get_node(valise3_button) as Button;
export(NodePath) onready var rebus4_button = self.get_node(rebus4_button) as Button;
export(NodePath) onready var puzzle_button = self.get_node(puzzle_button) as Button;
export(NodePath) onready var quiz_button = self.get_node(quiz_button) as Button;
export(NodePath) onready var retell_button = self.get_node(retell_button) as Button;
export(NodePath) onready var back_button = self.get_node(back_button) as Button;


func _ready() -> void:
	MusicController.play_ambiance(MusicController.Ambiance.AIR);
	
	memory_game_button.connect("pressed", self, "_on_memory_game_button_pressed");
	tongue_twister_button.connect("pressed", self, "_on_tongue_twister_button_pressed");
	valise3_button.connect("pressed", self, "_on_valise3_button_pressed");
	rebus4_button.connect("pressed", self, "_on_rebus4_button_pressed");
	puzzle_button.connect("pressed", self, "_on_puzzle_button_pressed");
	quiz_button.connect("pressed", self, "_on_quiz_button_pressed");
	retell_button.connect("pressed", self, "_on_retell_button_pressed");
	
	back_button.connect("pressed", self, "_on_back_button_pressed")


func _on_memory_game_button_pressed() -> void:
	BarTransition.transition_to("res://elements/air/memory_game/MemoryGame.tscn");


func _on_tongue_twister_button_pressed() -> void:
	BarTransition.transition_to("res://elements/air/tongue_twister/AirTongueTwisterGame.tscn");


func _on_valise3_button_pressed() -> void:
	BarTransition.transition_to("res://elements/air/valise3/Valise3LevelSelector.tscn");


func _on_rebus4_button_pressed() -> void:
	BarTransition.transition_to("res://elements/air/rebus4/Rebus4LevelSelector.tscn");


func _on_puzzle_button_pressed() -> void:
	BarTransition.transition_to("res://elements/air/puzzle/PuzzleLevelSelector.tscn");


func _on_quiz_button_pressed() -> void:
	BarTransition.transition_to("res://elements/air/quiz/QuizLevelSelector.tscn");


func _on_retell_button_pressed() -> void:
	BarTransition.transition_to("res://elements/air/retell/RetellLevelSelector.tscn");


func _on_back_button_pressed() -> void:
	BarTransition.transition_to("res://elements/ElementSelector.tscn");
