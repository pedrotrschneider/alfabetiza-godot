class_name QuizGame
extends Node2D

export(PackedScene) var option_card_scene;
export(PackedScene) var question_card_scene;

export(NodePath) onready var layer_node = self.get_node(layer_node) as Node2D;
export(NodePath) onready var level_label = self.get_node(level_label) as Label;
export(NodePath) onready var back_button = self.get_node(back_button) as Button;
export(NodePath) onready var debug_word_label = self.get_node(debug_word_label) as Label;
export(NodePath) onready var victory_sfx_player = self.get_node(victory_sfx_player) as AudioStreamPlayer;
export(NodePath) onready var fail_sfx_player = self.get_node(fail_sfx_player) as AudioStreamPlayer;
export(NodePath) onready var game_saver = self.get_node(game_saver) as GameSaver;

var level_data : QuizLevel;
var points : int = 3;
var wrong_picks : Array = [];
var time_taken : float = 0;


func _ready() -> void:
	back_button.connect("pressed", self, "_on_back_button_pressed");
	
	init_game();


func _process(delta: float) -> void:
	time_taken += delta;


func _on_card_selected(is_answer : bool, word : String) -> void:
	if(!is_answer):
		fail_sfx_player.play();
		wrong_picks.append(word)
		points -= 1;
		return;
	victory_sfx_player.play();
	for c in get_children():
		if(c is QuizOptionCard):
			c.selectable = false;
	var data : Dictionary = {
		"wrong_picks" : wrong_picks
	}
	game_saver.save(PlayerData.player_id, ElementID.AIR, MinigameID.VALISE2,\
		level_data.level_num, time_taken, wrong_picks.size(), data);
	yield(game_saver, "game_saved");
	yield(get_tree().create_timer(0.5), "timeout");
	
	var next_level_idx: int = level_data.level_num + 1;
	var file: File = File.new();
	var next_level_path: String = "res://elements/air/quiz/levels/level%d.tres" % next_level_idx;
	if file.file_exists(next_level_path):
		level_data = load(next_level_path);
		BarTransition.begin_transition();
		yield(BarTransition, "screen_dimmed");
		reset();
		BarTransition.end_transition();
	else:
		BarTransition.transition_to(get_level_selector_scene_path());
		yield(BarTransition, "screen_dimmed");
		self.queue_free();


func reset() -> void:
	points = 3;
	wrong_picks = [];
	time_taken = 0;
	
	for c in layer_node.get_children():
		c.queue_free();
	
	init_game();


func init_game() -> void:
	level_label.text = "NÍVEL %d" % level_data.level_num;
	var new_qcard : QuizQuestionCard = question_card_scene.instance();
	new_qcard.audio = level_data.question_audio;
	layer_node.add_child(new_qcard);
	new_qcard.position = Vector2(1920 / 2, 350);
	randomize();
	var num_opts : int = level_data.option_words.size();
	var idx : Array = range(num_opts);
	idx.shuffle();
	var ii : int = 0;
	for i in idx:
		var new_card : QuizOptionCard = option_card_scene.instance();
		new_card.is_answer = i == 0;
		layer_node.add_child(new_card);
		new_card.word = level_data.option_words[i];
		# warning-ignore:return_value_discarded
		new_card.connect("card_selected", self, "_on_card_selected");
		new_card.position = Vector2(1920 / float(num_opts + 1) * (ii + 1), 800);
		ii += 1;
	
	yield(self.get_tree().create_timer(0.5), "timeout");
	new_qcard.audio_stream_player.play();
	new_qcard.interpolate_scale_to(0.9);
	yield(self.get_tree().create_timer(1.0), "timeout");
	new_qcard.interpolate_scale_to(1.0);


func _on_back_button_pressed() -> void:
	BarTransition.transition_to(get_level_selector_scene_path());
	yield(BarTransition, "screen_dimmed");
	self.queue_free();


func get_level_selector_scene_path() -> String:
	return "res://elements/air/quiz/QuizLevelSelector.tscn";
