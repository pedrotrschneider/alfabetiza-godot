class_name QuizTutorial
extends Node2D

export(NodePath) onready var animation_player = self.get_node(animation_player) as AnimationPlayer;
export(NodePath) onready var back_button = self.get_node(back_button) as Button;
export(NodePath) onready var question_card = self.get_node(question_card) as QuizQuestionCard;
export(NodePath) onready var option_card1 = self.get_node(option_card1) as QuizOptionCard;
export(NodePath) onready var option_card2 = self.get_node(option_card2) as QuizOptionCard;
export(NodePath) onready var option_card3 = self.get_node(option_card3) as QuizOptionCard;
export(NodePath) onready var debug_syllable = self.get_node(debug_syllable) as Label;

var level_data : QuizLevel = load("res://elements/air/quiz/levels/tutorial.tres") as QuizLevel;

func _ready() -> void:
	back_button.connect("pressed", self, "_on_back_button_pressed");
	
	question_card.audio = level_data.question_audio;
	question_card.audio_stream_player.stream = level_data.question_audio;
	
	option_card1.word = level_data.option_words[0];
	option_card1.selectable = false;
	option_card1.is_answer = true;
	option_card2.word = level_data.option_words[1];
	option_card2.selectable = false;
	option_card3.word = level_data.option_words[2];
	option_card3.selectable = false;
	
	yield(self.get_tree().create_timer(0.5), "timeout");
	animation_player.play("p0");
	yield(animation_player, "animation_finished");
	yield(self.get_tree().create_timer(0.5), "timeout");
	var dialogue : Node = Dialogic.start("quiz_tutorial1");
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");
	animation_player.play("p1", -1, 0.5);
	yield(animation_player, "animation_finished");
	dialogue = Dialogic.start("quiz_tutorial2");
	self.add_child(dialogue);
	yield(dialogue, "tree_exited");
	yield(get_tree().create_timer(0.5), "timeout");
	
	BarTransition.transition_to(get_level_selector_scene_path());
	yield(BarTransition, "screen_dimmed");
	self.queue_free();


func _on_back_button_pressed() -> void:
	BarTransition.transition_to(get_level_selector_scene_path());
	yield(BarTransition, "screen_dimmed");
	self.queue_free();


func get_level_selector_scene_path() -> String:
	return "res://elements/air/quiz/QuizLevelSelector.tscn";
