class_name MinigameButton
extends Node2D

export var target_scene_path: String;
export var icon: Texture;

onready var sprite := $Sprite;
onready var timer := $AnimationTimer;
onready var animation_player := $AnimationPlayer;
onready var button := $Button;

var enabled: bool = false setget set_enabled;


func _ready() -> void:
	reset_timer();
	sprite.texture = icon;
	
	# warning-ignore:return_value_discarded
	timer.connect("timeout", self, "_on_timer_timeout");
	# warning-ignore:return_value_discarded
	button.connect("pressed", self, "_on_button_pressed");


func _on_timer_timeout() -> void:
	animation_player.play((randi() % 2) as String);
	reset_timer();


func _on_button_pressed() -> void:
	if enabled:
		BarTransition.transition_to(target_scene_path);


func reset_timer() -> void:
	timer.start(randf() * 7 + 3);


func set_enabled(val: bool) -> void:
	enabled = val;
	if enabled:
		animation_player.play("saturate");
