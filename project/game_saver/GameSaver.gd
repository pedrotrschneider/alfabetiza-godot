class_name GameSaver
extends Control

signal game_saved();
signal save_failed();

export(NodePath) onready var save_icon = self.get_node(save_icon) as ColorRect;


func _ready() -> void:
	# warning-ignore:return_value_discarded
	HttpRequester.connect("request_completed", self, "_on_request_completed");


func reset() -> void:
	save_icon.hide();


func save(player_id : int, element_id : int, minigame_id : int,\
	level_id : int, time_taken : float, num_mistakes : int, data : Dictionary) -> void:
	if PlayerData.logged_in:
		HttpRequester.request_minigame_data_post(player_id, element_id,\
			minigame_id, level_id, time_taken, num_mistakes, data);
		save_icon.color = Color.white;
		save_icon.show();
	else:
		save_icon.hide();
		yield(self.get_tree().create_timer(0.05), "timeout");
		self.emit_signal("game_saved");


func _on_request_completed(response_code: int, body: String) -> void:
	if(response_code == HttpResponseCode.SUCCESS):
		print("Response code 200: " + body);
		self.emit_signal("game_saved");
		save_icon.color = Color.green;
	elif(response_code == HttpResponseCode.BAD_REQUEST_ERROR):
		print("Error response code 403: " + body);
		self.emit_signal("save_failed");
		self.emit_signal("game_saved");
		save_icon.color = Color.red;
	elif(response_code == HttpResponseCode.SERVER_EREROR\
		|| response_code == HttpResponseCode.SERVER_UNAVAILABLE):
		print("Error response code 500:" + body);
		self.emit_signal("save_failed");
		self.emit_signal("game_saved");
		save_icon.color = Color.red;
	yield(self.get_tree().create_timer(0.5), "timeout");
	reset();
