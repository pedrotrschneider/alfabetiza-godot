extends CanvasLayer

signal screen_dimmed();
signal finished();

export(NodePath) onready var rect_top = self.get_node(rect_top) as ColorRect;
export(NodePath) onready var rect_bottom = self.get_node(rect_bottom) as ColorRect;
export(NodePath) onready var tween = self.get_node(tween) as Tween;
export var transition_duration : float = 0.7;
export var transition_delay : float = 0.2;


func begin_transition() -> void:
	tween.stop_all();
	tween.remove_all();
	
	tween.interpolate_property(rect_top, "rect_size", null, Vector2(1920, 540), transition_duration, Tween.TRANS_BOUNCE, Tween.EASE_OUT);
	tween.interpolate_property(rect_bottom, "rect_position", null, Vector2(0, 540), transition_duration, Tween.TRANS_BOUNCE, Tween.EASE_OUT);
	tween.interpolate_property(rect_bottom, "rect_size", null, Vector2(1920, 540), transition_duration, Tween.TRANS_BOUNCE, Tween.EASE_OUT);
	tween.start();
	
	yield(tween, "tween_all_completed");
	yield(get_tree(), "idle_frame");
	self.emit_signal("screen_dimmed");


func end_transition() -> void:
	tween.stop_all();
	tween.remove_all();
	
	tween.interpolate_property(rect_top, "rect_size", null, Vector2(1920, 0), transition_duration * 0.2, Tween.TRANS_LINEAR);
	tween.interpolate_property(rect_bottom, "rect_position", null, Vector2(0, 1080), transition_duration * 0.2, Tween.TRANS_LINEAR);
	tween.interpolate_property(rect_bottom, "rect_size", null, Vector2(1920, 0), transition_duration * 0.2, Tween.TRANS_LINEAR);
	tween.start();
	
	yield(tween, "tween_all_completed");
	yield(get_tree(), "idle_frame");
	self.emit_signal("finished");


func transition_to(scene_path : String) -> void:
	begin_transition();
	yield(self, "screen_dimmed");
	yield(get_tree(), "idle_frame");
	
	# warning-ignore:return_value_discarded
	get_tree().change_scene(scene_path);
	yield(get_tree().create_timer(transition_delay), "timeout");
	yield(get_tree(), "idle_frame");
	
	end_transition();
