extends Node

enum Ambiance {
	EARTH,
	WATER,
	FIRE,
	AIR,
	STOP
}

export(NodePath) onready var earth_ambiance_player = self.get_node(earth_ambiance_player) as AudioStreamPlayer;
export(NodePath) onready var water_ambiance_player = self.get_node(water_ambiance_player) as AudioStreamPlayer;
export(NodePath) onready var fire_ambiance_player = self.get_node(fire_ambiance_player) as AudioStreamPlayer;
export(NodePath) onready var air_ambiance_player = self.get_node(air_ambiance_player) as AudioStreamPlayer;

export(float, 1, 5) var transition_duration = 2.0;

var current_player: AudioStreamPlayer = null;

func play_ambiance(ambiance: int) -> void:
	# Get the new player
	var new_current_player: AudioStreamPlayer = null;
	match ambiance:
		Ambiance.EARTH:
			new_current_player = earth_ambiance_player;
		Ambiance.WATER:
			new_current_player = water_ambiance_player;
		Ambiance.FIRE:
			new_current_player = fire_ambiance_player;
		Ambiance.AIR:
			new_current_player = air_ambiance_player;
	
	# If the new requested player is already playing, just keep playing
	if new_current_player == current_player:
		return;
	
	# Fade out the current player
	if current_player:
		fadeout_stream_player(current_player);
	# Fade in the new player
	if new_current_player:
		fadein_stream_player(new_current_player);
	# Wait for the fadeout transition to end
	yield(self.get_tree().create_timer(0.5 * transition_duration), "timeout");
	# Stop and update the current player
	if current_player:
		current_player.stop();
	current_player = new_current_player;


func fadein_stream_player(sp: AudioStreamPlayer) -> void:
	sp.volume_db = -40;
	sp.play();
	var tween: SceneTreeTween = self.get_tree().create_tween();
	# warning-ignore:return_value_discarded
	tween.tween_property(sp, "volume_db", 0.0, transition_duration);


func fadeout_stream_player(sp: AudioStreamPlayer) -> void:
	var tween: SceneTreeTween = self.get_tree().create_tween();
	# warning-ignore:return_value_discarded
	tween.tween_property(sp, "volume_db", -40.0, transition_duration);
	yield(self.get_tree().create_timer(0.5 * transition_duration), "timeout");
