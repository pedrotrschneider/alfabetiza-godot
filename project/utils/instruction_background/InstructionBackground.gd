class_name InstructionBackground
extends Panel

export(NodePath) onready var parent = self.get_node(parent) as Control;
export var padding: Vector2 = Vector2(50, 50);


func _physics_process(_delta: float) -> void:
	self.rect_pivot_offset = parent.rect_pivot_offset;
	self.rect_global_position = parent.rect_global_position - padding * 0.5;
	self.rect_size = parent.rect_size + padding;
