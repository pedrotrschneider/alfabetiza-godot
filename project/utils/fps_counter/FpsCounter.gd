extends CanvasLayer

export(NodePath) onready var fps_counter = self.get_node(fps_counter) as Label;

func _process(delta: float) -> void:
	fps_counter.text = "FPS: %d" % (1 / delta);
