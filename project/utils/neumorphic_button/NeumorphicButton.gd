extends Button

export(Resource) onready var button_data = button_data as NeumorphicButtonAnimationData;

var button_stylebox: StyleBoxFlat;
var background_stylebox: StyleBoxFlat;
var shadow_stylebox: StyleBoxFlat;

var background_panel1: Panel;
var background_panel2: Panel;
var shadow_panel: Panel;


func _ready() -> void:
	# warning-ignore-all:return_value_discarded
	self.connect("mouse_entered", self, "_on_mouse_entered");
	self.connect("mouse_exited", self, "_on_mouse_exited");
	self.connect("button_down", self, "_on_button_down");
	self.connect("button_up", self, "_on_button_up");
	
	# Generating styleboxes
	# Generating button stylebox
	button_stylebox = StyleBoxFlat.new();
	button_stylebox.bg_color = button_data.button_normal_background_color;
	button_stylebox.corner_detail = 20;
	button_stylebox.corner_radius_top_left = button_data.normal_corner_radius;
	button_stylebox.corner_radius_top_right = button_data.normal_corner_radius;
	button_stylebox.corner_radius_bottom_left = button_data.normal_corner_radius;
	button_stylebox.corner_radius_bottom_right = button_data.normal_corner_radius;
	button_stylebox.shadow_color = button_data.light_shadow_color;
	button_stylebox.shadow_size = button_data.button_shadow_size;
	button_stylebox.shadow_offset = Vector2(- button_data.shadow_offset, - button_data.shadow_offset);
	# Generating shadow stylebox
	shadow_stylebox = StyleBoxFlat.new();
	shadow_stylebox.bg_color = Color.transparent;
	shadow_stylebox.corner_detail = 20;
	shadow_stylebox.corner_radius_top_left = button_data.normal_corner_radius;
	shadow_stylebox.corner_radius_top_right = button_data.normal_corner_radius;
	shadow_stylebox.corner_radius_bottom_left = button_data.normal_corner_radius;
	shadow_stylebox.corner_radius_bottom_right = button_data.normal_corner_radius;
	shadow_stylebox.shadow_color = button_data.dark_shadow_color;
	shadow_stylebox.shadow_size = button_data.button_shadow_size;
	shadow_stylebox.shadow_offset = Vector2(button_data.shadow_offset, button_data.shadow_offset);
	# Generating background stylebox
	background_stylebox = StyleBoxFlat.new();
	background_stylebox.bg_color = button_data.background_color;
	background_stylebox.corner_detail = 20;
	background_stylebox.corner_radius_top_left = button_data.normal_corner_radius;
	background_stylebox.corner_radius_top_right = button_data.normal_corner_radius;
	background_stylebox.corner_radius_bottom_left = button_data.normal_corner_radius;
	background_stylebox.corner_radius_bottom_right = button_data.normal_corner_radius;
	background_stylebox.expand_margin_left = button_data.background_expand_margin;
	background_stylebox.expand_margin_right = button_data.background_expand_margin;
	background_stylebox.expand_margin_top = button_data.background_expand_margin;
	background_stylebox.expand_margin_bottom = button_data.background_expand_margin;
	background_stylebox.shadow_color = button_data.background_color;
	background_stylebox.shadow_size = button_data.background_shadow_size;
	
	# Adding styles to button
	self.add_stylebox_override("normal", button_stylebox);
	self.add_stylebox_override("hover", button_stylebox);
	self.add_stylebox_override("pressed", button_stylebox);
	self.add_stylebox_override("focus", StyleBoxEmpty.new());
	self.add_stylebox_override("disabled", StyleBoxEmpty.new());
	
	# Adding nodes to scene
	# Adding background 1 to the scene
	background_panel1 = Panel.new();
	background_panel1.show_behind_parent = true;
	background_panel1.mouse_filter = Control.MOUSE_FILTER_IGNORE;
	background_panel1.anchor_top = 0;
	background_panel1.anchor_left = 0;
	background_panel1.anchor_bottom = 1;
	background_panel1.anchor_right = 1;
	background_panel1.add_stylebox_override("panel", background_stylebox);
	self.add_child(background_panel1);
	# Adding background 2 to the scene
	background_panel2 = background_panel1.duplicate();
	self.add_child(background_panel2);
	# Adding shadow to the scene
	shadow_panel = background_panel1.duplicate();
	shadow_panel.add_stylebox_override("panel", shadow_stylebox);
	self.add_child(shadow_panel);


func _on_mouse_entered() -> void:
	tween_style(button_data.light_shadow_color, \
		button_data.dark_shadow_color, \
		button_data.button_normal_background_color, \
		button_data.hover_corner_radius);


func _on_mouse_exited() -> void:
	tween_style(button_data.light_shadow_color, \
		button_data.dark_shadow_color, \
		button_data.button_normal_background_color, \
		button_data.normal_corner_radius);


func _on_button_down() -> void:
	tween_style(button_data.dark_shadow_color, \
		button_data.light_shadow_color, \
		button_data.button_pressed_background_color, \
		button_data.hover_corner_radius);


func _on_button_up() -> void:
	tween_style(button_data.light_shadow_color, \
		button_data.dark_shadow_color, \
		button_data.button_normal_background_color, \
		button_data.hover_corner_radius);


func tween_style(top_shadow_color: Color, \
	bottom_shadow_color: Color, \
	button_color: Color, \
	corner_radius: int) -> void:
	
	var tween: SceneTreeTween = self.create_tween() \
		.set_parallel(true) \
		.set_trans(Tween.TRANS_CUBIC) \
		.set_ease(Tween.EASE_IN_OUT);
	
	# warning-ignore-all:return_value_discarded
	# Tweening corner radius
	tween.tween_property(button_stylebox, "corner_radius_bottom_left", corner_radius, button_data.transition_duration);
	tween.tween_property(button_stylebox, "corner_radius_bottom_right", corner_radius, button_data.transition_duration);
	tween.tween_property(button_stylebox, "corner_radius_top_left", corner_radius, button_data.transition_duration);
	tween.tween_property(button_stylebox, "corner_radius_top_right", corner_radius, button_data.transition_duration);
	
	tween.tween_property(shadow_stylebox, "corner_radius_bottom_left", corner_radius, button_data.transition_duration);
	tween.tween_property(shadow_stylebox, "corner_radius_bottom_right", corner_radius, button_data.transition_duration);
	tween.tween_property(shadow_stylebox, "corner_radius_top_left", corner_radius, button_data.transition_duration);
	tween.tween_property(shadow_stylebox, "corner_radius_top_right", corner_radius, button_data.transition_duration);
	
	tween.tween_property(background_stylebox, "corner_radius_bottom_left", corner_radius, button_data.transition_duration);
	tween.tween_property(background_stylebox, "corner_radius_bottom_right", corner_radius, button_data.transition_duration);
	tween.tween_property(background_stylebox, "corner_radius_top_left", corner_radius, button_data.transition_duration);
	tween.tween_property(background_stylebox, "corner_radius_top_right", corner_radius, button_data.transition_duration);
	
	# Tweening colors
	tween.tween_property(button_stylebox, "bg_color", button_color, button_data.transition_duration);
	tween.tween_property(button_stylebox, "shadow_color", top_shadow_color, button_data.transition_duration);
	tween.tween_property(shadow_stylebox, "shadow_color", bottom_shadow_color, button_data.transition_duration);
