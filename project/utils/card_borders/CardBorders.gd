class_name CardBorders
extends Node2D

export var size : Vector2;
export var corner_radius : int;
export var border_width : int;
export var border_color : Color;

var style_box : StyleBoxFlat;


func _ready() -> void:
	style_box = StyleBoxFlat.new();
	style_box.bg_color = Color(0.0, 0.0, 0.0, 0.0);
	style_box.set_corner_radius_all(corner_radius);
	style_box.set_border_width_all(border_width);
	style_box.set_border_color(border_color);


func _draw() -> void:
	self.draw_style_box(style_box, Rect2(-size / 2, size));
