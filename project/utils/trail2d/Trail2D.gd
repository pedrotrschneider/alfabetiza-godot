class_name Trail2D
extends Line2D

export(NodePath) onready var tracker = self.get_node(tracker) as Node2D;
export var length: int = 50;

var point: Vector2;

func _process(_delta: float) -> void:
	self.global_position = Vector2.ZERO;
	self.global_rotation = 0;
	
	var new_point: Vector2 = tracker.global_position;
#	if point.distance_squared_to(new_point) > 30.0:
	self.add_point(new_point);
	point = new_point;
	
	while self.get_point_count() > length:
		self.remove_point(0)
