class_name ButtonAnimationData
extends Resource

export(Color) var background_color;
export(Color) var button_normal_background_color;
export(Color) var button_pressed_background_color;
export(Color) var light_shadow_color;
export(Color) var dark_shadow_color;

export(int) var normal_corner_radius;
export(int) var hover_corner_radius;
export(int) var background_shadow_size;
export(int) var button_shadow_size;
export(int) var shadow_offset;
export(int) var background_expand_margin;
export(int) var content_margin;

export(float) var transition_duration;
