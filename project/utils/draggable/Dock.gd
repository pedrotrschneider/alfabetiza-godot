class_name Dock
extends Area2D

var enabled : bool = true;
var dock_index : int = -1;

export var size : Vector2 = Vector2(250, 200) setget set_size;
var pos : Vector2 = -size / 2;
var corner_radius : int = 30;


func _draw() -> void:
	var style_box : StyleBoxFlat = StyleBoxFlat.new()
	style_box.bg_color = Color(0.3, 0.3, 0.3);
	style_box.border_color = Color.black;
	style_box.set_corner_radius_all(corner_radius);
	style_box.set_border_width_all(10);
	draw_style_box(style_box, Rect2(pos, size));


func set_size(val : Vector2) -> void:
	size = val;
	pos = -size / 2;
	update();
