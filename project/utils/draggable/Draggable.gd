class_name Draggable
extends Node2D

export(NodePath) onready var area = self.get_node(area) as Area2D;

var start_position : Vector2;
var target_position : Vector2;
var docked : bool = true;
var dock : Dock = null;

func _ready() -> void:
	area.connect("area_entered", self, "_on_area_entered");
	area.connect("area_exited", self, "_on_area_exited");
	
	yield(get_tree(), "idle_frame");
	start_position = self.get_parent().position;
	target_position = start_position;


func _process(_delta: float) -> void:
	if(docked && dock):
		dock.enabled = false;


func reset() -> void:
	target_position = start_position;


func _on_area_entered(_area : Area2D) -> void:
	if(_area is Dock && _area.enabled):
		target_position = _area.global_position;
		dock = _area;


func _on_area_exited(_area : Area2D) -> void:
	if(_area is Dock):
		target_position = start_position;
		if(dock):
			dock.enabled = true;
			dock = null;
