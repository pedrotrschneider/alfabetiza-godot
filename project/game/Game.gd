extends Node

export(NodePath) onready var fullscreen_label = self.get_node(fullscreen_label) as Label;

var can_fullscreen: bool = true;
var plataform: int = InitLoader.Plataforms.DESKTOP;
var login_type: int = InitLoader.LoginTypes.NO_LOGIN;

func _input(event: InputEvent) -> void:
	if(event is InputEventScreenTouch):
		if(event.is_pressed()):
			var a = InputEventAction.new()
			a.action = "mouse_select"
			a.pressed = true
			Input.parse_input_event(a)
		else:
			var a = InputEventAction.new()
			a.action = "mouse_select"
			a.pressed = false
			Input.parse_input_event(a)
	if(event.is_action_pressed("quit")):
		quit_game();


func _physics_process(_delta: float) -> void:
	if fullscreen_label:
		fullscreen_label.visible = not OS.window_fullscreen;
	
	if Input.is_key_pressed(KEY_CONTROL):
		if Input.is_key_pressed(KEY_F):
			if can_fullscreen:
				OS.window_fullscreen = not OS.window_fullscreen;
				can_fullscreen = false;
	else:
		can_fullscreen = true;


func send_input_action(action: String, pressed: bool) -> void:
	var input_action := InputEventAction.new();
	input_action.action = action;
	input_action.pressed = pressed;
	Input.parse_input_event(input_action);


func quit_game() -> void:
	BarTransition.begin_transition();
	yield(BarTransition, "screen_dimmed");
	get_tree().quit();
