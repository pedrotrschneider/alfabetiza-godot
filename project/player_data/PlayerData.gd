extends Node

var logged_in : bool = false;
var player_id : int = 0;
var username : String = "";

var played_earth_legend: bool = false;
var played_tutorials: Array = [
	false, # Caça Alimentos
	false, # Caça Letras
	false, # Acrofonia
	false, # Valise 1
	false, # Valise 2
	false, # Valise 3
	false, # Rebus 1
	false, # Nada, por algum motivo ??
	false, # Corrida do Boto
	false, # Troca Sílabas
	false, # Rebus 2
	false, # Corrida do Fogo
	false, # Caça Palavras
	false, # Rebus 3
	false, # Trava Línguas
	false, # Adivinha
	false, # Jogo da Memória
	false, # Rebus 4
	false, # Enigma
	false, # Quiz
	false, # Reconto
];


func logout() -> void:
	logged_in = false;
	player_id = 0;
	username = "";
