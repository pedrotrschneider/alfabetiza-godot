class_name InitLoader
extends Node

enum LoginTypes {
	NO_LOGIN,
	LOCAL_LOGIN,
	SERVER_LOGIN,
	GUEST_LOGIN,
}

enum Plataforms {
	DESKTOP,
	MOBILE
}

export(PackedScene) var element_selector_scene;
export(PackedScene) var login_screen_scene;

export(LoginTypes) var login_type: int = LoginTypes.NO_LOGIN;
export(Plataforms) var plataform: int = Plataforms.DESKTOP;
export var server_url: String = "http://www.usp.br/line/alfabetiza/jogo/server/";
export var local_url: String = "http://localhost/alfabetiza/";


func _ready() -> void:
	Game.plataform = plataform;
	Game.login_type = login_type;
	
	match login_type:
		LoginTypes.NO_LOGIN:
			init_no_login();
		LoginTypes.LOCAL_LOGIN:
			init_local_login();
		LoginTypes.SERVER_LOGIN:
			init_server_login();
		LoginTypes.GUEST_LOGIN:
			init_guest_login();
		_:
			printerr("Invalid login type");


func init_no_login() -> void:
	# warning-ignore:return_value_discarded
	self.get_tree().change_scene_to(element_selector_scene);


func init_local_login() -> void:
	HttpRequester.HOST_URL = local_url;
	# warning-ignore:return_value_discarded
	self.get_tree().change_scene_to(login_screen_scene);


func init_server_login() -> void:
	HttpRequester.HOST_URL = server_url;
	# warning-ignore:return_value_discarded
	self.get_tree().change_scene_to(login_screen_scene);


func init_guest_login() -> void:
	printerr("Guest login is not available yet");
